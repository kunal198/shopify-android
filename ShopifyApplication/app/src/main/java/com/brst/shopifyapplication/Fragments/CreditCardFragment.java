package com.brst.shopifyapplication.Fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.ApiImplementation.ApiClass;
import com.brst.shopifyapplication.HelperClasses.Validator;
import com.brst.shopifyapplication.Interfaces.CheckoutInterface;
import com.brst.shopifyapplication.Interfaces.PaymentInterface;
import com.brst.shopifyapplication.Interfaces.PaymentSettingInterface;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.databinding.CreditCardDetailsBinding;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.shopify.buy3.CardClient;
import com.shopify.buy3.CreditCard;
import com.shopify.buy3.CreditCardVaultCall;
import com.shopify.buy3.Storefront;
import com.shopify.graphql.support.ID;

import java.io.IOException;
import java.math.BigDecimal;


public class CreditCardFragment extends BaseFragment {
    private final static int DO_UPDATE_TEXT = 0;
    private final static int DO_THAT = 1;
    static CreditCardFragment productFragment;
    private final Handler myHandler = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message msg) {
            final int what = msg.what;
            switch (what) {
                case DO_UPDATE_TEXT:

                    Toast.makeText(getActivity(), "Pay id", Toast.LENGTH_SHORT).show();

                    break;
                case DO_THAT:
                    dismissProgressDialog(getActivity());

                    break;
            }
        }
    };
    View view;
    ID chceckout_id;
    Storefront.MailingAddressInput input;
    BigDecimal getTotalPrice;
    String selected_month = "1", selcted_year = "2017";
    CreditCardDetailsBinding creditCardDetailsBinding;

    /*public void setData(Storefront.Collection collection) {
        data = new ArrayList<>();
        data.add(collection);

    }
*/
    public static CreditCardFragment getInstance() {
        if (productFragment == null) {
            return productFragment = new CreditCardFragment();
        }
        return productFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //  if (view == null) {
        creditCardDetailsBinding = DataBindingUtil.inflate(
                inflater, R.layout.credit_card_details, container, false);
        view = creditCardDetailsBinding.getRoot();

        initView();
        getActivity().setTitle("Card Details");

        getDataFromBundle();
        initSpinner();
        //    }
        ((HomeActivity) getActivity()).setExpansion(true, true, 0);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.search);
        MenuItem item1 = menu.findItem(R.id.sort);
        MenuItem item12 = menu.findItem(R.id.bag);
        MenuItem logout = menu.findItem(R.id.logout);
        MenuItem mode = menu.findItem(R.id.mode);
        mode.setVisible(false);
        item12.setVisible(false);
        item.setVisible(false);
        item1.setVisible(false);
        logout.setVisible(false);
    }

    private void initSpinner() {
        creditCardDetailsBinding.monthSp.setItems(getResources().getStringArray(R.array.month));


        creditCardDetailsBinding.monthSp.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                selected_month = item.toString();
            }
        });

        creditCardDetailsBinding.yearSp.setItems(getResources().getStringArray(R.array.year));


        creditCardDetailsBinding.yearSp.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selcted_year = item.toString();

            }
        });
    }

    private void initView() {

        creditCardDetailsBinding.payNowBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Validator.validCreditCardDetails(getActivity(), creditCardDetailsBinding.firstNameET, creditCardDetailsBinding.lastnameET, creditCardDetailsBinding.carNoET, creditCardDetailsBinding.cvvET)) {
                    initProgressDialog(getActivity());
                    new ApiClass().paymentSettings(getActivity(), new PaymentSettingInterface() {
                        @Override
                        public void onPayemntSettings(Storefront.PaymentSettings settings) {
                            String card_vault_url = settings.getCardVaultUrl();
                            // String vault_id = settings.getShopifyPaymentsAccountId();
                            creditCard(card_vault_url, chceckout_id, getTotalPrice);
                        }

                        @Override
                        public void onFailure(String error) {
                            myHandler.sendEmptyMessage(DO_THAT);

                        }

                        @Override
                        public void onError(String error) {
                            myHandler.sendEmptyMessage(DO_THAT);

                        }
                    });
                }

            }
        });


    }

    private void creditCard(String vault_url, ID checkoutId, BigDecimal amount) {

        CardClient cardClient = new CardClient();
        CreditCard creditCard = CreditCard.builder()
                .firstName(creditCardDetailsBinding.firstNameET.getText().toString())
                .lastName(creditCardDetailsBinding.lastnameET.getText().toString())
                .number(creditCardDetailsBinding.carNoET.getText().toString())
                .expireMonth(creditCardDetailsBinding.monthSp.getText().toString())
                .expireYear(creditCardDetailsBinding.yearSp.getText().toString())

                .verificationCode(creditCardDetailsBinding.cvvET.getText().toString())
                .build();

        cardClient.vault(creditCard, vault_url).enqueue(new CreditCardVaultCall.Callback() {
            @Override
            public void onResponse(@NonNull String token) {

                String s = token;
                new ApiClass().payWithCreditCard(getActivity(), checkoutId, amount, token, input, new PaymentInterface() {
                    @Override
                    public void onPayment(Storefront.Payment response, ID checkout_id) {
                        boolean paymentReady = response.getReady();
                        boolean paymentReady1 = response.getTest();
                        String getAmount = String.valueOf(response.getAmount());

                        String pay_id = String.valueOf(response.getId());
                        new ApiClass().completeCheckoutViaCreditCard(getActivity(), checkout_id, new CheckoutInterface() {
                            @Override
                            public void onSucessfullCheckout(Storefront.Checkout checkout) {

                                myHandler.sendEmptyMessage(DO_THAT);
                            }

                            @Override
                            public void onFailure() {
                                myHandler.sendEmptyMessage(DO_THAT);

                            }

                            @Override
                            public void onError(String error) {
                                myHandler.sendEmptyMessage(DO_THAT);

                            }
                        });

                    }

                    @Override
                    public void onFailure(String error) {

                    }

                    @Override
                    public void onError(String error) {

                    }
                });
                // proceed to complete checkout with token
            }

            @Override
            public void onFailure(@NonNull IOException error) {
                // handle error
            }
        });


    }

    private void getDataFromBundle() {
        Bundle bundle = getArguments();


        if (bundle != null) {

            chceckout_id = (ID) bundle.getSerializable("ID");
            input = (Storefront.MailingAddressInput) bundle.getSerializable("address_input");
            getTotalPrice = (BigDecimal) bundle.getSerializable("amount");


        }


    }


}

