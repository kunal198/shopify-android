package com.brst.shopifyapplication.HelperClasses;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.brst.shopifyapplication.Beans.CartBean;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by brst-pc93 on 11/24/16.
 */

public class MySharedPreferences {
    private static MySharedPreferences instance = null;

    public final String PreferenceName = "shopify_pref";

    public static MySharedPreferences getInstance() {
        if (instance == null) {
            instance = new MySharedPreferences();
        }
        return instance;
    }


    public SharedPreferences getPreference(Context context) {
        return context.getSharedPreferences(PreferenceName, Activity.MODE_PRIVATE);
    }


    public void storeData(Context context, String key, String value) {

        SharedPreferences.Editor editor = getPreference(context).edit();
        editor.putString(key, value);
        editor.apply();


    }

    public void storeSerializableData(Context context, String key, String value) {

        SharedPreferences.Editor editor = getPreference(context).edit();
        editor.putString(key, value);
        editor.apply();


    }

    public void storeSerializableListData(Context context, ArrayList<CartBean> value) {

        SharedPreferences.Editor editor = getPreference(context).edit();
        Gson gson = new Gson();
        String json = gson.toJson(value);
        editor.putString("serilizableObject", json);
        editor.apply();


    }


    public ArrayList<CartBean> getSerializableListData(Context context) {
        Gson gson = new Gson();
        String json = getPreference(context).getString("serilizableObject", null);
        Type type = new TypeToken<ArrayList<CartBean>>() {
        }.getType();

        ArrayList<CartBean> cartBeans = gson.fromJson(json, type);
        return cartBeans;
    }


    public void clearDataListData(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PreferenceName, Activity.MODE_PRIVATE);
        if (preferences != null)
            preferences.edit().remove("serilizableObject").apply();

    }


    public String getData(Context context, String key) {

        return getPreference(context).getString(key, null);
    }




}

