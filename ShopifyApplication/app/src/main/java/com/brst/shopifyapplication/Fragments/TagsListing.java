package com.brst.shopifyapplication.Fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Adaptors.TagsAdapter;
import com.brst.shopifyapplication.ApiImplementation.ApiClass;
import com.brst.shopifyapplication.Interfaces.CommonInterface;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.databinding.LayoutTagsListBinding;
import com.shopify.buy3.Storefront;

import java.util.ArrayList;
import java.util.List;


public class TagsListing extends BaseFragment {
    private final static int UPDATE_DATA = 0;
    private final static int DO_THAT = 1;
    private final static int START_DIALOG = 2;
    static TagsListing productFragment;

    TagsAdapter tagsAdapter;
    View view;
    LayoutTagsListBinding layoutTagsListBinding;
    List<Storefront.Article> articleList;

    public static TagsListing getInstance() {
        if (productFragment == null) {
            return productFragment = new TagsListing();
        }
        return productFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //  if (view == null) {
        layoutTagsListBinding = DataBindingUtil.inflate(
                inflater, R.layout.layout_tags_list, container, false);
        view = layoutTagsListBinding.getRoot();
        getActivity().setTitle("Tags");

        setAdpater();

        ((HomeActivity) getActivity()).setExpansion(true, false, 0);

        fetchTags();
        return view;
    }

    private void fetchTags() {
        myHandler.sendEmptyMessage(START_DIALOG);
        new ApiClass().fetchArticles(getActivity(), new CommonInterface<Storefront.Article>() {
            @Override
            public void onSuccess(List<Storefront.Article> list) {

                articleList = list;

                myHandler.sendEmptyMessage(UPDATE_DATA);

                myHandler.sendEmptyMessage(DO_THAT);
            }

            @Override
            public void onFailure(String message) {

                myHandler.sendEmptyMessage(DO_THAT);

            }

            @Override
            public void onError(String message) {
                myHandler.sendEmptyMessage(DO_THAT);

            }
        });
    }


    private final Handler myHandler = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message msg) {
            final int what = msg.what;
            switch (what) {
                case UPDATE_DATA:

                    if (articleList.get(0).getTags().size() > 0) {
                        tagsAdapter.adddata(articleList);

                    }

                    break;
                case DO_THAT:
                    dismissProgressDialog(getActivity());

                    break;
                case START_DIALOG:
                    initProgressDialog(getActivity());

                    break;
            }
        }
    };


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private void setAdpater() {

        articleList = new ArrayList<>();
        layoutTagsListBinding.tagsListRV.setHasFixedSize(true);
        tagsAdapter = new TagsAdapter(getActivity(), articleList, TagsListing.this, layoutTagsListBinding.tagsListRV);
        layoutTagsListBinding.tagsListRV.setAdapter(tagsAdapter);

    }


    //https://6a45267ae59af22a49957eb05f32cf3b:81999f1b7aabef1d747a98ae86d0e243@getshopifyapp.myshopify.com/admin/pages.json


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.search);
        MenuItem item1 = menu.findItem(R.id.sort);
        MenuItem item12 = menu.findItem(R.id.bag);
        MenuItem logout = menu.findItem(R.id.logout);
        MenuItem mode = menu.findItem(R.id.mode);
        mode.setVisible(false);
        item12.setVisible(false);
        item.setVisible(false);
        item1.setVisible(false);
        logout.setVisible(false);
    }

}

