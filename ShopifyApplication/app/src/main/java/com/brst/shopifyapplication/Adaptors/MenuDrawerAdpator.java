package com.brst.shopifyapplication.Adaptors;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Fragments.ProductStaggeredFragment;
import com.brst.shopifyapplication.R;
import com.shopify.buy3.Storefront;

import java.util.List;


/**
 * Created by brst-pc89 on 9/5/17.
 */

public class MenuDrawerAdpator extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    int TYPE_HEADER = 0;
    int TYPE_ITEM = 1;
    List<Storefront.Collection> data_list;
    Context context;

    public MenuDrawerAdpator(Context context, List<Storefront.Collection> data_list) {

        this.context = context;
        this.data_list = data_list;


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView;

        if (viewType == TYPE_ITEM) {
            itemView = LayoutInflater.from(context).inflate(R.layout.navigation_menu_item_view, parent, false);
            return new ItemViewHolder(itemView);

        } else {
            itemView = LayoutInflater.from(context).inflate(R.layout.navigation_header, parent, false);
            return new HeaderViewHolder(itemView);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        Storefront.Collection categoryBean = data_list.get(position);

        if (holder instanceof ItemViewHolder) {
            String myString=categoryBean.getTitle();
            String upperString = myString.substring(0,1).toUpperCase() + myString.substring(1);
            ((ItemViewHolder) holder).categoryNameTV.setText(upperString);
        } else if (holder instanceof HeaderViewHolder) {
            //((HeaderViewHolder) holder).viewHeader.setVisibility(View.GONE);
            if (position == 0) {
                ((HeaderViewHolder) holder).viewHeader.setVisibility(View.GONE);
            } else {

                ((HeaderViewHolder) holder).viewHeader.setVisibility(View.VISIBLE);
                ((HeaderViewHolder) holder).headerViewTV.setText("Shop More");
            }

        }


    }

    @Override
    public int getItemCount() {
        return data_list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        } else {
            return TYPE_ITEM;
        }

    }

    private boolean isPositionHeader(int position) {
        return data_list.get(position) == null;
    }

    public void addList(List<Storefront.Collection> data_list) {
        this.data_list = data_list;
        ((HomeActivity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {

                notifyDataSetChanged();
            }
        });
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView categoryNameTV;
        ImageView subCategoryArrowIV;

        public ItemViewHolder(View itemView) {
            super(itemView);

            categoryNameTV = itemView.findViewById(R.id.categoryNameTV);
            subCategoryArrowIV = itemView.findViewById(R.id.subCategoryArrowIV);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((HomeActivity) context).activityHomeBinding.drawerLayout.closeDrawers();
                    Bundle bundle = new Bundle();
                    bundle.putString("collection_id", String.valueOf(data_list.get(getAdapterPosition()).getId()));
                    bundle.putString("title", String.valueOf(data_list.get(getAdapterPosition()).getTitle()));
                    bundle.putInt("pos", getAdapterPosition());
                    ((HomeActivity) context).setVisibilityOfSearch(true);

                    //  ProductFragment productFragment = new ProductFragment();
                    ProductStaggeredFragment productFragment = new ProductStaggeredFragment();
                    //   productFragment.setData(data_list.get(getAdapterPosition()));

                    ((HomeActivity) context).fragmentTransaction(productFragment, R.id.container, bundle);
                }
            });
        }
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {

        TextView headerViewTV;
        View viewHeader;

        public HeaderViewHolder(View itemView) {
            super(itemView);

            headerViewTV = itemView.findViewById(R.id.headerViewTV);
            viewHeader = itemView.findViewById(R.id.viewHeader);

        }
    }


}
