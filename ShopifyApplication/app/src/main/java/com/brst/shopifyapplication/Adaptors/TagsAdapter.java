package com.brst.shopifyapplication.Adaptors;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Fragments.ProductStaggeredFromTagsFragment;
import com.brst.shopifyapplication.R;
import com.shopify.buy3.Storefront;

import java.util.List;

/**
 * Created by brst-pc89 on 12/30/17.
 */

public class TagsAdapter extends RecyclerView.Adapter<TagsAdapter.ViewHolder> {

    Context context;
    List<Storefront.Article> categoryList;
    Fragment fragment;

    public TagsAdapter(Context context, List<Storefront.Article> categoryList, Fragment fragment, RecyclerView recyclerView) {

        this.context = context;
        this.categoryList = categoryList;
        this.fragment = fragment;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // return null;
        View view = LayoutInflater.from(context).inflate(R.layout.layout_row_tags, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.title.setText(categoryList.get(0).getTags().get(position));
    }


    @Override
    public int getItemCount() {

        if (categoryList.size() > 0) {
            return categoryList.get(0).getTags().size();
        }
        return 0;
    }

    public void adddata(List<Storefront.Article> categoryList) {
        this.categoryList = categoryList;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;

        public ViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    Bundle bundle = new Bundle();
                    bundle.putString("tag", categoryList.get(0).getTags().get(getAdapterPosition()));

                    ((HomeActivity)context).fragmentTransaction(ProductStaggeredFromTagsFragment.getInstance(),R.id.container,bundle);


                }
            });
        }
    }
}
