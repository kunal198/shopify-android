package com.brst.shopifyapplication.Fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Adaptors.CmsListingAdapter;
import com.brst.shopifyapplication.Interfaces.ShopifyApiInterface;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.RetrofitPackage.ApiClient;
import com.brst.shopifyapplication.databinding.LayoutCmsListBinding;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CmsListing extends BaseFragment {

    static CmsListing productFragment;

    CmsListingAdapter cmsListingAdapter;
    View view;
    LayoutCmsListBinding layoutCmsBinding;
    JsonArray data_JsonArrays;

    public static CmsListing getInstance() {
        if (productFragment == null) {
            return productFragment = new CmsListing();
        }
        return productFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //  if (view == null) {
        layoutCmsBinding = DataBindingUtil.inflate(
                inflater, R.layout.layout_cms_list, container, false);
        view = layoutCmsBinding.getRoot();
        getActivity().setTitle("");

        setAdpater();
        callApi();
        ((HomeActivity) getActivity()).setExpansion(true, false, 0);
        getActivity().setTitle("Cms Pages");
        return view;
    }

    private void setAdpater() {
        data_JsonArrays = new JsonArray();
        layoutCmsBinding.cmsListRV.setHasFixedSize(true);
        cmsListingAdapter = new CmsListingAdapter(getActivity(), data_JsonArrays, CmsListing.this, layoutCmsBinding.cmsListRV);
        layoutCmsBinding.cmsListRV.setAdapter(cmsListingAdapter);

    }


    //https://6a45267ae59af22a49957eb05f32cf3b:81999f1b7aabef1d747a98ae86d0e243@getshopifyapp.myshopify.com/admin/pages.json

    private void callApi() {
        ShopifyApiInterface shopifyApiInterface = ApiClient.getClient().create(ShopifyApiInterface.class);
//        Call<Example> call = shopifyApiInterface.getPages();
        initProgressDialog(getContext());
        Call<JsonObject> call = shopifyApiInterface.getPagesListing(getAuthToken());
        call.enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dismissProgressDialog(getContext());
                JsonObject jsonObject = response.body().getAsJsonObject();
                JsonArray page_array = jsonObject.getAsJsonArray("pages");
                String a = page_array.get(0).getAsJsonObject().get("title").getAsString();

                cmsListingAdapter.adddata(page_array);

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dismissProgressDialog(getContext());

                String error = t.getMessage();
                Log.d("Error", t.getMessage());
            }
        });

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.search);
        MenuItem item1 = menu.findItem(R.id.sort);
        MenuItem item12 = menu.findItem(R.id.bag);
        MenuItem logout = menu.findItem(R.id.logout);
        MenuItem mode = menu.findItem(R.id.mode);
        mode.setVisible(false);
        item12.setVisible(false);
        item.setVisible(false);
        item1.setVisible(false);
        logout.setVisible(false);
    }

}

