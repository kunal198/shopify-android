package com.brst.shopifyapplication.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Adaptors.CollectionAdaptor;
import com.brst.shopifyapplication.R;

import java.util.ArrayList;
import java.util.List;

public class HomeFragmentViewThree extends Fragment {

    static HomeFragmentViewThree homeFragmentViewThree;
    RecyclerView recyclerViewKids, recyclerViewBrand, recyclerViewArrivals;
    View view;
    List<String> colllectionList = new ArrayList<>();

    public HomeFragmentViewThree() {
        // Required empty public constructor
    }

    public static HomeFragmentViewThree getInstance() {
        if (homeFragmentViewThree == null) {
            return homeFragmentViewThree = new HomeFragmentViewThree();
        }
        return homeFragmentViewThree;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.search);
        MenuItem item1 = menu.findItem(R.id.sort);
        MenuItem item12 = menu.findItem(R.id.bag);
        MenuItem logout = menu.findItem(R.id.logout);
        MenuItem mode = menu.findItem(R.id.mode);
        mode.setVisible(false);
        item12.setVisible(false);
        item.setVisible(false);
        item1.setVisible(false);
        logout.setVisible(false);
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //   if (view == null) {

        view = inflater.inflate(R.layout.fragment_home_three, container, false);

        setUpViews();

        setUpAdaptor();
        ((HomeActivity) getActivity()).setExpansion(true, false, 0);

        //    }
        return view;
    }

    private void setUpAdaptor() {

        colllectionList.add("Minimum 50% Off");
        colllectionList.add("Flat 50% Off");
        colllectionList.add("Upto 60% Off");
        colllectionList.add("Minimum 50% Off");
        colllectionList.add("Flat 50% Off");
        colllectionList.add("Upto 60% Off");
        colllectionList.add("Minimum 50% Off");
        colllectionList.add("Flat 50% Off");
        colllectionList.add("Upto 60% Off");


        CollectionAdaptor collectionAdaptor = new CollectionAdaptor(getContext(), colllectionList);
        RecyclerView.LayoutManager layoutManagerKids = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewKids.setLayoutManager(layoutManagerKids);
        recyclerViewKids.setAdapter(collectionAdaptor);

        RecyclerView.LayoutManager layoutManagerBrand = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);

        recyclerViewBrand.setLayoutManager(layoutManagerBrand);
        recyclerViewBrand.setAdapter(collectionAdaptor);

        RecyclerView.LayoutManager layoutManagerArrival = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerViewArrivals.setLayoutManager(layoutManagerArrival);
        recyclerViewArrivals.setAdapter(collectionAdaptor);
    }

    private void setUpViews() {

        recyclerViewKids = view.findViewById(R.id.recyclerViewKids);
        recyclerViewBrand = view.findViewById(R.id.recyclerViewBrand);
        recyclerViewArrivals = view.findViewById(R.id.recyclerViewArrivals);
    }


}
