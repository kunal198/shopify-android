package com.brst.shopifyapplication.Beans;

/**
 * Created by brst-pc89 on 9/5/17.
 */

public class CategoryBean {
    int type;
    String headername, categoryname;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getHeadername() {
        return headername;
    }

    public void setHeadername(String headername) {
        this.headername = headername;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }
}
