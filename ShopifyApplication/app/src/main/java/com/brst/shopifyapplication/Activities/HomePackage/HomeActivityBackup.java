package com.brst.shopifyapplication.Activities.HomePackage;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;

import com.brst.shopifyapplication.Activities.ParentActivity;
import com.brst.shopifyapplication.ApiImplementation.ApiClass;
import com.brst.shopifyapplication.Interfaces.ProductsInterface;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.databinding.ActivityHomeBinding;
import com.shopify.buy3.Storefront;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HomeActivityBackup extends ParentActivity
       /* implements DuoMenuView.OnMenuClickListener */{
    public static int mode = 0;
    static ProductsInterface productsInterface;
    static SwipeRefreshLayout swipeRefreshLayout;
    static boolean visibilityOfSearch = false;
    public ActivityHomeBinding activityHomeBinding;
    Dialog dialog;
    static public List<Storefront.Collection> collectionList;
    ArrayList<String> mTitles;
    int TYPE_ITEM = 1, TYPE_HEADER = 0;
    ApiClass apiClass;
    private String Tag = "HomeActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityHomeBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        mTitles = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.menuOptions)));
   /*      activityHomeBinding.drawerLayout.closeDrawer();
        fragmentTransaction(HomeFragment.getInstance(), R.id.container);
       handleToolbar();
        apiClass = new ApiClass();
        // Handle menu actions
        handleMenu();

        // Handle drawer actions
        handleDrawer();

        // Show main fragment in container


        // setTitle("Fashion Feed");
        setupTabIcons();
        setBottomNavigate();
    */}


  /*  @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(Tag, "onActivityResult" + requestCode + " " + resultCode);


    }


    public void setExpansion(boolean expansion, boolean navigation) {
        activityHomeBinding.appBar.setExpanded(expansion, expansion);
        if (!expansion) {

            activityHomeBinding.bottomNavigation.animate()
                    .translationY(activityHomeBinding.bottomNavigation.getHeight());


        } else {
            activityHomeBinding.bottomNavigation.animate()
                    .translationY(activityHomeBinding.bottomNavigation.getHeight())
                    .alpha(1.0f);

        }
        activityHomeBinding.bottomNavigation.setVisibility(navigation ? View.VISIBLE : View.GONE);

    }


    private void handleToolbar() {
        setSupportActionBar(activityHomeBinding.toolbar);
    }

    private void handleDrawer() {
        DuoDrawerToggle duoDrawerToggle = new DuoDrawerToggle(this,
                activityHomeBinding.drawerLayout,
                activityHomeBinding.toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);

        activityHomeBinding.drawerLayout.setDrawerListener(duoDrawerToggle);
        duoDrawerToggle.syncState();

    }


    @Override
    protected void onResume() {
        super.onResume();
        String currency = MySharedPreferences.getInstance().getData(ShopifyApplication.getAppContext(), Constants.SELECTED_CURRENCY);

        if (currency == null) {

            new ApiClass().fetchShopPolicy(ShopifyApplication.getAppContext(), null);
        }
    }

    private void handleMenu() {
        collectionList = new ArrayList<>();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(HomeActivityBackup.this);


        activityHomeBinding.drawerMenuView.setLayoutManager(mLayoutManager);
        activityHomeBinding.drawerMenuView.setItemAnimator(new DefaultItemAnimator());

        MenuDrawerAdpator menuDrawerAdpator = new MenuDrawerAdpator(HomeActivityBackup.this, collectionList);
        activityHomeBinding.drawerMenuView.setAdapter(menuDrawerAdpator);
        ApiClass apiClass = new ApiClass();
        apiClass.fetchCategories(this, new ProductListingInterface() {
            @Override
            public void onSuccess(List<Storefront.Collection> collections) {
                collectionList = collections;

                menuDrawerAdpator.addList(collections);


            }

            @Override
            public void onFailure(String error) {

            }

            @Override
            public void onError() {

            }
        });
    }


    private void setBottomNavigate() {


        activityHomeBinding.bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {


            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.category)
                    fragmentTransaction(HomeFragment.getInstance(), R.id.container);
                if (item.getItemId() == R.id.percentage)
                    fragmentTransaction(HomeFragmentViewOne.getInstance(), R.id.container);
                if (item.getItemId() == R.id.like)
                    fragmentTransaction(HomeFragmentViewTwo.getInstance(), R.id.container);
                if (item.getItemId() == R.id.bag)
                    fragmentTransaction(CartFragment.getInstance(), R.id.container);
                if (item.getItemId() == R.id.user)
                    fragmentTransaction(CmsListing.getInstance(), R.id.container);
                return true;
            }
        });
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count < 2) {
            finish();
        }
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search_menu, menu);
        return true;
    }


    public void setVisibilityOfSearch(boolean visibilityOfSearch) {

        this.visibilityOfSearch = visibilityOfSearch;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // this takes the user 'back', as if they pressed the left-facing triangle icon on the main android toolbar.
                // if this doesn't work as desired, another possibility is to call `finish()` here.
                Toast.makeText(HomeActivityBackup.this, "alacalccca", Toast.LENGTH_LONG).show();
                onBackPressed();
                return true;

            case R.id.sort:
                initSortDialog();


                return true;


            case R.id.logout:

                MySharedPreferences.getInstance().storeData(ShopifyApplication.getAppContext(), Constants.ACCESS_TOKEN, null);
                MySharedPreferences.getInstance().storeData(ShopifyApplication.getAppContext(), Constants.EMAIL, null);
                MySharedPreferences.getInstance().storeData(ShopifyApplication.getAppContext(), Constants.SELECTED_CURRENCY, null);

                setOnActivityTransfer(LoginActivity.class);
                finish();


                return true;
            case R.id.bag:

                productsInterface.onModeChange(mode);


                return true;
            case android.R.id.home:
                Toast.makeText(HomeActivity.this, "ff", Toast.LENGTH_SHORT).show();
                return true;


            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void initSortDialog() {


        dialog = new Dialog(this, android.R.style.Theme_Translucent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.sort_dialog);


        RadioButton low = dialog.findViewById(R.id.lowToHigh);
        RadioButton high = dialog.findViewById(R.id.highToLow);
        RadioButton deafultRB = dialog.findViewById(R.id.deafultRB);
        RadioButton az = dialog.findViewById(R.id.az);
        RadioButton za = dialog.findViewById(R.id.za);
        ImageView close = dialog.findViewById(R.id.close);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });


        low.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                dialog.dismiss();
                if (productsInterface != null) {
                    productsInterface.onSort("low");
                }
            }
        });
        high.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                dialog.dismiss();
                if (productsInterface != null) {
                    productsInterface.onSort("high");
                }
            }
        });
        az.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                dialog.dismiss();
                if (productsInterface != null) {
                    productsInterface.onSort("az");
                }
            }
        });
        za.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                dialog.dismiss();
                if (productsInterface != null) {
                    productsInterface.onSort("za");
                }
            }
        });
        deafultRB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                dialog.dismiss();
                if (productsInterface != null) {
                    productsInterface.onSort("default");
                }
            }
        });


        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        //   menu.findItem(R.id.search).setVisible(visibilityOfSearch);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {

                if (productsInterface != null) {
                    String title = "title=" + s;
                    // String title = "tags:" + "Accessories";

                    apiClass.fetchCategoriesWithPaginationOnSearch(HomeActivityBackup.this, HomeActivityBackup.this, "", "", false, productsInterface, 0, s, swipeRefreshLayout);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {


                return false;
            }
        });


        return true;
    }

    public void getInstanceFromProductFragment(ProductsInterface productsInterface, SwipeRefreshLayout swipeRefreshLayout) {
        this.productsInterface = productsInterface;
        this.swipeRefreshLayout = swipeRefreshLayout;

    }


    @Override
    public void onFooterClicked() {

    }

    @Override
    public void onHeaderClicked() {

    }


    @Override
    public void onOptionClicked(int position, Object objectClicked) {
        // setTitle(mTitles.get(position));

        // Set the right options selected
        mMenuAdapter.setViewSelected(position, true);

        // Navigate to the right fragment
        switch (position) {
            default:
                fragmentTransaction(HomeFragment.getInstance(), R.id.container);
                break;
        }

        // Close the drawer
        activityHomeBinding.drawerLayout.closeDrawer();
    }


    private void setupTabIcons() {


    }
*/

}
