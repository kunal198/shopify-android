package com.brst.shopifyapplication.Fragments;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Adaptors.ShippingMethodAdpater;
import com.brst.shopifyapplication.ApiImplementation.ApiClass;
import com.brst.shopifyapplication.HelperClasses.GeometricProgressView;
import com.brst.shopifyapplication.Interfaces.CheckoutInterface;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.databinding.LayoutShippingMethodBinding;
import com.google.android.gms.wallet.MaskedWallet;
import com.shopify.buy3.Storefront;
import com.shopify.buy3.pay.PayHelper;
import com.shopify.graphql.support.ID;

import java.util.ArrayList;
import java.util.List;


public class ShippingMethodFragment extends BaseFragment {
    static ShippingMethodFragment productFragment;
    Storefront.MailingAddressInput input;

    View view;
    LayoutShippingMethodBinding layoutShippingMethodBinding;
    ShippingMethodAdpater shippingMethodAdpater;
    List<Storefront.ShippingRate> data_list;
    String subtotalPrice = null, shipping_charges = "",shipping_handle="",tax="",total_price="";
    ID checkoutId;
    String discount_on_order = "";
    private String Tag = "ShippingMethodFragment";

    /*public void setData(Storefront.Collection collection) {
        data = new ArrayList<>();
        data.add(collection);

    }
*/
    public static ShippingMethodFragment getInstance() {
        if (productFragment == null) {
            return productFragment = new ShippingMethodFragment();
        }
        return productFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // if (view == null) {
        layoutShippingMethodBinding = DataBindingUtil.inflate(
                inflater, R.layout.layout_shipping_method, container, false);
        view = layoutShippingMethodBinding.getRoot();

        initView();
        getActivity().setTitle("Checkout Process");
        addAdapter();
        getDataFromBundle();


        //    }
        setClickListner();
        ((HomeActivity) getActivity()).setExpansion(true, false, 0);
        layoutShippingMethodBinding.saveBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Bundle bundle = new Bundle();
                bundle.putSerializable("checkoutId", checkoutId);

                bundle.putString("totalPrice", total_price);
                bundle.putString("sub_total", subtotalPrice);
                bundle.putString("shipping_price", shipping_charges);
                bundle.putString("shipping_handle", shipping_handle);
                bundle.putString("tax", tax);
                bundle.putSerializable("input", input);

                fragmentTransaction(ShippingPaymentFragment.getInstance(), R.id.container, bundle);
            }
        });

        return view;
    }

    private void setClickListner() {
        layoutShippingMethodBinding.addressTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                replaceFragment(ShippingAddressFragment.getInstance(),getActivity());

            }
        });


/*

        layoutShippingMethodBinding.discountNameTV.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getRawX() >= (layoutShippingMethodBinding.discountNameTV.getRight() - layoutShippingMethodBinding.discountNameTV.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    // your action here
                    openShippingCharges(checkoutId);

                    return true;
                }

                return false;
            }
        });
*/


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(Tag, "onActivityResult" + requestCode + " " + resultCode);

        PayHelper.handleWalletResponse(requestCode, resultCode, data, new PayHelper.WalletResponseHandler() {

            @Override
            public void onWalletError(int requestCode, int errorCode) {
                Log.d(Tag, "onWalletError" + requestCode + " " + errorCode);

                // show error, errorCode is one of defined in com.google.android.gms.wallet.WalletConstants
            }

            @Override
            public void onMaskedWallet(final MaskedWallet maskedWallet) {
                Log.d(Tag, "maskedWallet" + requestCode + " " + resultCode);

                // show order confirmation screen to complete checkout
                // update checkout with shipping address from Masked Wallet
                // fetch shipping rates
            }
        });
    }

    public void initProgressDialogVisible(Context context) {


        layoutShippingMethodBinding.progressBarPB.setType(GeometricProgressView.TYPE.TRIANGLE);
        layoutShippingMethodBinding.progressBarPB.setFigurePaddingInDp(1);
        layoutShippingMethodBinding.progressBarPB.setNumberOfAngles(10);
        layoutShippingMethodBinding.progressBarPB.setVisibility(View.VISIBLE);

    }

    public void dismissProgressDialogGone(Context context) {
        Log.d("progressdialog", "dialog");
        if (layoutShippingMethodBinding.progressBarPB.getVisibility() == View.VISIBLE) {
            layoutShippingMethodBinding.progressBarPB.setVisibility(View.GONE);
        }
    }

/*
    private void addDiscountCoupanToCheckOut() {

        layoutShippingMethodBinding.applyBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String discount_code = layoutShippingMethodBinding.discountCodeET.getText().toString();

                hideKeyboard(getActivity());
                if (!discount_code.isEmpty()) {
                    initProgressDialog(getActivity());
                    new ApiClass().applyGiftCard(getActivity(), checkoutId, discount_code, new CheckoutInterface() {
                        @Override
                        public void onSucessfullCheckout(Storefront.Checkout checkout) {
                            dismissProgressDialog(getActivity());
                            //  String checkoutWebUrl = checkout.getWebUrl();
                            ID checkoutId = checkout.getId();

                            String discoupan_text = layoutShippingMethodBinding.discountCodeET.getText().toString();
                            String getPaymentDue = String.valueOf(checkout.getPaymentDue());
                            String getSubtotalPrice = String.valueOf(checkout.getSubtotalPrice());
                            String getTotalPrice = String.valueOf(checkout.getTotalPrice());
                            if (subtotalPrice.equals(getPaymentDue)) {
                                showMessage("Wrong coupan");
                            } else {
                                showMessage("Coupan applied sucessfully");
                                setLabel(discoupan_text);
                                double discount = Double.parseDouble(subtotalPrice) - Double.parseDouble(getPaymentDue);
                                showText(String.valueOf(discount), getPaymentDue);
                                discount_on_order = String.valueOf(discount);

                            }


                        }

                        @Override
                        public void onFailure() {
                            dismissProgressDialog(getActivity());

                        }

                        @Override
                        public void onError(String error) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
                                }
                            });
                            dismissProgressDialog(getActivity());

                        }
                    });
                } else {
                    Toast.makeText(getActivity(), "Please enter the discount code", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
*/

    /*private void setLabel(String discoupan_text) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                layoutShippingMethodBinding.discountNameTV.setText(discoupan_text);
                layoutShippingMethodBinding.discountCodeET.getText().clear();

            }
        });
    }

    private void showMessage(String msg) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

            }
        });
    }*/

   /* private void showText(String discount, String paymentdue) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                layoutShippingMethodBinding.discountLL.setVisibility(View.VISIBLE);
                layoutShippingMethodBinding.discountCoupan.setText("-"+getCurrency() + discount);
                double due = Double.parseDouble(paymentdue) + Double.parseDouble(shipping_charges);
                layoutShippingMethodBinding.totalTV.setText(getCurrency() + due);

            }
        });
    }*/

    private void addAdapter() {
        data_list = new ArrayList<>();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());


        layoutShippingMethodBinding.shippingMethodRV.setLayoutManager(mLayoutManager);
        layoutShippingMethodBinding.shippingMethodRV.setItemAnimator(new DefaultItemAnimator());
        layoutShippingMethodBinding.shippingMethodRV.setHasFixedSize(true);

        layoutShippingMethodBinding.shippingMethodRV.setEmptyView(layoutShippingMethodBinding.emptyView);
        layoutShippingMethodBinding.shippingMethodRV.setRecyclerView(layoutShippingMethodBinding.shippingMethodRV);
        shippingMethodAdpater = new ShippingMethodAdpater(getActivity(), ShippingMethodFragment.this, layoutShippingMethodBinding.shippingMethodRV, data_list);

        layoutShippingMethodBinding.shippingMethodRV.setAdapter(shippingMethodAdpater);


    }


    public void setShippingPrice(int pos) {
        if (data_list != null && data_list.size() > 0) {
            String ship_price = String.valueOf(data_list.get(pos).getPrice());
            String handle = String.valueOf(data_list.get(pos).getHandle());

            shipping_charges = ship_price;
            shipping_handle = handle;


        }
    }


    public void openShippingCharges(ID checkoutId) {


        initProgressDialogVisible(getActivity());
        new ApiClass().getShippingCharges(getActivity(), checkoutId, new CheckoutInterface() {
            @Override
            public void onSucessfullCheckout(Storefront.Checkout checkout) {
                data_list = checkout.getAvailableShippingRates().getShippingRates();

                //   List<Storefront.AppliedGiftCard> appliedGiftCard = checkout.getAppliedGiftCards();
                shippingMethodAdpater.addList(data_list);
                //  setShippingPrice(0);

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        dismissProgressDialogGone(getActivity());

                    }
                });


            }

            @Override
            public void onFailure() {

            }

            @Override
            public void onError(String error) {

            }
        });


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.search);
        MenuItem item1 = menu.findItem(R.id.sort);
        MenuItem item12 = menu.findItem(R.id.bag);
        MenuItem logout = menu.findItem(R.id.logout);
        MenuItem mode = menu.findItem(R.id.mode);
        mode.setVisible(false);
        item12.setVisible(false);
        item.setVisible(false);
        item1.setVisible(false);
        logout.setVisible(false);
    }

    private void initView() {


    }

    private void getDataFromBundle() {
        Bundle bundle = getArguments();

        if (bundle != null) {
            checkoutId = (ID) bundle.getSerializable("checkoutId");
            subtotalPrice = bundle.getString("sub_total");
            total_price = bundle.getString("totalPrice");
            tax = bundle.getString("tax");
            input = (Storefront.MailingAddressInput) bundle.getSerializable("input");
            openShippingCharges(checkoutId);
                /*layoutShippingMethodBinding.subtotalTV.setText(subtotalPrice != null ? getCurrency() + subtotalPrice : "");
*/


        }

    }
}