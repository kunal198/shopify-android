package com.brst.shopifyapplication.Interfaces;

import com.shopify.buy3.Storefront;
import com.shopify.graphql.support.ID;

/**
 * Created by brst-pc20 on 12/14/17.
 */

public interface PaymentInterface {


    public void onPayment(Storefront.Payment payment, ID checkout_id);

    public void onFailure(String error);

    public void onError(String error);


}
