package com.brst.shopifyapplication.Interfaces;

import com.shopify.buy3.Storefront;

import java.util.List;

/**
 * Created by brst-pc20 on 12/4/17.
 */

public interface ProductListingInterface {

    public void onSuccess(List<Storefront.Collection> collections);

    public void onFailure(String s);

    public void onError();


}
