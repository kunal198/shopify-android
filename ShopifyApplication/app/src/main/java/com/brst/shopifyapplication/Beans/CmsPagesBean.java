package com.brst.shopifyapplication.Beans;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by brst-pc20 on 1/1/18.
 */

public class CmsPagesBean {


    @SerializedName("pages")

    private List<Page> pages = null;

    public List<Page> getPages() {
        return pages;
    }

    public void setPages(List<Page> pages) {
        this.pages = pages;
    }


    public class Page {

        @SerializedName("id")

        private Integer id;
        @SerializedName("title")

        private String title;
        @SerializedName("shop_id")

        private Integer shopId;
        @SerializedName("handle")

        private String handle;
        @SerializedName("body_html")

        private String bodyHtml;
        @SerializedName("author")

        private String author;
        @SerializedName("created_at")

        private String createdAt;
        @SerializedName("updated_at")

        private String updatedAt;
        @SerializedName("published_at")

        private String publishedAt;
        @SerializedName("template_suffix")

        private String templateSuffix;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Integer getShopId() {
            return shopId;
        }

        public void setShopId(Integer shopId) {
            this.shopId = shopId;
        }

        public String getHandle() {
            return handle;
        }

        public void setHandle(String handle) {
            this.handle = handle;
        }

        public String getBodyHtml() {
            return bodyHtml;
        }

        public void setBodyHtml(String bodyHtml) {
            this.bodyHtml = bodyHtml;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getPublishedAt() {
            return publishedAt;
        }

        public void setPublishedAt(String publishedAt) {
            this.publishedAt = publishedAt;
        }

        public String getTemplateSuffix() {
            return templateSuffix;
        }

        public void setTemplateSuffix(String templateSuffix) {
            this.templateSuffix = templateSuffix;
        }

    }

}
