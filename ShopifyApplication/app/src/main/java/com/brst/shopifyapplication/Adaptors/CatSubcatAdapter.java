package com.brst.shopifyapplication.Adaptors;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.brst.shopifyapplication.Adaptors.ViewHolders.CategoryViewHolder;
import com.brst.shopifyapplication.Adaptors.ViewHolders.SubCategoryViewHolder;
import com.brst.shopifyapplication.Beans.CategorySubCategoryBean;
import com.brst.shopifyapplication.Beans.MainCatSubCategoryBean;
import com.brst.shopifyapplication.R;

import java.util.List;

public class CatSubcatAdapter extends ExpandableRecyclerAdapter<MainCatSubCategoryBean, CategorySubCategoryBean, CategoryViewHolder, SubCategoryViewHolder> {

    private static final int PARENT_NORMAL = 1;
    private static final int CHILD_NORMAL = 3;

    private LayoutInflater mInflater;
    private List<MainCatSubCategoryBean> mRecipeList;

    public CatSubcatAdapter(Context context, @NonNull List<MainCatSubCategoryBean> recipeList) {
        super(recipeList);
        mRecipeList = recipeList;
        mInflater = LayoutInflater.from(context);
    }

    @UiThread
    @NonNull
    @Override
    public CategoryViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        View recipeView;
        switch (viewType) {
            default:
            case PARENT_NORMAL:

                recipeView = mInflater.inflate(R.layout.category_view, parentViewGroup, false);
                break;
        }
        return new CategoryViewHolder(recipeView);
    }

    @UiThread
    @NonNull
    @Override
    public SubCategoryViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        View ingredientView;
        switch (viewType) {
            default:
            case CHILD_NORMAL:

                ingredientView = mInflater.inflate(R.layout.subcategory_view, childViewGroup, false);
                break;
        }
        return new SubCategoryViewHolder(ingredientView);
    }

    @Override
    public void onBindParentViewHolder(CategoryViewHolder parentViewHolder, int parentPosition, MainCatSubCategoryBean parent) {
        parentViewHolder.bind(parent);
    }

    @Override
    public void onBindChildViewHolder(SubCategoryViewHolder childViewHolder, int parentPosition, int childPosition, CategorySubCategoryBean child) {
        childViewHolder.bind(child);

    }


    @Override
    public int getParentViewType(int parentPosition) {

        return PARENT_NORMAL;

    }

    @Override
    public int getChildViewType(int parentPosition, int childPosition) {

        return CHILD_NORMAL;
    }

    @Override
    public boolean isParentViewType(int viewType) {
        return viewType == PARENT_NORMAL;
    }

}
