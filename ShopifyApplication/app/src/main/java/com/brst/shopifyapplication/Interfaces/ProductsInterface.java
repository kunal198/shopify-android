package com.brst.shopifyapplication.Interfaces;

import com.shopify.buy3.Storefront;

import java.util.List;

/**
 * Created by brst-pc20 on 12/4/17.
 */

public interface ProductsInterface {

    public void onSuccess(List<Storefront.Product> collections, boolean morePages, boolean pagination, String cursor,String searched_string);

    public void onModeChange(int mode);

    public void onSort(String s);

    public void onFailure(String s);

    public void onError(String error);


}
