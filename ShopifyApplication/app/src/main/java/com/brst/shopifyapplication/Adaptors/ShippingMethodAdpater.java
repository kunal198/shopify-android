package com.brst.shopifyapplication.Adaptors;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.brst.shopifyapplication.Fragments.ShippingMethodFragment;
import com.brst.shopifyapplication.HelperClasses.Constants;
import com.brst.shopifyapplication.HelperClasses.MySharedPreferences;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.ShopifyApplication;
import com.shopify.buy3.Storefront;

import java.util.List;


/**
 * Created by brst-pc20 on 1/9/17.
 */

public class ShippingMethodAdpater extends RecyclerView.Adapter<ShippingMethodAdpater.MyViewHolder> {

    public int selectedPosition = 0;
    List<Storefront.ShippingRate> data_list;
    Context context;
    Fragment fragment;
    RecyclerView recyclerView;
    String currency;
    private boolean onBind;

    public ShippingMethodAdpater(Context context, Fragment fragment, RecyclerView recyclerView, List<Storefront.ShippingRate> data_list) {

        this.context = context;
        this.data_list = data_list;

        this.fragment = fragment;
        this.recyclerView = recyclerView;
        currency = MySharedPreferences.getInstance().getData(ShopifyApplication.getAppContext(), Constants.SELECTED_CURRENCY);


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.shipping_method_row, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        onBind = true;


        if (selectedPosition == position) {
            holder.shippingPointRB.setChecked(true);
        } else {
            holder.shippingPointRB.setChecked(false);

        }

         ((ShippingMethodFragment) fragment).setShippingPrice(selectedPosition);

        String price = String.valueOf(data_list.get(position).getPrice());
        holder.priceTV.setText(currency != null ? currency + price : "" + price);
        holder.title.setText(data_list.get(position).getTitle());
        holder.handle.setText(data_list.get(position).getHandle());
        onBind = false;

     /*   holder.shippingPointRB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                selectedPosition =position;
//                notifyDataSetChanged();
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return data_list.size();
    }

    public void addList(List<Storefront.ShippingRate> data) {
        this.data_list = data;
        fragment.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                notifyDataSetChanged();

            }
        });
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView priceTV, title, handle;
        CheckBox shippingPointRB;

        public MyViewHolder(View view) {
            super(view);


            priceTV = view.findViewById(R.id.priceTV);
            title = view.findViewById(R.id.title);
            handle = view.findViewById(R.id.handle);
            shippingPointRB = view.findViewById(R.id.shippingPointRB);

            shippingPointRB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (!onBind) {
                        // your process when checkBox changed
                        // ...
                        selectedPosition = getAdapterPosition();

                        notifyDataSetChanged();
                    }

                }
            });


        }
    }
}