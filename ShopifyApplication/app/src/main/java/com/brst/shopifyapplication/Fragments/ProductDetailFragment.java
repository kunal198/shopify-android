package com.brst.shopifyapplication.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.alexvasilkov.foldablelayout.FoldableListLayout;
import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Adaptors.ProductDetailFlipViewAdapter;
import com.brst.shopifyapplication.Beans.CartBean;
import com.brst.shopifyapplication.HelperClasses.MySharedPreferences;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.databinding.LayoutProductDetailBinding;
import com.google.gson.Gson;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.shopify.buy3.Storefront;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;


public class ProductDetailFragment extends BaseFragment {
    static List<Storefront.Product> productList;
    static ProductDetailFragment productFragment;
    String title = "", price = "", imageUrl = "";
    String Tag = "ProductDetailFragment";
    View view;
    boolean helperToStopCallForFirstTime = false;
    LayoutProductDetailBinding layoutProductDetailBinding;
    FoldableListLayout foldableListLayout;
    String product_variant_id = "", variant_name = "";
    ArrayList<CartBean> cartBeans;
    Storefront.Product product;
    String valueOfFirstSpinner = "", valueOfSecondSpinner = "", valueOfThirdSpinner = "";

    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.wtf("", "UTF-8 should always be supported", e);
            return "";
        }
    }

    public static ProductDetailFragment getInstance() {
        if (productFragment == null) {
            return productFragment = new ProductDetailFragment();
        }
        return productFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //  if (view == null) {
        layoutProductDetailBinding = DataBindingUtil.inflate(
                inflater, R.layout.layout_product_detail, container, false);
        view = layoutProductDetailBinding.getRoot();
        cartBeans = new ArrayList<>();
        //    initView();
        init(view);
        // }
        // getDataFromBundle();]

        product = productList.get(0);


        getOptions(product);
        ((HomeActivity) getActivity()).setExpansion(false, true, 0);

        //  }
        return view;
    }

    public void getOptions(Storefront.Product product) {
        if (!product.getOptions().get(0).getValues().get(0).equals("Default Title")) {
            for (int i = 0; i < product.getOptions().size(); i++) {

                ArrayList<String> color = new ArrayList<>();
                color.add(product.getOptions().get(i).getName());
                for (int k = 0; k < product.getOptions().get(i).getValues().size(); k++) {
                    color.add(product.getOptions().get(i).getValues().get(k));

                }

                createSpinner(color, i, product);

                Log.d(Tag, "name" + product.getOptions().get(i).getName());
                Log.d(Tag, "size" + product.getOptions().get(i).getValues().get(0) + "");
                Log.d(Tag, "size" + product.getOptions().size());


            }
        }

    }

    private void createSpinner(ArrayList<String> arrayList, int i, Storefront.Product product) {

        if (i == 0) {
            layoutProductDetailBinding.oneSp.setVisibility(View.VISIBLE);
            layoutProductDetailBinding.oneSp.setItems(arrayList);
            layoutProductDetailBinding.oneSp.setSelectedIndex(1);
            valueOfFirstSpinner = arrayList.get(1);

            layoutProductDetailBinding.oneSp.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                @Override
                public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                    valueOfFirstSpinner = arrayList.get(position);
                    if (helperToStopCallForFirstTime) {
                        getPositionOfSelectedData(product);
                    }
                }
            });

        }
        if (i == 1) {
            layoutProductDetailBinding.twoSP.setVisibility(View.VISIBLE);

            layoutProductDetailBinding.twoSP.setItems(arrayList);
            layoutProductDetailBinding.twoSP.setSelectedIndex(1);
            valueOfSecondSpinner = arrayList.get(1);

            layoutProductDetailBinding.twoSP.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                @Override
                public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                    valueOfSecondSpinner = arrayList.get(position);
                    if (helperToStopCallForFirstTime) {
                        getPositionOfSelectedData(product);
                    }
                }
            });

        }
        if (i == 2) {
            layoutProductDetailBinding.threeSP.setVisibility(View.VISIBLE);

            layoutProductDetailBinding.threeSP.setItems(arrayList);
            layoutProductDetailBinding.threeSP.setSelectedIndex(1);
            valueOfThirdSpinner = arrayList.get(1);
            layoutProductDetailBinding.threeSP.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                @Override
                public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                    valueOfThirdSpinner = arrayList.get(position);
                    if (helperToStopCallForFirstTime) {
                        getPositionOfSelectedData(product);
                    }
                }
            });
        }
        getPositionOfSelectedData(product);
        helperToStopCallForFirstTime = true;

    }

    private void getPositionOfSelectedData(Storefront.Product product) {
        for (int k = 0; k < product.getVariants().getEdges().size(); k++) {
            List<Storefront.SelectedOption> selectedOption = product.getVariants().getEdges().get(k).getNode().getSelectedOptions();

            for (int i = 0; i < selectedOption.size(); i++) {

                String name = selectedOption.get(i).getName();
                String value = selectedOption.get(i).getValue();

                if (i == 0 && valueOfFirstSpinner.equalsIgnoreCase(value) && selectedOption.size() == 1) {
                    variant_name = value;

                    setDataOnView(product, k);
                }
                if (i == 1 && valueOfFirstSpinner.equalsIgnoreCase(selectedOption.get(0).getValue()) && valueOfSecondSpinner.equalsIgnoreCase(value) && selectedOption.size() == 2) {
                    variant_name = selectedOption.get(0).getValue() + "/" + value;

                    setDataOnView(product, k);
                }
                if (i == 2 && valueOfFirstSpinner.equalsIgnoreCase(selectedOption.get(0).getValue()) && valueOfSecondSpinner.equalsIgnoreCase(selectedOption.get(1).getValue()) && valueOfThirdSpinner.equalsIgnoreCase(value) && selectedOption.size() == 3) {
                    variant_name = selectedOption.get(0).getValue() + "/" + selectedOption.get(1).getValue() + "/" + value;

                    setDataOnView(product, k);
                }


            }
        }
    }

    private void setDataOnView(Storefront.Product product, int pos) {
        String compare_price = String.valueOf(product.getVariants().getEdges().get(pos).getNode().getCompareAtPrice());
        price = product.getVariants().getEdges().get(pos).getNode().getPrice() + "";
        title = product.getTitle();
        product_variant_id = String.valueOf(product.getVariants().getEdges().get(pos).getNode().getId());
        // Boolean sale = product.getVariants().getEdges().get(pos).getNode().getAvailable();
        Boolean availableForSale = product.getVariants().getEdges().get(pos).getNode().getAvailableForSale();

        //  Log.d(Tag, "sale" + sale);
        Log.d(Tag, "availableForSale" + availableForSale);


        String weight = String.valueOf(product.getVariants().getEdges().get(pos).getNode().getWeight());
        String weightUnit = String.valueOf(product.getVariants().getEdges().get(pos).getNode().getWeightUnit());
        String sku = String.valueOf(product.getVariants().getEdges().get(pos).getNode().getSku());
        String product_type = product.getProductType();
        String vendor = product.getVendor();
        List<String> tags = product.getTags();
        // String handle = product.getHandle();
        String onlineStoreUrl = product.getOnlineStoreUrl();
        getActivity().setTitle(title != null ? title : "");


        // String id= String.valueOf(product.getVariants().getEdges().get(pos).getNode().getImage().getId());
        // String id= String.valueOf(product.getVariants().getEdges().get(pos).getNode().getImage().getId());
        String src = "";
        if (product.getVariants().getEdges().size() > 0) {
            if (product.getVariants().getEdges().get(pos).getNode().getImage() != null) {
                if (product.getVariants().getEdges().get(pos).getNode().getImage().getSrc() != null) {
                    src = String.valueOf(product.getVariants().getEdges().get(pos).getNode().getImage().getSrc());
                }

            }
        }
        if (product.getImages().getEdges().size() > 0) {
            imageUrl = product.getImages().getEdges().get(0).getNode().getSrc();
        }
        //  if (product.getVariants().getEdges().get(pos).getNode().getImage().getId())

        if (src != null && !src.isEmpty()) {
            if (product.getImages().getEdges().size() > 0) {
                // String image_id = String.valueOf(product.getImages().getEdges().get(pos).getNode().getId());


                for (int q = 0; q < product.getImages().getEdges().size(); q++) {

                    // String image_id = String.valueOf(product.getImages().getEdges().get(q).getNode().getSrc());
                    String image_id1 = String.valueOf(product.getImages().getEdges().get(q).getNode().getSrc());
                    if (image_id1 != null) {
                        if (src.equals(image_id1)) {
                            imageUrl = image_id1;
                            foldableListLayout.scrollToPosition(q);
                        }
                    }
                }


            }
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            layoutProductDetailBinding.descriptionTV.setText(product.getDescriptionHtml() != null ? Html.fromHtml(product.getDescriptionHtml(), Html.FROM_HTML_MODE_COMPACT) : "");
        } else {
            layoutProductDetailBinding.descriptionTV.setText(product.getDescriptionHtml() != null ? Html.fromHtml(product.getDescriptionHtml()) : "");
        }

        if (title != null && !title.isEmpty()) {

            layoutProductDetailBinding.title.setText(title);
        }

        if (availableForSale) {
            layoutProductDetailBinding.shopNowBT.setText(availableForSale ? getResources().getString(R.string.shop_now) : getResources().getString(R.string.sold));
        } else {
            layoutProductDetailBinding.shopNowBT.setText(availableForSale ? getResources().getString(R.string.shop_now) : getResources().getString(R.string.sold));
            layoutProductDetailBinding.shopNowBT.setTextAppearance(getActivity(), R.style.buttonStyleDisable);
        }

        if (product_type != null && !product_type.isEmpty()) {

            layoutProductDetailBinding.productType.setText(product_type);
        }

        if (vendor != null && !vendor.isEmpty()) {

            layoutProductDetailBinding.vendor.setText(vendor);
        }

        if (onlineStoreUrl != null && !onlineStoreUrl.isEmpty()) {

            layoutProductDetailBinding.onlineUrlLL.setVisibility(View.VISIBLE);
            layoutProductDetailBinding.fbShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
// intent.putExtra(Intent.EXTRA_SUBJECT, "Foo bar"); // NB: has no effect!
                    intent.putExtra(Intent.EXTRA_TEXT, onlineStoreUrl);

// See if official Facebook app is found
                    boolean facebookAppFound = false;
                    List<ResolveInfo> matches = getActivity().getPackageManager().queryIntentActivities(intent, 0);
                    for (ResolveInfo info : matches) {
                        if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook.katana")) {
                            intent.setPackage(info.activityInfo.packageName);
                            facebookAppFound = true;
                            break;
                        }
                    }

// As fallback, launch sharer.php in a browser
                    if (!facebookAppFound) {
                        String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + onlineStoreUrl;
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
                    }

                    startActivity(intent);

                }
            });

            layoutProductDetailBinding.twitterShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
// intent.putExtra(Intent.EXTRA_SUBJECT, "Foo bar"); // NB: has no effect!
                    intent.putExtra(Intent.EXTRA_TEXT, onlineStoreUrl);

// See if official Facebook app is found
                    boolean facebookAppFound = false;
                    List<ResolveInfo> matches = getActivity().getPackageManager().queryIntentActivities(intent, 0);
                    for (ResolveInfo info : matches) {
                        if (info.activityInfo.packageName.toLowerCase().startsWith("com.twitter.android")) {
                            intent.setPackage(info.activityInfo.packageName);
                            facebookAppFound = true;
                            break;
                        }
                    }

// As fallback, launch sharer.php in a browser
                    if (!facebookAppFound) {
                        String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + onlineStoreUrl;
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
                    }

                    startActivity(intent);

                }
            });
        }

        layoutProductDetailBinding.pinterestShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "https://www.pinterest.com/pin/create/button/?url=%s" + onlineStoreUrl;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                filterByPackageName(getActivity(), intent, "com.pinterest");
                startActivity(intent);
            }
        });


        if (tags.size() > 0) {

            Log.d("tag1", "" + tags.size());
            String tagss = "";
            for (int i = 0; i < tags.size(); i++) {
                Log.d("tag2", "" + tags.get(i));

                tagss = tagss + tags.get(i);
                Log.d("tag3", "" + tagss);

                if (i < tags.size() - 1) {
                    tagss = tagss + ",";
                    Log.d("tag4", "" + tagss);

                }

            }

            layoutProductDetailBinding.tags.setText(tagss);
        }

        if (compare_price != null && !compare_price.equals("null") && !compare_price.isEmpty()) {

            layoutProductDetailBinding.comparePriceTV.setVisibility(View.VISIBLE);
            layoutProductDetailBinding.comparePriceTV.setPaintFlags(layoutProductDetailBinding.comparePriceTV.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            layoutProductDetailBinding.comparePriceTV.setText(getCurrency() + compare_price);
        }
        if (price != null && !price.isEmpty()) {

            layoutProductDetailBinding.price.setText(getCurrency() + price);
        }
        if (sku != null && !sku.isEmpty() && !sku.equals("null")) {
            layoutProductDetailBinding.sku.setVisibility(View.VISIBLE);

            layoutProductDetailBinding.sku.setText("SKU: " + sku);
        }

        if (weight != null && weightUnit != null && !weight.equals("0.0") && !weight.equals("0.00")) {
            layoutProductDetailBinding.weight.setVisibility(View.VISIBLE);


            layoutProductDetailBinding.weight.setText("Weight: " + weight + " " + weightUnit);

        }


    }

    public void filterByPackageName(Context context, Intent intent, String prefix) {
        List<ResolveInfo> matches = context.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo info : matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith(prefix)) {
                intent.setPackage(info.activityInfo.packageName);
                return;
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.search);
        MenuItem item1 = menu.findItem(R.id.sort);
        MenuItem item12 = menu.findItem(R.id.bag);
        MenuItem logout = menu.findItem(R.id.logout);
        MenuItem mode = menu.findItem(R.id.mode);
        mode.setVisible(false);
        item12.setVisible(false);
        item.setVisible(false);
        item1.setVisible(false);
        logout.setVisible(false);
    }

    private void init(View view) {
        foldableListLayout = view.findViewById(R.id.foldable_list);


        layoutProductDetailBinding.emptyView.setVisibility(productList.get(0).getImages().getEdges().size() > 0 ? View.GONE : View.VISIBLE);


        foldableListLayout.setAdapter(new ProductDetailFlipViewAdapter(productList, getActivity(), foldableListLayout));

        layoutProductDetailBinding.shopNowBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (layoutProductDetailBinding.shopNowBT.getText().equals(getResources().getString(R.string.shop_now))) {


                    fetChandStoreDataInLocalPersistent(product_variant_id);


                } else {
                    Toast.makeText(getActivity(), "Sorry! This prododuct is currently not available", Toast.LENGTH_SHORT).show();

                }
            }

        });


    }

    public void fetChandStoreDataInLocalPersistent(String product_variant_id) {

        //Object object = LocalPersistence.readObjectFromFile(ShopifyApplication.getAppContext(), "cartFile");
        ArrayList<CartBean> existingList = MySharedPreferences.getInstance().getSerializableListData(getActivity());
        if (existingList != null && existingList.size() > 0) {
            boolean helper = true;

            for (int i = 0; i < existingList.size(); i++) {
                CartBean cartBean = existingList.get(i);
                CartBean.CartDetail cartDetail = existingList.get(i).getCartDetail();

                if (cartBean.getVariantId().equals(product_variant_id)) {
                    helper = false;
                    cartDetail.setQuantity(cartDetail.getQuantity() + 1);


                    cartBean.setCartDetail(cartDetail);

                    existingList.set(i, cartBean);


                    break;
                }


            }
            if (helper) {

                existingList.add(addNewData());
                Toast.makeText(getActivity(), "Product added to cart", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(getActivity(), "Product quantity Updated", Toast.LENGTH_SHORT).show();

            }

            //     LocalPersistence.witeObjectToFile(ShopifyApplication.getAppContext(), cartBeans, "cartFile");
            MySharedPreferences.getInstance().storeSerializableListData(getActivity(), existingList);


        } else {
            cartBeans.add(addNewData());
            // LocalPersistence.witeObjectToFile(ShopifyApplication.getAppContext(), cartBeans, "cartFile");
            MySharedPreferences.getInstance().storeSerializableListData(getActivity(), cartBeans);

            Toast.makeText(getActivity(), "Product added to cart", Toast.LENGTH_SHORT).show();

        }


    }

    private CartBean addNewData() {
        CartBean cartBean = new CartBean();
        cartBean.setVariantId(product_variant_id);
        CartBean.CartDetail cartDetail = new CartBean.CartDetail();
        cartDetail.setTitle(title);
        cartDetail.setPrice(price);
        cartDetail.setQuantity(1);
        cartDetail.setImageUrl(imageUrl);
        cartDetail.setVariantName(variant_name);
        Gson gson = new Gson();
        String json = gson.toJson(product);
        cartDetail.setProduct(json);

        cartBean.setCartDetail(cartDetail);


        return cartBean;

    }

    public void setData(Storefront.Product collection) {
        productList = new ArrayList<>();
        productList.add(collection);
        //       layoutProductDetailBinding.title.setText(collection.getVariants().getEdges().get(0).getNode());

    }


}

