package com.brst.shopifyapplication.Fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Adaptors.ProductsAdpater;
import com.brst.shopifyapplication.Adaptors.ProductsListAdpater;
import com.brst.shopifyapplication.Adaptors.ProductsStaggeredAdpater;
import com.brst.shopifyapplication.ApiImplementation.ApiClass;
import com.brst.shopifyapplication.HelperClasses.EndlessRecyclerViewScrollListener;
import com.brst.shopifyapplication.Interfaces.ProductsInterface;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.databinding.ProductFragmentBinding;
import com.shopify.buy3.Storefront;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class ProductStaggeredFromTagsFragment extends Fragment implements ProductsInterface {

    static ProductStaggeredFromTagsFragment productFragment;
    int position;
    String collection_id = "", cursor = "";
    View view;
    ProductsAdpater mAdapter;
    ProductsStaggeredAdpater mStaggeredAdapter;
    ProductsListAdpater mListAdpater;
    ProductFragmentBinding productFragmentBinding;
    List<Storefront.Product> data;
    ApiClass apiClass;
    ProductsInterface productsInterface;
    boolean isLoading = false;
    boolean hasMorePages = false;
    private String tag = "";

    /*public void setData(Storefront.Collection collection) {
        data = new ArrayList<>();
        data.add(collection);

    }
*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // if (view == null) {
        productFragmentBinding = DataBindingUtil.inflate(
                inflater, R.layout.product_fragment, container, false);
        view = productFragmentBinding.getRoot();
        data = new ArrayList<>();
        setInstanceToHomePage();

        initView();
        ((HomeActivity) getActivity()).setExpansion(true, false, 0);

        //getDataFromBundle();
        //   }
        getDataFromBundle();
        return view;
    }

    public static ProductStaggeredFromTagsFragment getInstance() {
        if (productFragment == null) {
            return productFragment = new ProductStaggeredFromTagsFragment();
        }

        return productFragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    public void setInstanceToHomePage() {
        productsInterface = this;
        HomeActivity homeActivity = new HomeActivity();
        homeActivity.getInstanceFromProductFragment(productsInterface, productFragmentBinding.swipeContainer);

    }



/*
    public void setupShopifyClient(String shopname) {
        try {
            JsonDirectoryCredentialsStore store = new JsonDirectoryCredentialsStore(getFilesDir());
            Credential creds = store.loadCredential(shopname);
            processor = new ProductsCallerTask(creds);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
*/

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.search);
        MenuItem item1 = menu.findItem(R.id.sort);
        MenuItem item12 = menu.findItem(R.id.bag);
        MenuItem log = menu.findItem(R.id.logout);
        item.setVisible(true);
        item1.setVisible(true);
        item12.setVisible(true);
        log.setVisible(false);
    }


    private void getDataFromBundle() {
        Bundle bundle = getArguments();
        apiClass = new ApiClass();

        if (bundle != null) {

            tag = bundle.getString("tag");

            tag = "tags:" + tag;
            productFragmentBinding.swipeContainer.setRefreshing(true);

            apiClass.fetchCategoriesWithPaginationOnSearch(getActivity(), getActivity(), "", "", false, productsInterface, 0, tag, productFragmentBinding.swipeContainer);



/*
            Storefront.Collection collection = (Storefront.Collection) bundle.getSerializable("product_list");
            collection.getProducts().getEdges().size();
            List<Storefront.Collection> collections = new ArrayList<>();
            collections.add(collection);
            mAdapter.addList(collections);*/
        }


        productFragmentBinding.swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
               /* isLoading = false;
                data.clear();
                mStaggeredAdapter.notifyDataSetChanged();
                apiClass.fetchCategoriesWithPagination(getActivity(), collection_id, "", false, productsInterface, position);
*/

            }
        });


    }

    private void initView() {
        setStaggeredAdpater();

    }


    private void setListAdapter() {
        ((HomeActivity) getActivity()).mode = 0;
        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(getActivity());
        productFragmentBinding.productRV.setHasFixedSize(true);
        productFragmentBinding.productRV.setLayoutManager(gridLayoutManager);
        //  productFragmentBinding.productRV.setEmptyView(productFragmentBinding.emptyView);

        mListAdpater = new ProductsListAdpater(getActivity(), ProductStaggeredFromTagsFragment.this, productFragmentBinding.productRV, data);
        productFragmentBinding.productRV.setAdapter(mListAdpater);
        productFragmentBinding.productRV.addOnScrollListener(new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                if (!isLoading) {
                    if (hasMorePages) {
                        isLoading = true;
                        productFragmentBinding.swipeContainer.setRefreshing(true);
                        apiClass.fetchCategoriesWithPagination(getActivity(), collection_id, cursor, true, productsInterface, position);
                    }

                }


            }
        });
    }

    private void setStaggeredAdpater() {
        ((HomeActivity) getActivity()).mode = 1;
        StaggeredGridLayoutManager gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        productFragmentBinding.productRV.setHasFixedSize(true);
        productFragmentBinding.productRV.setLayoutManager(gaggeredGridLayoutManager);
        //  productFragmentBinding.productRV.setEmptyView(productFragmentBinding.emptyView);

        mStaggeredAdapter = new ProductsStaggeredAdpater(getActivity(), ProductStaggeredFromTagsFragment.this, productFragmentBinding.productRV, data);
        productFragmentBinding.productRV.setAdapter(mStaggeredAdapter);
        productFragmentBinding.productRV.addOnScrollListener(new EndlessRecyclerViewScrollListener(gaggeredGridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                if (!isLoading) {
                    if (hasMorePages) {
                        isLoading = true;
                        productFragmentBinding.swipeContainer.setRefreshing(true);
                        apiClass.fetchCategoriesWithPagination(getActivity(), collection_id, cursor, true, productsInterface, position);
                    }

                }


            }
        });
    }

    @Override
    public void onSuccess(List<Storefront.Product> collections, boolean morePages, boolean pagination, String cursor,String serached_string) {

        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    productFragmentBinding.swipeContainer.setRefreshing(false);
                }
            });

            if (pagination) {
                data.addAll(collections);
                refreshDataInAdapter();
            } else {

                data = collections;
                refreshDataInAdapter();
            }
            hasMorePages = morePages;
            this.cursor = cursor;
            isLoading = false;
        }
    }

    @Override
    public void onModeChange(int mode) {
        if (mode == 0) {
            setStaggeredAdpater();
        } else {
            setListAdapter();
        }


    }

    @Override
    public void onSort(String s) {

        if (s.equals("low")) {
            sortProduct();
        } else if (s.equals("high")) {
            sortProductHighToLow();
        }

    }

    private void sortProduct() {
        Collections.sort(data, new Comparator<Storefront.Product>() {
            @Override
            public int compare(Storefront.Product c1, Storefront.Product c2) {
                Double one = Double.valueOf(String.valueOf(c1.getVariants().getEdges().get(0).getNode().getPrice()));
                Double two = Double.valueOf(String.valueOf(c2.getVariants().getEdges().get(0).getNode().getPrice()));
                return Double.compare(one, two);
            }
        });
        refreshDataInAdapter();

    }

    private void sortProductHighToLow() {
        Collections.sort(data, new Comparator<Storefront.Product>() {
            @Override
            public int compare(Storefront.Product c1, Storefront.Product c2) {
                Double one = Double.valueOf(String.valueOf(c1.getVariants().getEdges().get(0).getNode().getPrice()));
                Double two = Double.valueOf(String.valueOf(c2.getVariants().getEdges().get(0).getNode().getPrice()));
                return Double.compare(two, one);
            }
        });
        refreshDataInAdapter();
    }

    private void refreshDataInAdapter() {
        if (((HomeActivity) getActivity()).mode == 0) {

            mListAdpater.addList(data);

        } else {
            mStaggeredAdapter.addList(data);

        }


    }

    @Override
    public void onFailure(String s) {

    }

    @Override
    public void onError(String error) {

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Toast.makeText(getActivity(), "No Match Found", Toast.LENGTH_SHORT).show();


            }
        });
    }
}

