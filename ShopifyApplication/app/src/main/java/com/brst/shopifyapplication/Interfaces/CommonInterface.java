package com.brst.shopifyapplication.Interfaces;

import java.util.List;

/**
 * Created by brst-pc20 on 1/9/18.
 */

public interface CommonInterface<T> {


    public void onSuccess(List<T>  list);

    public void onFailure(String message);
    public void onError(String message);


}
