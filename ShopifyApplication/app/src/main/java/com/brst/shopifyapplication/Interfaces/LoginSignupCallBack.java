package com.brst.shopifyapplication.Interfaces;

/**
 * Created by brst-pc20 on 12/13/17.
 */

public interface LoginSignupCallBack {

    public void onSuccess(String accessToken);

    public void onFailure();

    public void onError(String errorr);


}
