package com.brst.shopifyapplication.Adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.brst.shopifyapplication.R;

import java.util.List;

/**
 * Created by brst-pc89 on 12/30/17.
 */

public class CollectionAdaptor extends RecyclerView.Adapter<CollectionAdaptor.ViewHolder> {

    Context context;
    List<String> collectionList;

    public CollectionAdaptor(Context context, List<String> colllectionList) {

        this.context = context;
        this.collectionList = colllectionList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // return null;
        View view = LayoutInflater.from(context).inflate(R.layout.custom_collectionview, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.textViewDiscount.setText(collectionList.get(position));
    }

    @Override
    public int getItemCount() {
        return collectionList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewDiscount;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewDiscount = itemView.findViewById(R.id.textViewDiscount);
        }
    }
}
