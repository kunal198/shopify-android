package com.brst.shopifyapplication.Activities.LoginPackage;

public class LogInPresenter implements LogInInterfaces.LoginModelInterface.Listeners {
    LogInInterfaces.LoginViewInterface loginViewInterface;
    LogInInterfaces.LoginModelInterface loginModelInterface;


    public LogInPresenter(LogInInterfaces.LoginViewInterface loginViewInterface) {
        this.loginViewInterface = loginViewInterface;
        loginModelInterface = new LogInModel();

    }

    public void validateCredentials(String email, String password) {
        loginModelInterface.validateCredentials(email, password, this);
    }

    @Override
    public void onEmailEmptyError() {
        loginViewInterface.setEmailEmptyError();
    }

    @Override
    public void onEmailValidError() {
        loginViewInterface.setEmailValidError();
    }

    @Override
    public void onPasswordEmptyError() {
        loginViewInterface.setPasswordEmptyError();
    }

    @Override
    public void onPasswordLimitError() {
        loginViewInterface.setPasswordLimitError();
    }

    @Override
    public void onSuccess() {
        loginViewInterface.navigateToHome();
    }
}
