package com.brst.shopifyapplication.Adaptors;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alexvasilkov.foldablelayout.FoldableListLayout;
import com.brst.shopifyapplication.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.shopify.buy3.Storefront;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PSD on 13-04-17.
 */

public class ProductDetailFlipViewAdapter extends BaseAdapter {
    Context context;
    FoldableListLayout foldableListLayout;
    private List<Storefront.Product> mOptions = new ArrayList<>();
    private LayoutInflater mLayoutInflater = null;
    private String Tag = "ProductDetailFlipViewAdapter";

    public ProductDetailFlipViewAdapter(List<Storefront.Product> options, Context context, FoldableListLayout foldableListLayout) {
        mOptions = options;
        this.mLayoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.foldableListLayout = foldableListLayout;

    }

    @Override
    public int getCount() {
        return mOptions.get(0).getImages().getEdges().size();
    }

    @Override
    public Object getItem(int position) {
        return mOptions.get(position).getImages().getEdges().size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Storefront.Product option = mOptions.get(0);


        ViewHolder vh;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.flip_view_row,
                    parent, false);
            vh = new ViewHolder();

            vh.productIV = convertView
                    .findViewById(R.id.productIV);


            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        //vh.categoryNameTV.setText(option);
        if (option.getImages().getEdges().size() > 0) {

            Glide.with(context).applyDefaultRequestOptions(new RequestOptions().placeholder(R.mipmap.placeholder_image)).load(option.getImages().getEdges().get(position).getNode().getSrc()).into(vh.productIV);
        } else {
            Glide.with(context).load(R.mipmap.placeholder_image).into(vh.productIV);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(Tag, "clicked");
                Log.d(Tag, "clicked" + foldableListLayout.getPosition());

                showImage(option.getImages().getEdges().get(position).getNode().getSrc(), option.getTitle());
            }
        });
        return convertView;
    }

    public void showImage(String image, String title) {


        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_image);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

        dialog.show();
        final ImageView cou_bck_iv = dialog.findViewById(R.id.cou_bck_iv);
        final ImageView imageview = dialog.findViewById(R.id.imageview);
        TextView name = dialog.findViewById(R.id.name);

        final ProgressBar progress = dialog.findViewById(R.id.progress);
        name.setText(title != null ? title : "");





      /*  Glide.with(context).load(image).apply(RequestOptions.placeholderOf(R.mipmap.picture)).into(chat_iv);*/
        Glide.with(context).load(image).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                progress.setVisibility(View.GONE);

                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                progress.setVisibility(View.GONE);
                return false;
            }
        }).into(imageview);

        cou_bck_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });


        dialog.setCanceledOnTouchOutside(true);
    }

    static class ViewHolder {
        ImageView productIV;

    }

}
