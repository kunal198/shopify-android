package com.brst.shopifyapplication.Interfaces;

import com.shopify.buy3.Storefront;

/**
 * Created by brst-pc20 on 12/14/17.
 */

public interface PaymentSettingInterface {


    public void onPayemntSettings(Storefront.PaymentSettings paymentSettings);

    public void onFailure(String error);

    public void onError(String error);


}
