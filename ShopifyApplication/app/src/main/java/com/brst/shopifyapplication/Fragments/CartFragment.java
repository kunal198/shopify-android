package com.brst.shopifyapplication.Fragments;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Activities.LoginPackage.LoginActivity;
import com.brst.shopifyapplication.Adaptors.CartAdapter;
import com.brst.shopifyapplication.ApiImplementation.ApiClass;
import com.brst.shopifyapplication.Beans.CartBean;
import com.brst.shopifyapplication.HelperClasses.Constants;
import com.brst.shopifyapplication.HelperClasses.MySharedPreferences;
import com.brst.shopifyapplication.Interfaces.CartInterface;
import com.brst.shopifyapplication.Interfaces.CheckoutInterface;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.ShopifyApplication;
import com.brst.shopifyapplication.databinding.EmptyCartBinding;
import com.brst.shopifyapplication.databinding.FragmentCartBinding;
import com.shopify.buy3.Storefront;
import com.shopify.graphql.support.ID;
import com.shopify.graphql.support.Input;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;


public class CartFragment extends BaseFragment implements CartInterface {

    static CartFragment productFragment;
    public boolean helper = true;
    View view;
    FragmentCartBinding fragmentCartBinding;
    EmptyCartBinding emptyCartBinding;
    CartAdapter cartAdapter;
    ArrayList<CartBean> cartBeans;
    TextView subtotalTV;
    CartInterface cartInterface;
    String token = null;

    public static CartFragment getInstance() {
        if (productFragment == null) {
            return productFragment = new CartFragment();
        }
        return productFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        //    if (view == null) {
        cartBeans = MySharedPreferences.getInstance().getSerializableListData(getActivity());


        fragmentCartBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_cart, container, false);
        view = fragmentCartBinding.getRoot();

        initView(view);


        fragmentCartBinding.rootView.setVisibility(cartBeans == null || cartBeans.size() < 1 ? View.GONE : View.VISIBLE);
        fragmentCartBinding.emptyView.setVisibility(cartBeans == null || cartBeans.size() < 1 ? View.VISIBLE : View.GONE);


        ((HomeActivity) getActivity()).setExpansion(true, false, 0);
        getActivity().setTitle("Basket");
        ((HomeActivity)getActivity()).disableCoordinateLayout();
        //    }
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.search);
        MenuItem item1 = menu.findItem(R.id.sort);
        MenuItem item12 = menu.findItem(R.id.bag);
        MenuItem logout = menu.findItem(R.id.logout);
        MenuItem mode = menu.findItem(R.id.mode);
        mode.setVisible(false);
        item12.setVisible(false);
        item.setVisible(false);
        item1.setVisible(false);
        logout.setVisible(false);
    }

    @Subscribe
    public void onMessageEvent(String a) {

     //   placeOrder();

        /* Do something */
    }


    @Override
    public void onStart() {
        super.onStart();
        ShopifyApplication.eventBus.register(this);
    }

    @Override
    public void onStop() {
        ShopifyApplication.eventBus.unregister(this);
        super.onStop();

    }

    private void placeOrder() {


        token = MySharedPreferences.getInstance().getData(ShopifyApplication.getAppContext(), Constants.ACCESS_TOKEN);
        if (token != null) {

            if (cartBeans.size() > 0) {

                initProgressDialog(getActivity());

                ArrayList<Storefront.CheckoutLineItemInput> quantityAndIDs = new ArrayList<>();

                for (int i = 0; i < cartBeans.size(); i++) {

                    CartBean.CartDetail cartDetail = cartBeans.get(i).getCartDetail();
                    CartBean cartBean = cartBeans.get(i);
                    quantityAndIDs.add(new Storefront.CheckoutLineItemInput(cartDetail.getQuantity(), new ID(cartBean.getVariantId())));


                }


                Storefront.CheckoutCreateInput input = new Storefront.CheckoutCreateInput()
                        .setLineItemsInput(Input.value(quantityAndIDs));


                new ApiClass().purchaseItem(getContext(), input, new CheckoutInterface() {
                    @Override
                    public void onSucessfullCheckout(Storefront.Checkout checkout) {
                        String checkoutWebUrl = checkout.getWebUrl();
                        ID checkoutId = checkout.getId();
                        //String checkoutId = checkout.getId().toString();
                        if (checkoutWebUrl != null && !checkoutWebUrl.isEmpty() && checkoutId != null) {

                            if (token != null) {
                                new ApiClass().updatingToken(getContext(), checkoutId, token, new CheckoutInterface() {
                                    @Override
                                    public void onSucessfullCheckout(Storefront.Checkout checkout) {

                                        dismissProgressDialog(getActivity());
                                        String checkoutWebUrl = checkout.getWebUrl();
                                        ID checkoutId = checkout.getId();
                                        Bundle bundle = new Bundle();
                                        bundle.putSerializable("checkout_id", checkoutId);
                                        bundle.putString("checkout_url", checkoutWebUrl);
                                        fragmentTransaction(ShippingAddressFragment.getInstance(), R.id.container, bundle);

                                    }

                                    @Override
                                    public void onFailure() {
                                        dismissProgressDialog(getActivity());

                                    }

                                    @Override
                                    public void onError(String error) {
                                        dismissProgressDialog(getActivity());

                                    }
                                });
                            } else {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getActivity(), "Please Login First", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }


                            //openWebBrowser(checkoutWebUrl);
                        } else {
                            dismissProgressDialog(getActivity());

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getActivity(), "Error Occured", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }

                    @Override
                    public void onFailure() {
                        dismissProgressDialog(getActivity());

                    }

                    @Override
                    public void onError(String error) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();

                            }
                        });

                        dismissProgressDialog(getActivity());

                    }
                });
            } else {
                Toast.makeText(getActivity(), "Please select the product variant", Toast.LENGTH_SHORT).show();
            }
        } else {

            Intent mIntent = new Intent(getActivity(), LoginActivity.class); //'this' is Activity B
            mIntent.putExtra("FROM_ACTIVITY", "Login");
            startActivity(mIntent);

            // setOnActivityTransfer(LoginActivit);
        }
    }


    private void initView(View view) {

/*
        subtotalTV = view.findViewById(R.id.subtotalTV);
*/

        if (cartBeans != null) {
            cartInterface = this;
            cartInterface = this;
            fragmentCartBinding.cartRV.setHasFixedSize(true);

            cartAdapter = new CartAdapter(getActivity(), CartFragment.this, fragmentCartBinding.cartRV, cartBeans, cartInterface);
            fragmentCartBinding.cartRV.setAdapter(cartAdapter);
            getTotalAmount(cartBeans);
        }


        fragmentCartBinding.placeOrderBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                placeOrder();
            }
        });
    }

    private void getTotalAmount(ArrayList<CartBean> cartBeans) {

        double total_price = 0;
        for (int i = 0; i < cartBeans.size(); i++) {
            double price = Double.parseDouble(cartBeans.get(i).getCartDetail().getPrice());
            int qty = cartBeans.get(i).getCartDetail().getQuantity();
            total_price += price * qty;
        }
        String cost = String.format("%.02f", total_price);
        fragmentCartBinding.totalTV.setText(getCurrency() + cost);
        //  subtotalTV.setText(getCurrency() + String.valueOf(total_price));

    }

    @Override
    public void addClicked(int adapter_pos, String pos) {
        CartBean cartBean = cartBeans.get(adapter_pos);
        CartBean.CartDetail cartDetail = cartBeans.get(adapter_pos).getCartDetail();

        cartDetail.setQuantity(Integer.parseInt(pos));
        cartBean.setCartDetail(cartDetail);
        cartBeans.set(adapter_pos, cartBean);
        cartAdapter.notifyItemChanged(adapter_pos);
        getTotalAmount(cartBeans);
        MySharedPreferences.getInstance().storeSerializableListData(getActivity(), cartBeans);
        // LocalPersistence.witeObjectToFile(ShopifyApplication.getAppContext(), cartBeans, "cartFile");

    }

    @Override
    public void subClicked(int pos) {
        CartBean cartBean = cartBeans.get(pos);
        CartBean.CartDetail cartDetail = cartBeans.get(pos).getCartDetail();

        if (cartDetail.getQuantity() > 1) {
            cartDetail.setQuantity(cartDetail.getQuantity() - 1);
            cartBean.setCartDetail(cartDetail);
            cartBeans.set(pos, cartBean);
            cartAdapter.notifyItemChanged(pos);
            getTotalAmount(cartBeans);
            //  LocalPersistence.witeObjectToFile(ShopifyApplication.getAppContext(), cartBeans, "cartFile");
            MySharedPreferences.getInstance().storeSerializableListData(getActivity(), cartBeans);

        }
    }

    @Override
    public void deleteItem(int pos) {
        cartBeans.remove(pos);
        cartAdapter.notifyItemRemoved(pos);
        getTotalAmount(cartBeans);
        if (cartBeans.size() < 1) {
            fragmentCartBinding.rootView.setVisibility(View.GONE);
            fragmentCartBinding.emptyView.setVisibility(View.VISIBLE);

        } else {
            fragmentCartBinding.rootView.setVisibility(View.VISIBLE);
            fragmentCartBinding.emptyView.setVisibility(View.GONE);
        }
        Toast.makeText(getActivity(), "Item Deleted.", Toast.LENGTH_SHORT).show();
        ((HomeActivity) getActivity()).setBadge(cartBeans.size());
        MySharedPreferences.getInstance().storeSerializableListData(getActivity(), cartBeans);
    }
}

