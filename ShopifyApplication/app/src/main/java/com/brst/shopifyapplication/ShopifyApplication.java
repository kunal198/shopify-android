package com.brst.shopifyapplication;

import android.app.Application;
import android.content.Context;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by brst-pc20 on 12/6/17.
 */

public class ShopifyApplication extends Application {

    public static Context context;
    public static EventBus eventBus;

    public static Context getAppContext() {
        return context;

    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        eventBus = EventBus.getDefault();
    }
}
