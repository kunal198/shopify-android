package com.brst.shopifyapplication.Interfaces;

import com.shopify.buy3.Storefront;

/**
 * Created by brst-pc20 on 1/8/18.
 */

public interface ShopPolicyInterface {

    public void onResponse(Storefront.Shop shop);

    public void onError(String er);

    public void onFail(String fail);




}
