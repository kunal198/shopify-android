package com.brst.shopifyapplication.Interfaces;

import com.shopify.buy3.Storefront;

/**
 * Created by brst-pc20 on 12/14/17.
 */

public interface CheckoutInterface {


    public void onSucessfullCheckout(Storefront.Checkout checkout);

    public void onFailure();

    public void onError(String error);


}
