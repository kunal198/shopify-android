package com.brst.shopifyapplication.Activities.SignUpPackage;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.brst.shopifyapplication.Activities.LoginPackage.LoginActivity;
import com.brst.shopifyapplication.Activities.ParentActivity;
import com.brst.shopifyapplication.ApiImplementation.ApiClass;
import com.brst.shopifyapplication.HelperClasses.Validator;
import com.brst.shopifyapplication.Interfaces.LoginSignupCallBack;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.databinding.ActivitySignupBinding;

import org.greenrobot.eventbus.Subscribe;


public class SignUpActivity extends ParentActivity {

    ActivitySignupBinding activitySignupBinding;
    String previousActivity;

    @Subscribe
    public void onMessageEvent() {




        /* Do something */
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activitySignupBinding = DataBindingUtil.setContentView(this, R.layout.activity_signup);

        Intent mIntent = getIntent();
        previousActivity = mIntent.getStringExtra("FROM_ACTIVITY");
        activitySignupBinding.signInTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        activitySignupBinding.backIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        activitySignupBinding.signUpNowBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  setOnActivityTransfer(HomeActivity.class);
                finish();*/
                hideKeyboard(SignUpActivity.this);

                if (Validator.signUpValidation(SignUpActivity.this, activitySignupBinding.fullnameET.getText().toString(), activitySignupBinding.lastnameET.getText().toString(), activitySignupBinding.phoneET.getText().toString(), activitySignupBinding.emailET.getText().toString(), activitySignupBinding.passET.getText().toString(), activitySignupBinding.confirmPass.getText().toString())) {

                    clearFocus(activitySignupBinding.fullnameET);
                    clearFocus(activitySignupBinding.lastnameET);
                    clearFocus(activitySignupBinding.emailET);
                    clearFocus(activitySignupBinding.passET);
                    clearFocus(activitySignupBinding.confirmPass);
                    clearFocus(activitySignupBinding.phoneET);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            initProgressDialog(SignUpActivity.this);

                        }
                    });
                    String phone = "+91" + activitySignupBinding.phoneET.getText().toString();
                    new ApiClass().signupProcess(SignUpActivity.this, activitySignupBinding.fullnameET.getText().toString(), activitySignupBinding.lastnameET.getText().toString(), phone, activitySignupBinding.emailET.getText().toString(), activitySignupBinding.passET.getText().toString(), new LoginSignupCallBack() {
                        @Override
                        public void onSuccess(String accessToken) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    dismissProgressDialog(SignUpActivity.this);
                                    Toast.makeText(SignUpActivity.this, "Your Account has been created sucessfully", Toast.LENGTH_SHORT).show();

                                }
                            });
                            if (previousActivity != null && previousActivity.equals("Login")) {
                                
                                Intent mIntent = new Intent(SignUpActivity.this, LoginActivity.class); //'this' is Activity B
                                mIntent.putExtra("FROM_ACTIVITY", "Login");
                                startActivity(mIntent);
                                finish();
                            } else {
                                setOnActivityTransfer(LoginActivity.class);
                                finish();
                            }


                        }

                        @Override
                        public void onFailure() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    dismissProgressDialog(SignUpActivity.this);

                                }
                            });

                        }

                        @Override
                        public void onError(String errorr) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(SignUpActivity.this, errorr, Toast.LENGTH_SHORT).show();

                                    dismissProgressDialog(SignUpActivity.this);

                                }
                            });

                        }
                    });
                }
            }
        });
    }


}
