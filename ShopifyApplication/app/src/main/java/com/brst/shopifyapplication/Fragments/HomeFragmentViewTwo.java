package com.brst.shopifyapplication.Fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Adaptors.CategoryAdaptor;
import com.brst.shopifyapplication.ApiImplementation.ApiClass;
import com.brst.shopifyapplication.Interfaces.ProductListingInterface;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.databinding.FragmentHomeTwoBinding;
import com.shopify.buy3.Storefront;

import java.util.ArrayList;
import java.util.List;


public class HomeFragmentViewTwo extends BaseFragment {


    static HomeFragmentViewTwo homeFragmentViewTwo;
    TextView textViewCategory;
    View view;
    List<Storefront.Collection> categoryList;
    RecyclerView recyclerViewCategory;
    CategoryAdaptor categoryAdaptor;
    FragmentHomeTwoBinding fragmentHomeTwoBinding;

    public HomeFragmentViewTwo() {
        // Required empty public constructor
    }

    public static HomeFragmentViewTwo getInstance() {
        if (homeFragmentViewTwo == null) {
            return homeFragmentViewTwo = new HomeFragmentViewTwo();
        }
        return homeFragmentViewTwo;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //  if (view == null) {
        fragmentHomeTwoBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_home_two, container, false);
        view = fragmentHomeTwoBinding.getRoot();


        setUpList();
        setUpViews();
        ((HomeActivity)getActivity()).enableDrawer();
        setUpAdaptor();
        getActivity().setTitle("Shopify Application");
        //    }
        ((HomeActivity) getActivity()).setExpansion(true, false, 0);
        fetchCollection();

        fragmentHomeTwoBinding.shopNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentTransaction(HomeFragmentViewOne.getInstance(),R.id.container);
            }
        });

        return view;
    }

    private void setUpList() {
        categoryList = new ArrayList<>();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.search);
        MenuItem item1 = menu.findItem(R.id.sort);
        MenuItem item12 = menu.findItem(R.id.bag);
        MenuItem logout = menu.findItem(R.id.logout);
        MenuItem mode = menu.findItem(R.id.mode);
        mode.setVisible(false);
        item12.setVisible(false);
        item.setVisible(false);
        item1.setVisible(false);
        logout.setVisible(false);
    }

    private void setUpAdaptor() {

        categoryAdaptor = new CategoryAdaptor(getContext(), categoryList);
        RecyclerView.LayoutManager layoutManagerKids = new GridLayoutManager(getContext(), 2);
        recyclerViewCategory.setLayoutManager(layoutManagerKids);
        recyclerViewCategory.setAdapter(categoryAdaptor);

    }


    public void fetchCollection() {


        //shopAdaptor.addList(((HomeActivity)getActivity()).collectionList);

        initProgressDialog(getActivity());
        ApiClass apiClass = new ApiClass();
        apiClass.fetchCategories(getActivity(), new ProductListingInterface() {
            @Override
            public void onSuccess(List<Storefront.Collection> collections) {
                collections.remove(0);
                categoryList = collections;

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        categoryAdaptor.addList(categoryList);
                        dismissProgressDialog(getActivity());
                    }
                });


            }

            @Override
            public void onFailure(String error) {
                dismissProgressDialog(getActivity());

            }

            @Override
            public void onError() {
                dismissProgressDialog(getActivity());

            }
        });
    }


    private void setUpViews() {

        textViewCategory = view.findViewById(R.id.textViewCategory);
        recyclerViewCategory = view.findViewById(R.id.recyclerViewCategory);

        textViewCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

}
