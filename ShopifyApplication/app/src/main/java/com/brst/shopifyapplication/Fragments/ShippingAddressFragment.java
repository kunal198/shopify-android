package com.brst.shopifyapplication.Fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Adaptors.CustomerAdapter;
import com.brst.shopifyapplication.ApiImplementation.ApiClass;
import com.brst.shopifyapplication.Async_Thread.Super_AsyncTask;
import com.brst.shopifyapplication.Async_Thread.Super_AsyncTask_Interface;
import com.brst.shopifyapplication.Beans.Country;
import com.brst.shopifyapplication.HelperClasses.Constant;
import com.brst.shopifyapplication.HelperClasses.Constants;
import com.brst.shopifyapplication.HelperClasses.CountryGetter;
import com.brst.shopifyapplication.HelperClasses.MySharedPreferences;
import com.brst.shopifyapplication.HelperClasses.Validator;
import com.brst.shopifyapplication.Interfaces.CheckoutInterface;
import com.brst.shopifyapplication.Interfaces.CustomerDetailInterface;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.ShopifyApplication;
import com.brst.shopifyapplication.databinding.FragmentAddressBinding;
import com.shopify.buy3.Storefront;
import com.shopify.graphql.support.ID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class ShippingAddressFragment extends BaseFragment {

    static ShippingAddressFragment productFragment;
    String Tag = "ShippingAddressFragment";
    View view = null;
    FragmentAddressBinding fragmentAddressBinding;
    CustomerAdapter stateAdpater, cityAdpater;

    ID checkout_id;
    String checkout_url = "", country_code = "";

    ArrayList<Country> stateArrayList, cityArrayList;

    public static ShippingAddressFragment getInstance() {
        if (productFragment == null) {
            return productFragment = new ShippingAddressFragment();
        }
        return productFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //  if (view == null) {
        fragmentAddressBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_address, container, false);
        view = fragmentAddressBinding.getRoot();

        initView();
        getActivity().setTitle("Checkout Process");
        ((HomeActivity) getActivity()).setExpansion(true, false, 0);
        getDataFromBundle();
        String access_token = MySharedPreferences.getInstance().getData(ShopifyApplication.getAppContext(), Constants.ACCESS_TOKEN);
        initProgressDialog(getActivity());
        new ApiClass().getCustomerDetails(getActivity(), access_token, new CustomerDetailInterface() {
            @Override
            public void onSuccessFulDetail(Storefront.Customer customer) {
                if (customer != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dismissProgressDialog(getActivity());
                            fragmentAddressBinding.firstNameET.setText(customer.getFirstName() != null ? customer.getFirstName() : "");
                            fragmentAddressBinding.lastNameET.setText(customer.getLastName() != null ? customer.getLastName() : "");
                            fragmentAddressBinding.phoneET.setText(customer.getPhone() != null ? customer.getPhone() : "");
                        }
                    });
                }

            }

            @Override
            public void onFailure() {

            }

            @Override
            public void onError(String error) {

            }
        });

        ((HomeActivity) getActivity()).setExpansion(true, false, 0);

        setSppinerCountry();

        //   }

        return view;
    }

    private void setSppinerCountry() {
        ArrayList<Country> countryArrayList = new ArrayList<>();
        //  final TreeMap<String, String> map = new TreeMap<String, String>(countries);
        stateArrayList = new ArrayList<>();
        cityArrayList = new ArrayList<>();
        Country country = new Country();
        country.setCountryName("State");
        Country countryy = new Country();
        countryy.setCountryName("City");
        cityArrayList.add(countryy);
        stateArrayList.add(country);
        CountryGetter countryGetter = new CountryGetter(getActivity());

        countryArrayList = countryGetter.ReadFromfile();


        //SortSpinnerAdapter1 countryAdapter = new SortSpinnerAdapter1(getActivity(), R.layout.spinner_layout2, countryArrayList);

        // Drop down layout style - list view with radio button
        // countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        CustomerAdapter customerAdapter = new CustomerAdapter(getActivity(), R.layout.spinner_layout2, countryArrayList);

        fragmentAddressBinding.countryET.setThreshold(1);

        fragmentAddressBinding.countryET.setAdapter(customerAdapter);
        Log.d("countriesArrayList11", "--" + countryArrayList);


        final ArrayList<Country> finalCountryArrayList = countryArrayList;

        fragmentAddressBinding.countryET.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int i, long id) {
                String item = parent.getItemAtPosition(i).toString();

                String geoID = finalCountryArrayList.get(i).geoID();
                country_code = finalCountryArrayList.get(i).countryCode();

                fragmentAddressBinding.stateET.getText().clear();
                fragmentAddressBinding.cityET.getText().clear();
                Log.d("resulttt", "--" + item
                        + "---" + geoID);
                if (!item.equals("Select Country")) {
                    setSppinerState(geoID, stateArrayList, stateAdpater);
                }
            }
        });

        stateAdpater = new CustomerAdapter(getActivity(), R.layout.spinner_layout3, stateArrayList);

        // Drop down layout style - list view with radio button
        //  stateAdpater.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        fragmentAddressBinding.stateET.setThreshold(1);
        fragmentAddressBinding.stateET.setAdapter(stateAdpater);


        fragmentAddressBinding.stateET.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int i, long id) {

                if (i != 0) {
                    String item = parent.getItemAtPosition(i).toString();
                    Log.d("statearrayList", "---" + stateArrayList);
                    String geoID = stateArrayList.get(i).geoID();
                    /*cityArrayList.clear();
                    Country country1 = new Country();
                    country1.setCountryName("City");
                    cityArrayList.add(country1);*/
                    Log.d("itemmm", "--" +
                            "---" + geoID);
                    fragmentAddressBinding.cityET.getText().clear();
                    setSppinerState(geoID, cityArrayList, cityAdpater);
                }
                //   country_code = map.get(item);

            }


        });

        cityAdpater = new CustomerAdapter(getActivity(), R.layout.spinner_layout3, cityArrayList);

        // Drop down layout style - list view with radio button
        // cityAdpater.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        fragmentAddressBinding.cityET.setThreshold(1);
        fragmentAddressBinding.cityET.setAdapter(cityAdpater);


        fragmentAddressBinding.cityET.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int i, long id) {
                String item = parent.getItemAtPosition(i).toString();

                String geoID = cityArrayList.get(i).geoID();
                Log.d("itemmm", "--" + item
                        + "---" + geoID);
                //   country_code = map.get(item);

            }
        });
    }

    private void setSppinerState(String geoID, ArrayList<Country> arrayList, final CustomerAdapter sortSpinnerAdapter1) {

        Log.d("countriesArrayList11", "--" + stateArrayList);

        String url = Constant.GET_CITIES + geoID + Constant.GEONAME_USERNAME;
        Log.d("urllll", "--" + url);
        if (haveNetworkConnection()) {

            Constant.execute(new Super_AsyncTask(getActivity(), url, new Super_AsyncTask_Interface() {
                @Override
                public void onTaskCompleted(String result) {
                    Log.d("states_op", "" + result);
                    ArrayList<Country> arrayList1 = new ArrayList();
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        JSONArray jsonArray = jsonObject.optJSONArray("geonames");
                        if (jsonArray != null) {

                            for (int i = 0; i < jsonArray.length(); i++) {
                                Country country = new Country();
                                JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                                country.setCountryName(jsonObject1.optString("name"));
                                String geonameId = jsonObject1.optString("geonameId");
                                Log.d("geonameId>>>>", "--" + geonameId);
                                country.setGeoNameId(jsonObject1.optString("geonameId"));
                                arrayList1.add(country);
                            }

                            sortSpinnerAdapter1.addList(arrayList1);

                        }




                      /*  stateAdpater = new CustomerAdapter(getActivity(), R.layout.spinner_layout3, arrayList);

                        // Drop down layout style - list view with radio button
                        //  stateAdpater.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


                        stateSP.setThreshold(1);
                        stateSP.setAdapter(stateAdpater);*/
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, true, false));


        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.search);
        MenuItem item1 = menu.findItem(R.id.sort);
        MenuItem item12 = menu.findItem(R.id.bag);
        MenuItem logout = menu.findItem(R.id.logout);
        MenuItem mode = menu.findItem(R.id.mode);
        mode.setVisible(false);
        item12.setVisible(false);
        item.setVisible(false);
        item1.setVisible(false);
        logout.setVisible(false);
    }

    private void initView() {
        fragmentAddressBinding.saveBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = fragmentAddressBinding.firstNameET.getText().toString();
                String last_name = fragmentAddressBinding.lastNameET.getText().toString();
                String addressOne = fragmentAddressBinding.addressOneET.getText().toString();
                String addressTwo = fragmentAddressBinding.addresstwoET.getText().toString();
                String city = fragmentAddressBinding.cityET.getText().toString();
                String state = fragmentAddressBinding.stateET.getText().toString();
                String zip = fragmentAddressBinding.zipET.getText().toString();
                String country = fragmentAddressBinding.countryET.getText().toString();
                String phone = fragmentAddressBinding.phoneET.getText().toString();


                if (Validator.shippingValidation(getActivity(), name, last_name, addressOne, addressTwo, city, state, zip, country, phone)) {

                    initProgressDialog(getActivity());
                    Storefront.MailingAddressInput input = new Storefront.MailingAddressInput()
                            .setAddress1(addressOne)
                            .setAddress2(addressTwo)
                            .setCity(city)
                            .setCountry(country)
                            .setFirstName(name)
                            .setLastName(last_name)
                            .setPhone(phone)
                            .setProvince(state)
                            .setZip(zip);

                    new ApiClass().updatingAddress(getActivity(), checkout_id, input, new CheckoutInterface() {
                        @Override
                        public void onSucessfullCheckout(Storefront.Checkout checkout) {
                            dismissProgressDialog(getActivity());
                            String checkoutWebUrl = checkout.getWebUrl();
                            ID checkoutId = checkout.getId();
                            boolean getTaxIncluded = checkout.getTaxesIncluded();
                            boolean taxExempt = checkout.getTaxExempt();
                            String total_tax = String.valueOf(checkout.getTotalTax());
                            String sub_total = String.valueOf(checkout.getSubtotalPrice());

                            String totalPrice = String.valueOf(checkout.getTotalPrice());

                            Bundle bundle = new Bundle();
                            bundle.putSerializable("checkoutId", checkoutId);
                            bundle.putString("checkoutUrl", checkoutWebUrl);
                            bundle.putString("totalPrice", totalPrice);
                            bundle.putString("sub_total", sub_total);
                            if (!getTaxIncluded) {
                                bundle.putString("tax", total_tax);
                            }
                            bundle.putSerializable("input", input);

                            fragmentTransaction(ShippingMethodFragment.getInstance(), R.id.container, bundle);

                            /*  if (checkoutWebUrl != null && !checkoutWebUrl.isEmpty()) {
                                openWebBrowser(checkoutWebUrl);
                            }*/
                        }

                        @Override
                        public void onFailure() {
                            dismissProgressDialog(getActivity());

                        }

                        @Override
                        public void onError(String error) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
                                }
                            });
                            dismissProgressDialog(getActivity());

                        }
                    });


                }


            }
        });


    }


    private void getDataFromBundle() {
        Bundle bundle = getArguments();


        if (bundle != null) {

            checkout_id = (ID) bundle.getSerializable("checkout_id");
            checkout_url = bundle.getString("checkout_url");
        }


    }


}

