package com.brst.shopifyapplication.Activities.HomePackage;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.brst.shopifyapplication.Activities.LoginPackage.LoginActivity;
import com.brst.shopifyapplication.Activities.ParentActivity;
import com.brst.shopifyapplication.Adaptors.MenuDrawerAdpator;
import com.brst.shopifyapplication.ApiImplementation.ApiClass;
import com.brst.shopifyapplication.Fragments.CartFragment;
import com.brst.shopifyapplication.Fragments.HomeFragmentViewTwo;
import com.brst.shopifyapplication.Fragments.ProductStaggeredFragment;
import com.brst.shopifyapplication.HelperClasses.Constants;
import com.brst.shopifyapplication.HelperClasses.MySharedPreferences;
import com.brst.shopifyapplication.Interfaces.ProductListingInterface;
import com.brst.shopifyapplication.Interfaces.ProductsInterface;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.ShopifyApplication;
import com.brst.shopifyapplication.databinding.ActivityHomeBinding;
import com.nex3z.notificationbadge.NotificationBadge;
import com.shopify.buy3.Storefront;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import br.com.mauker.materialsearchview.MaterialSearchView;

public class HomeActivity extends ParentActivity /*implements NavigationView.OnNavigationItemSelectedListener*/ {
    public static int mode = 0;
    static ProductsInterface productsInterface;
    static SwipeRefreshLayout swipeRefreshLayout;
    static boolean visibilityOfSearch = false;
    public ActivityHomeBinding activityHomeBinding;
    Dialog dialog;
    static public List<Storefront.Collection> collectionList;
    ApiClass apiClass;
    private String Tag = "HomeActivity";
    public ActionBarDrawerToggle toggle;

    Menu menu = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityHomeBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);

        fragmentTransaction(HomeFragmentViewTwo.getInstance(), R.id.container);
        handleToolbar();
        apiClass = new ApiClass();
        // Handle menu actions


        // Handle drawer actions
        setUpDrawer();
        handleMenu();
        // Show main fragment in container


        // setTitle("Fashion Feed");
        setupTabIcons();
        setBottomNavigate();
        searchView();
    }



    public void disableCoordinateLayout() {
        activityHomeBinding.include.appBar.setActivated(false);
        //you will need to hide also all content inside CollapsingToolbarLayout
        //plus you will need to hide title of it


        AppBarLayout.LayoutParams p = (AppBarLayout.LayoutParams) activityHomeBinding.include.toolbar.getLayoutParams();
        p.setScrollFlags(0);
        activityHomeBinding.include.toolbar.setLayoutParams(p);
        activityHomeBinding.include.toolbar.setActivated(false);

        /*CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) activityHomeBinding.include.appBar.getLayoutParams();
        lp.height = getResources().getDimensionPixelSize(R.dimen.toolbar_height);
        activityHomeBinding.include.appBar.requestLayout();*/

        //you also have to setTitle for toolbar

    }

    public void enableCollapse() {
        activityHomeBinding.include.appBar.setActivated(true);
        activityHomeBinding.include.toolbar.setActivated(true);

        //you will need to show now all content inside CollapsingToolbarLayout
        //plus you will need to show title of it


        AppBarLayout.LayoutParams p = (AppBarLayout.LayoutParams) activityHomeBinding.include.toolbar.getLayoutParams();
        p.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);
        activityHomeBinding.include.toolbar.setLayoutParams(p);
    }

    private void handleMenu() {
        collectionList = new ArrayList<>();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(HomeActivity.this);


        activityHomeBinding.drawerMenuView.setLayoutManager(mLayoutManager);
        activityHomeBinding.drawerMenuView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(activityHomeBinding.drawerMenuView.getContext(), DividerItemDecoration.VERTICAL);
        activityHomeBinding.drawerMenuView.addItemDecoration(dividerItemDecoration);

        MenuDrawerAdpator menuDrawerAdpator = new MenuDrawerAdpator(HomeActivity.this, collectionList);
        activityHomeBinding.drawerMenuView.setAdapter(menuDrawerAdpator);
        ApiClass apiClass = new ApiClass();
        apiClass.fetchCategories(this, new ProductListingInterface() {
            @Override
            public void onSuccess(List<Storefront.Collection> collections) {
                collectionList = collections;

                menuDrawerAdpator.addList(collections);


            }

            @Override
            public void onFailure(String error) {

            }

            @Override
            public void onError() {

            }
        });
    }

    private void setUpDrawer() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, activityHomeBinding.include.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

   /*     NavigationView navigationView =  findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);*/
    }

    public void visibilityBottomNavigation(boolean navigation, int i) {
        activityHomeBinding.include.bottomNavigation.setVisibility(navigation ? View.VISIBLE : View.GONE);
        activityHomeBinding.include.bottomNavigationView.setVisibility(navigation ? View.VISIBLE : View.GONE);
        activityHomeBinding.include.checkout.setVisibility(i == 1 ? View.VISIBLE : View.GONE);
        activityHomeBinding.include.order.setVisibility(i == 2 ? View.VISIBLE : View.GONE);
    }

    public void setExpansion(boolean expansion, boolean navigation, int i) {
        activityHomeBinding.include.appBar.setExpanded(expansion, expansion);
        activityHomeBinding.include.bottomNavigation.setVisibility(navigation ? View.VISIBLE : View.GONE);
        activityHomeBinding.include.bottomNavigationView.setVisibility(navigation ? View.VISIBLE : View.GONE);
        activityHomeBinding.include.checkout.setVisibility(i == 1 ? View.VISIBLE : View.GONE);
        activityHomeBinding.include.order.setVisibility(i == 2 ? View.VISIBLE : View.GONE);

/*
        if (!expansion) {

            activityHomeBinding.include.bottomNavigation.animate()
                    .translationY(activityHomeBinding.include.bottomNavigation.getHeight());


        } else {
            activityHomeBinding.include.bottomNavigation.animate()
                    .translationY(activityHomeBinding.include.bottomNavigation.getHeight())
                    .alpha(1.0f);

        }
        activityHomeBinding.include.bottomNavigation.setVisibility(navigation ? View.VISIBLE : View.GONE);
*/

    }

    private void handleToolbar() {


        setSupportActionBar(activityHomeBinding.include.toolbar);

    }


    public void backButtonToolBar() {
        toggle.setDrawerIndicatorEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
      /*  final Drawable upArrow = getResources().getDrawable(R.drawable.ic_white_arrow);
        upArrow.setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);


        activityHomeBinding.include.toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_white_arrow));*/
        activityHomeBinding.include.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                Log.d("cek", "home selected");
            }
        });
    }

    public void enableDrawer() {
        setUpDrawer();
        toggle.setDrawerIndicatorEnabled(true);


    }



    /*public  void setBadgeCount(Context context, int count) {

        MenuItem itemCart = menu.findItem(R.id.bag);
        LayerDrawable icon = (LayerDrawable) itemCart.getIcon();
        BadgeDrawable badge;

        // Reuse drawable if possible\
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }

        badge.setCount(String.valueOf(count));
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);


    }*/

    public void setBadgeCount(Context context, int badgeCount) {

        View m = menu.findItem(R.id.bag).getActionView().findViewById(R.id.notificationBadge);
        View actionView = menu.findItem(R.id.bag).getActionView();
        ((NotificationBadge) m).setNumber(badgeCount);
        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentTransaction(CartFragment.getInstance(), R.id.container);
            }
        });
        // invalidateOptionsMenu();

    }


    public void setListnerOnMenuItem() {
        View m = menu.findItem(R.id.sort).getActionView();

        m.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initSortDialog();
            }
        });
        View m1 = menu.findItem(R.id.mode).getActionView();
        ImageView gridd = menu.findItem(R.id.mode).getActionView().findViewById(R.id.gridd);

        m1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // onSearchRequested();
                if (mode == 0) {
                    gridd.setImageResource(R.drawable.list);
                } else {
                    gridd.setImageResource(R.drawable.grid);
                }

                // invalidateOptionsMenu();
                productsInterface.onModeChange(mode);
            }
        });

        View m2 = menu.findItem(R.id.logout).getActionView();

        m2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        String currency = MySharedPreferences.getInstance().getData(ShopifyApplication.getAppContext(), Constants.SELECTED_CURRENCY);

        if (currency == null) {

            new ApiClass().fetchShopPolicy(ShopifyApplication.getAppContext(), null);
        }
    }

    public void setBadge(int number) {
        if (number > 0) {
            activityHomeBinding.include.cartNotification.setVisibility(View.VISIBLE);
            activityHomeBinding.include.cartNotification.setNumber(number);
        } else {
            activityHomeBinding.include.cartNotification.setVisibility(View.GONE);
        }

    }


    private void setBottomNavigate() {


        activityHomeBinding.include.home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentTransaction(HomeFragmentViewTwo.getInstance(), R.id.container);
            }
        });
        activityHomeBinding.include.checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentTransaction(CartFragment.getInstance(), R.id.container);
            }
        });

        activityHomeBinding.include.order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShopifyApplication.eventBus.post("");
            }
        });


       /* activityHomeBinding.include.bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {


            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.category)
                    fragmentTransaction(HomeFragment.getInstance(), R.id.container);
                if (item.getItemId() == R.id.percentage)
                    fragmentTransaction(HomeFragmentViewOne.getInstance(), R.id.container);
                if (item.getItemId() == R.id.like)
                    fragmentTransaction(HomeFragmentViewTwo.getInstance(), R.id.container);
                if (item.getItemId() == R.id.bag)
                    fragmentTransaction(CartFragment.getInstance(), R.id.container);
                if (item.getItemId() == R.id.user)
                    fragmentTransaction(CmsListing.getInstance(), R.id.container);
                return true;
            }
        });*/
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        if (activityHomeBinding.include.searchView.isOpen()) {
            // Close the search on the back button press.
            activityHomeBinding.include.searchView.closeSearch();
        } else {
            int count = getSupportFragmentManager().getBackStackEntryCount();
            if (count < 2) {
                finish();
            }
            super.onBackPressed();
        }
    }


    public void setVisibilityOfSearch(boolean visibilityOfSearch) {

        this.visibilityOfSearch = visibilityOfSearch;


    }


    private void logout() {
        MySharedPreferences.getInstance().storeData(ShopifyApplication.getAppContext(), Constants.ACCESS_TOKEN, null);
        MySharedPreferences.getInstance().storeData(ShopifyApplication.getAppContext(), Constants.EMAIL, null);
        MySharedPreferences.getInstance().storeData(ShopifyApplication.getAppContext(), Constants.SELECTED_CURRENCY, null);

        setOnActivityTransfer(LoginActivity.class);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // this takes the user 'back', as if they pressed the left-facing triangle icon on the main android toolbar.
                // if this doesn't work as desired, another possibility is to call `finish()` here.
                //Toast.makeText(HomeActivity.this, "alacalccca", Toast.LENGTH_LONG).show();
                onBackPressed();
                return true;

            case R.id.sort:
                initSortDialog();


                return true;


            case R.id.logout:

                logout();


                return true;

            case R.id.bag:

                fragmentTransaction(CartFragment.getInstance(), R.id.container);
                return true;

            case R.id.mode:

                productsInterface.onModeChange(mode);


                return true;
            case R.id.search:

                activityHomeBinding.include.searchView.openSearch();

                //  onSearchRequested();


                return true;
            /*case android.R.id.home:
                Toast.makeText(HomeActivity.this, "ff", Toast.LENGTH_SHORT).show();
                return true;
*/

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MaterialSearchView.REQUEST_VOICE && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size() > 0) {
                String searchWrd = matches.get(0);
                if (!TextUtils.isEmpty(searchWrd)) {
                    activityHomeBinding.include.searchView.setQuery(searchWrd, false);
                }
            }

            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    private void searchView() {
        //activityHomeBinding.include.searchView.adjustTintAlpha(0.8f);

        activityHomeBinding.include.searchView.setSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewOpened() {

                activityHomeBinding.include.appBar.setExpanded(false);
                // Do something once the view is open.
            }

            @Override
            public void onSearchViewClosed() {
                activityHomeBinding.include.appBar.setExpanded(true);
          /*      new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                    //    activityHomeBinding.include.toolbar.setVisibility(View.VISIBLE);
                    }
                }, 500);*/

                // Do something once the view is closed.
            }
        });

        activityHomeBinding.include.searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                activityHomeBinding.include.searchView.addSuggestion(query);
                String title = "title=" + query;
                // String title = "tags:" + "Accessories";

                apiClass.fetchCategoriesWithPaginationOnSearch(HomeActivity.this, HomeActivity.this, "", "", false, productsInterface, 0, query, swipeRefreshLayout);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        activityHomeBinding.include.searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Do something when the suggestion list is clicked.
                String suggestion = activityHomeBinding.include.searchView.getSuggestionAtPosition(position);
            //    closeSearchView();
                activityHomeBinding.include.searchView.setQuery(suggestion, false);
              //  apiClass.fetchCategoriesWithPaginationOnSearch(HomeActivity.this, HomeActivity.this, "", "", false, productsInterface, 0, suggestion, swipeRefreshLayout);

            }
        });
    }

    public void closeSearchView() {
        if (activityHomeBinding.include.searchView.isOpen())
            activityHomeBinding.include.searchView.closeSearch();
    }

    private void initSortDialog() {


        dialog = new Dialog(this, android.R.style.Theme_Translucent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.sort_dialog);


        RadioButton low = dialog.findViewById(R.id.lowToHigh);
        RadioButton high = dialog.findViewById(R.id.highToLow);
        RadioButton deafultRB = dialog.findViewById(R.id.deafultRB);
        RadioButton az = dialog.findViewById(R.id.az);
        RadioButton za = dialog.findViewById(R.id.za);
        ImageView close = dialog.findViewById(R.id.close);
        if (!ProductStaggeredFragment.selected_sort.isEmpty()) {
            if (ProductStaggeredFragment.selected_sort.equals("low")) {
                low.setChecked(true);
            } else if (ProductStaggeredFragment.selected_sort.equals("high")) {
                high.setChecked(true);
            } else if (ProductStaggeredFragment.selected_sort.equals("default")) {
                deafultRB.setChecked(true);
            } else if (ProductStaggeredFragment.selected_sort.equals("az")) {
                az.setChecked(true);
            } else if (ProductStaggeredFragment.selected_sort.equals("za")) {
                za.setChecked(true);
            }
        }

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });


        low.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {


                dialog.dismiss();
                if (productsInterface != null) {
                    productsInterface.onSort(ProductStaggeredFragment.selected_sort = "low");
                }
            }
        });


        high.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                dialog.dismiss();
                if (productsInterface != null) {
                    productsInterface.onSort(ProductStaggeredFragment.selected_sort = "high");
                }
            }
        });
        az.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                dialog.dismiss();
                if (productsInterface != null) {
                    productsInterface.onSort(ProductStaggeredFragment.selected_sort = "az");
                }
            }
        });
        za.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                dialog.dismiss();
                if (productsInterface != null) {
                    productsInterface.onSort(ProductStaggeredFragment.selected_sort = "za");
                }
            }
        });
        deafultRB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                dialog.dismiss();
                if (productsInterface != null) {
                    ProductStaggeredFragment.selected_sort = "";
                    productsInterface.onSort(ProductStaggeredFragment.selected_sort = "default");
                }
            }
        });


        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();/*
        searchView.setSearchableInfo(searchManager.getSearchableInfo(new ComponentName(this, SearchResultsActivity.class)));*/
       /* searchView.setIconifiedByDefault(false);*/
        // searchView.setSearchableInfo( searchManager.getSearchableInfo(getComponentName()) );
        //   menu.findItem(R.id.search).setVisible(visibilityOfSearch);

        // Associate searchable configuration with the SearchView
       /* SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        searchView.setIconifiedByDefault(false);

        SearchView.SearchAutoComplete searchAutoComplete = (SearchView.SearchAutoComplete) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchAutoComplete.setHintTextColor(Color.WHITE);
        searchAutoComplete.setTextColor(Color.WHITE);

      *//*  View searchplate = searchView.findViewById(android.support.v7.appcompat.R.id.search_plate);
        searchplate.setBackgroundResource(R.color.colorWhite);*//*

        ImageView searchCloseIcon = searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
        searchCloseIcon.setImageResource(R.drawable.ic_cross);


        ImageView searchIcon = searchView.findViewById(android.support.v7.appcompat.R.id.search_mag_icon);
        searchIcon.setImageResource(R.drawable.ic_search_new);


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {

                if (productsInterface != null) {
                    String title = "title=" + s;
                    // String title = "tags:" + "Accessories";

                    apiClass.fetchCategoriesWithPaginationOnSearch(HomeActivity.this, HomeActivity.this, "", "", false, productsInterface, 0, s, swipeRefreshLayout);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {


                return false;
            }
        });*/
        MenuItem menuItem = menu.findItem(R.id.search);
        MenuItem sort = menu.findItem(R.id.sort);
        MenuItem bag = menu.findItem(R.id.bag);
        MenuItem mode = menu.findItem(R.id.mode);
       /* menuItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                Log.d(Tag, "collapsed");
                sort.setVisible(true);
                bag.setVisible(true);
                mode.setVisible(true);
                // Do something when collapsed
                return true;       // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Do something when expanded
                Log.d(Tag, "expanded");
                activityHomeBinding.include.toolbar.setBackgroundColor(ContextCompat.getColor(HomeActivity.this,R.color.colorWhite));
                sort.setVisible(false);
                bag.setVisible(false);
                mode.setVisible(false);
                return true;      // Return true to expand action view
            }
        });*/
        setListnerOnMenuItem();
        return true;
    }



    public void getInstanceFromProductFragment(ProductsInterface productsInterface, SwipeRefreshLayout swipeRefreshLayout) {
        this.productsInterface = productsInterface;
        this.swipeRefreshLayout = swipeRefreshLayout;

    }


    @Override
    public void onStart() {
        super.onStart();
        ShopifyApplication.eventBus.register(this);
    }

    @Override
    public void onStop() {
        ShopifyApplication.eventBus.unregister(this);
        super.onStop();

    }


    @Subscribe
    public void onMessageEvent(String s) {




        /* Do something */
    }


   /* @Override
    public void onFooterClicked() {

    }

    @Override
    public void onHeaderClicked() {

    }



    @Override
    public void onOptionClicked(int position, Object objectClicked) {
        // setTitle(mTitles.get(position));

        // Set the right options selected
        mMenuAdapter.setViewSelected(position, true);

        // Navigate to the right fragment
        switch (position) {
            default:
                fragmentTransaction(HomeFragment.getInstance(), R.id.container);
                break;
        }

        // Close the drawer
        activityHomeBinding.drawerLayout.closeDrawer();
    }
*/

    private void setupTabIcons() {


    }


   /* @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.men) {
            // Handle the camera action
        } else if (id == R.id.women) {

        } else if (id == R.id.kids) {

        } else if (id == R.id.collection) {

        } else if (id == R.id.refer) {

        } else if (id == R.id.brands) {

        } else if (id == R.id.contact) {

        } else if (id == R.id.myOrder) {

        } else if (id == R.id.rate) {

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }*/
}
