package com.brst.shopifyapplication.Fragments;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Adaptors.ShopAdaptor;
import com.brst.shopifyapplication.ApiImplementation.ApiClass;
import com.brst.shopifyapplication.Interfaces.ProductListingInterface;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.databinding.FragmentHomeOneBinding;
import com.shopify.buy3.Storefront;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragmentViewOne extends BaseFragment {

    static HomeFragmentViewOne homeFragmentViewOne;
    View view;
    List<Storefront.Collection> parent_collectionss;
    ShopAdaptor shopAdaptor;
    FragmentHomeOneBinding fragmentHomeOneBinding;

    public HomeFragmentViewOne() {
        // Required empty public constructor
    }

    public static HomeFragmentViewOne getInstance() {
        if (homeFragmentViewOne == null) {
            return homeFragmentViewOne = new HomeFragmentViewOne();
        }
        return homeFragmentViewOne;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.search);
        MenuItem item1 = menu.findItem(R.id.sort);
        MenuItem item12 = menu.findItem(R.id.bag);
        MenuItem logout = menu.findItem(R.id.logout);
        MenuItem mode = menu.findItem(R.id.mode);
        mode.setVisible(false);
        item12.setVisible(false);
        item.setVisible(false);
        item1.setVisible(false);
        logout.setVisible(false);
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //      if (view == null) {
        fragmentHomeOneBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_home_one, container, false);
        view = fragmentHomeOneBinding.getRoot();
        ((HomeActivity)getActivity()).enableDrawer();

        setList();
        setUpViews();

        setUpAdaptor();
        //    }
        ((HomeActivity) getActivity()).setExpansion(true, false, 0);
        fetchCollection();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void setList() {
        parent_collectionss = new ArrayList<>();
    }

    public void fetchCollection() {


        //shopAdaptor.addList(((HomeActivity)getActivity()).collectionList);
        initProgressDialog(getActivity());
        ApiClass apiClass = new ApiClass();
        apiClass.fetchCategories(getActivity(), new ProductListingInterface() {
            @Override
            public void onSuccess(List<Storefront.Collection> collections) {
                collections.remove(0);
                parent_collectionss = collections;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dismissProgressDialog(getActivity());
                        shopAdaptor.addList(parent_collectionss);

                    }
                });


            }

            @Override
            public void onFailure(String error) {
                dismissProgressDialog(getActivity());

            }

            @Override
            public void onError() {
                dismissProgressDialog(getActivity());

            }
        });
    }


    private void setUpAdaptor() {

        shopAdaptor = new ShopAdaptor(getActivity(), parent_collectionss);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        fragmentHomeOneBinding.recyclerViewShop.setLayoutManager(layoutManager);
        fragmentHomeOneBinding.recyclerViewShop.setAdapter(shopAdaptor);

    }

    private void setUpViews() {


    }


}
