package com.brst.shopifyapplication.Adaptors.ViewHolders;

import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Beans.CategorySubCategoryBean;
import com.brst.shopifyapplication.Fragments.ProductStaggeredFragment;
import com.brst.shopifyapplication.R;


public class SubCategoryViewHolder extends ChildViewHolder {

    String TAG = "SubCategoryViewHolder";
    private TextView mIngredientTextView;

    public SubCategoryViewHolder(@NonNull final View itemView) {
        super(itemView);
        mIngredientTextView = (TextView) itemView.findViewById(R.id.ingredient_textview);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "==" + getChildAdapterPosition() + "==" + getParentAdapterPosition());
                ProductStaggeredFragment productStaggeredFragment = new ProductStaggeredFragment();
                ((HomeActivity) itemView.getContext()).fragmentTransaction(productStaggeredFragment, R.id.container);
            }
        });
    }

    public void bind(@NonNull CategorySubCategoryBean categorySubCategoryBean) {
        mIngredientTextView.setText(categorySubCategoryBean.getName());
    }
}
