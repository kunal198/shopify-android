package com.brst.shopifyapplication.Fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Adaptors.GalleryViewAdapter;
import com.brst.shopifyapplication.Adaptors.ImagePagerAdapter;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.databinding.LayoutGalleryViewBinding;
import com.shopify.buy3.Storefront;

import java.util.List;


public class GalleryViewFragment extends BaseFragment {
    private String TAG = getClass().getSimpleName();
    boolean stopCallingFirstTimeOnPageSelected = false;

    static GalleryViewFragment productFragment;
    View view;
    LayoutGalleryViewBinding layoutGalleryViewBinding;
    List<Storefront.ImageEdge> imageEdges = null;
    GalleryViewAdapter galleryViewAdapter;

    Handler handler;
    Runnable runnable;

    public static GalleryViewFragment getInstance() {
        if (productFragment == null) {
            return productFragment = new GalleryViewFragment();
        }
        return productFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //  if (view == null) {
        layoutGalleryViewBinding = DataBindingUtil.inflate(
                inflater, R.layout.layout_gallery_view, container, false);
        view = layoutGalleryViewBinding.getRoot();
        stopCallingFirstTimeOnPageSelected = false;
        handler = new Handler();

        initView();
        //   getActivity().setTitle("");

        getDataFromBundle();

        hideView();

        //    }
        ((HomeActivity) getActivity()).setExpansion(true, false, 0);

        ((HomeActivity)getActivity()).backButtonToolBar();


        //  ((HomeActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.search);
        MenuItem item1 = menu.findItem(R.id.sort);
        MenuItem item12 = menu.findItem(R.id.bag);
        MenuItem logout = menu.findItem(R.id.logout);
        MenuItem mode = menu.findItem(R.id.mode);
        mode.setVisible(false);
        item12.setVisible(false);
        item.setVisible(false);
        item1.setVisible(false);
        logout.setVisible(false);

    }


    private void initView() {


    }

    private void getDataFromBundle() {
        Bundle bundle = getArguments();


        if (bundle != null) {
            imageEdges = (List<Storefront.ImageEdge>) bundle.getSerializable("image_list");
            int position = bundle.getInt("position");

            layoutGalleryViewBinding.smallPreviewRV.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);

            galleryViewAdapter = new GalleryViewAdapter(getActivity(), GalleryViewFragment.this, layoutGalleryViewBinding.smallPreviewRV, imageEdges, position, new PostionListener() {
                @Override
                public void setPosition(int position) {

                    layoutGalleryViewBinding.productPreviewMainVP.setCurrentItem(position, true);
                }
            });

            layoutGalleryViewBinding.smallPreviewRV.setLayoutManager(layoutManager);
            layoutGalleryViewBinding.smallPreviewRV.setAdapter(galleryViewAdapter);
            layoutGalleryViewBinding.smallPreviewRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    hideView();

                }
            });
            setupViewPager(imageEdges, R.layout.pager_item2, position);

        }


    }


    private void hideView() {

        if (runnable != null) {
            handler.removeCallbacks(runnable);
        }

        runnable = new Runnable() {
            @Override
            public void run() {
                if (layoutGalleryViewBinding.smallPreviewRV.getVisibility() == View.VISIBLE) {

                    slideDown(layoutGalleryViewBinding.smallPreviewRV);
                }
            }
        };


        handler.postDelayed(runnable, 6000);
    }

    public void slideUp(View view) {
        layoutGalleryViewBinding.smallPreviewRV.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    // slide the view from its current position to below itself
    public void slideDown(View view) {
        layoutGalleryViewBinding.smallPreviewRV.setVisibility(View.GONE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }


    private void setupViewPager(List<Storefront.ImageEdge> banner_list, int layout, int pos) {

        ImagePagerAdapter imagePagerAdapter = new ImagePagerAdapter(getActivity(), banner_list, layout, new PostionListener.PostionTouchListener() {
            @Override
            public void touchListener() {
                hideView();
                if (layoutGalleryViewBinding.smallPreviewRV.getVisibility() == View.VISIBLE) {
                    slideDown(layoutGalleryViewBinding.smallPreviewRV);

                } else {
                    slideUp(layoutGalleryViewBinding.smallPreviewRV);
                }

            }
        });
        layoutGalleryViewBinding.productPreviewMainVP.setAdapter(imagePagerAdapter);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                layoutGalleryViewBinding.productPreviewMainVP.setCurrentItem(pos, true);
                layoutGalleryViewBinding.smallPreviewRV.smoothScrollToPosition(pos);

            }
        };
        handler.post(runnable);
        layoutGalleryViewBinding.productPreviewMainVP.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                Log.d(TAG, "onPageScrolled" + position);
                hideView();


            }

            @Override
            public void onPageSelected(int position) {
                if (stopCallingFirstTimeOnPageSelected) {
                    Log.d(TAG, "onPageSelected" + position);
                    galleryViewAdapter.notifyData(position);
                    layoutGalleryViewBinding.smallPreviewRV.smoothScrollToPosition(position);
                }
                Log.d(TAG, "onPageSelected" + position);

                stopCallingFirstTimeOnPageSelected = true;

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Log.d(TAG, "onPageScrollStateChanged" + state);

            }
        });

    }


    public interface PostionListener {

        public void setPosition(int position);

        public interface PostionTouchListener {

            public void touchListener();

        }
    }

}

