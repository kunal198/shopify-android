package com.brst.shopifyapplication.Activities.LoginPackage;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Activities.ParentActivity;
import com.brst.shopifyapplication.Activities.SignUpPackage.SignUpActivity;
import com.brst.shopifyapplication.ApiImplementation.ApiClass;
import com.brst.shopifyapplication.HelperClasses.Constants;
import com.brst.shopifyapplication.HelperClasses.MySharedPreferences;
import com.brst.shopifyapplication.HelperClasses.Validator;
import com.brst.shopifyapplication.Interfaces.LoginSignupCallBack;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.ShopifyApplication;
import com.brst.shopifyapplication.databinding.ActivityLoginBinding;

import org.greenrobot.eventbus.Subscribe;


public class LoginActivity extends ParentActivity implements LogInInterfaces.LoginViewInterface {

    ActivityLoginBinding activityLoginBinding;
    String Tag = "LoginActivity";
    String previousActivity;
    LoginActivity loginActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loginActivity = LoginActivity.this;
        activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        Intent mIntent = getIntent();
        previousActivity = mIntent.getStringExtra("FROM_ACTIVITY");

        activityLoginBinding.signUpTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                hideKeyboard(LoginActivity.this);
                Intent mIntent = new Intent(LoginActivity.this, SignUpActivity.class); //'this' is Activity B
                mIntent.putExtra("FROM_ACTIVITY", "Login");
                startActivity(mIntent);
            }
        });

        activityLoginBinding.skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (previousActivity != null && previousActivity.equals("Login")) {
                    finish();
                } else {
                    hideKeyboard(LoginActivity.this);
                    setOnActivityTransfer(HomeActivity.class);
                    finish();
                }

            }
        });


        activityLoginBinding.logInBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(LoginActivity.this);

                String email = activityLoginBinding.emailET.getText().toString();
                String pass = activityLoginBinding.passET.getText().toString();
                if (Validator.loginValidation(LoginActivity.this, email, pass)) {

                    clearFocus(activityLoginBinding.emailET);
                    clearFocus(activityLoginBinding.passET);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            initProgressDialog(LoginActivity.this);

                        }
                    });
                    new ApiClass().loginProcess(LoginActivity.this, email, pass, new LoginSignupCallBack() {
                        @Override
                        public void onSuccess(String accessToken) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    MySharedPreferences.getInstance().storeData(ShopifyApplication.getAppContext(), Constants.ACCESS_TOKEN, accessToken);
                                    MySharedPreferences.getInstance().storeData(ShopifyApplication.getAppContext(), Constants.EMAIL, activityLoginBinding.emailET.getText().toString());
                                   // Toast.makeText(LoginActivity.this, "Access Token is" + accessToken, Toast.LENGTH_SHORT).show();
                                    Toast.makeText(LoginActivity.this, "Logged In Sucessfully!", Toast.LENGTH_SHORT).show();
                                    dismissProgressDialog(LoginActivity.this);

                                    if (previousActivity != null && previousActivity.equals("Login")) {
                                        finish();
                                    } else {
                                        hideKeyboard(LoginActivity.this);
                                        setOnActivityTransfer(HomeActivity.class);
                                        finish();
                                    }

                                }
                            });
                        }

                        @Override
                        public void onFailure() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(LoginActivity.this, "Wrong Email and password", Toast.LENGTH_SHORT).show();

                                    dismissProgressDialog(LoginActivity.this);

                                }
                            });
                        }

                        @Override
                        public void onError(String errorr) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(LoginActivity.this, "Wrong Email and password", Toast.LENGTH_SHORT).show();

                                    dismissProgressDialog(LoginActivity.this);

                                }
                            });
                        }
                    });
                }


                // setOnActivityTransfer(HomeActivity.class);
                // finish();
            }
        });
    }

    private void initView() {

    }


    @Subscribe
    public void onMessageEvent() {




        /* Do something */
    }

    @Override
    public void setEmailValidError() {

    }

    @Override
    public void setEmailEmptyError() {

    }

    @Override
    public void setPasswordEmptyError() {

    }

    @Override
    public void setPasswordLimitError() {

    }

    @Override
    public void navigateToHome() {

    }
}
