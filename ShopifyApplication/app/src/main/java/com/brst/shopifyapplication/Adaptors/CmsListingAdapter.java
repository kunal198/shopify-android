package com.brst.shopifyapplication.Adaptors;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.brst.shopifyapplication.Fragments.CmsListing;
import com.brst.shopifyapplication.Fragments.CmsPagesDetail;
import com.brst.shopifyapplication.R;
import com.google.gson.JsonArray;

/**
 * Created by brst-pc89 on 12/30/17.
 */

public class CmsListingAdapter extends RecyclerView.Adapter<CmsListingAdapter.ViewHolder> {

    Context context;
    JsonArray categoryList;
    Fragment fragment;

    public CmsListingAdapter(Context context, JsonArray categoryList, Fragment fragment, RecyclerView recyclerView) {

        this.context = context;
        this.categoryList = categoryList;
        this.fragment = fragment;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // return null;
        View view = LayoutInflater.from(context).inflate(R.layout.layout_row_cms, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.title.setText(categoryList.get(position).getAsJsonObject().get("title").getAsString());
    }


    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public void adddata(JsonArray categoryList) {
        this.categoryList = categoryList;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;

        public ViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String title = categoryList.get(getAdapterPosition()).getAsJsonObject().get("title").getAsString();
                    String id = categoryList.get(getAdapterPosition()).getAsJsonObject().get("id").getAsString();
                    Bundle bundle = new Bundle();
                    bundle.putString("title", title);
                    bundle.putString("id", id);

                    ((CmsListing) fragment).fragmentTransaction(CmsPagesDetail.getInstance(), R.id.container, bundle);


                }
            });
        }
    }
}
