package com.brst.shopifyapplication.Fragments;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Adaptors.ShippingMethodAdpater;
import com.brst.shopifyapplication.ApiImplementation.ApiClass;
import com.brst.shopifyapplication.HelperClasses.Constants;
import com.brst.shopifyapplication.Interfaces.CheckoutInterface;
import com.brst.shopifyapplication.Interfaces.PaymentInterface;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.databinding.LayoutPaymentBinding;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wallet.MaskedWallet;
import com.google.android.gms.wallet.MaskedWalletRequest;
import com.google.android.gms.wallet.Wallet;
import com.google.android.gms.wallet.WalletConstants;
import com.shopify.buy3.CardClient;
import com.shopify.buy3.CreditCard;
import com.shopify.buy3.CreditCardVaultCall;
import com.shopify.buy3.Storefront;
import com.shopify.buy3.pay.PayCart;
import com.shopify.buy3.pay.PayHelper;
import com.shopify.graphql.support.ID;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;


public class ShippingPaymentFragment extends BaseFragment implements GoogleApiClient.ConnectionCallbacks {
    static ShippingPaymentFragment productFragment;
    Storefront.MailingAddressInput input;
    GoogleApiClient googleApiClient;
    View view;
    LayoutPaymentBinding layoutPaymentBinding;
    ShippingMethodAdpater shippingMethodAdpater;
    List<Storefront.ShippingRate> data_list;
    String subtotalPrice = null, shipping_charges = "", shipping_handle = "", tax = "", total_price = "";
    ID checkoutId;
    String discount_on_order = "";
    private String Tag = "ShippingMethodFragment";

    /*public void setData(Storefront.Collection collection) {
        data = new ArrayList<>();
        data.add(collection);

    }
*/
    public static ShippingPaymentFragment getInstance() {
        if (productFragment == null) {
            return productFragment = new ShippingPaymentFragment();
        }
        return productFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // if (view == null) {
        layoutPaymentBinding = DataBindingUtil.inflate(
                inflater, R.layout.layout_payment, container, false);
        view = layoutPaymentBinding.getRoot();

        initView();

        getActivity().setTitle("Checkout Process");
        getDataFromBundle();

        addDiscountCoupanToCheckOut();
        //    }
        setClickListner();
        ((HomeActivity) getActivity()).setExpansion(true, false, 0);
        checkBoxListner();
        return view;
    }

    private void checkBoxListner() {
        layoutPaymentBinding.creditCard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    layoutPaymentBinding.webCheckout.setChecked(false);
                }
            }
        });
        layoutPaymentBinding.webCheckout.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    layoutPaymentBinding.creditCard.setChecked(false);
                }
            }
        });


    }


    private void setClickListner() {

/*

        layoutPaymentBinding.discountNameTV.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getRawX() >= (layoutPaymentBinding.discountNameTV.getRight() - layoutPaymentBinding.discountNameTV.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    // your action here
                    openShippingCharges(checkoutId);

                    return true;
                }

                return false;
            }
        });
*/

        layoutPaymentBinding.addressTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                replaceFragment(ShippingAddressFragment.getInstance(), getActivity());

            }
        });

        layoutPaymentBinding.shippingTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                replaceFragment(ShippingMethodFragment.getInstance(), getActivity());

            }
        });


        layoutPaymentBinding.saveBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (layoutPaymentBinding.webCheckout.isChecked()) {
                    webCheckout();
                } else if (layoutPaymentBinding.creditCard.isChecked()) {
                    creditCard();
                } else {
                    showMessage("Please choose an payment method.");
                }
            }
        });


        layoutPaymentBinding.androidPayCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             /*   initProgressDialog(getActivity());
                new ApiClass().shippingLineUpdate(getActivity(), checkoutId, data_list.get(shippingMethodAdpater.selectedPosition).getHandle(), new CheckoutInterface() {
                    @Override
                    public void onSucessfullCheckout(Storefront.Checkout checkout) {
                        //  String checkoutWebUrl = checkout.getWebUrl();
                        ID checkoutId = checkout.getId();
                        String url = checkout.getWebUrl();

                        String getPaymentDue = String.valueOf(checkout.getPaymentDue());
                        String getSubtotalPrice = String.valueOf(checkout.getSubtotalPrice());
                        String getTotalPrice = String.valueOf(checkout.getTotalPrice());
                        payWithAndroidPay(checkout.getSubtotalPrice(), checkout.getTotalPrice());
                        Bundle bundle = new Bundle();
                        bundle.putString("url", url);
                        fragmentTransaction(WebViewFragment.getInstance(), R.id.container, bundle);
                        dismissProgressDialog(getActivity());
                    }

                    @Override
                    public void onFailure() {
                        dismissProgressDialog(getActivity());

                    }

                    @Override
                    public void onError(String error) {
                        dismissProgressDialog(getActivity());

                    }
                });*/
            }
        });


    }


    private void creditCard() {

        initProgressDialog(getActivity());
        new ApiClass().shippingLineUpdate(getActivity(), checkoutId, shipping_handle, new CheckoutInterface() {
            @Override
            public void onSucessfullCheckout(Storefront.Checkout checkout) {
                //  String checkoutWebUrl = checkout.getWebUrl();
                ID checkoutId = checkout.getId();
                String url = checkout.getWebUrl();
                BigDecimal getTotalPrice = checkout.getTotalPrice();
                dismissProgressDialog(getActivity());

                Bundle bundle = new Bundle();
                bundle.putSerializable("ID", checkoutId);
                bundle.putSerializable("address_input", input);
                bundle.putSerializable("amount", getTotalPrice);
                fragmentTransaction(CreditCardFragment.getInstance(), R.id.container, bundle);


                //   String getPaymentDue = String.valueOf(checkout.getPaymentDue());
                //  String getSubtotalPrice = String.valueOf(checkout.getSubtotalPrice());
                //  String getTotalPrice = String.valueOf(checkout.getTotalPrice());

                /*        Bundle bundle = new Bundle();
                        bundle.putString("url", url);
                        fragmentTransaction(WebViewFragment.getInstance(), R.id.container, bundle);
                        dismissProgressDialog(getActivity());*/
            }

            @Override
            public void onFailure() {
                dismissProgressDialog(getActivity());

            }

            @Override
            public void onError(String error) {
                dismissProgressDialog(getActivity());

            }
        });

    }

    private void webCheckout() {
        initProgressDialog(getActivity());
        new ApiClass().shippingLineUpdate(getActivity(), checkoutId, shipping_handle, new CheckoutInterface() {
            @Override
            public void onSucessfullCheckout(Storefront.Checkout checkout) {
                //  String checkoutWebUrl = checkout.getWebUrl();

                ID checkoutId = checkout.getId();
                String url = checkout.getWebUrl();

                String getPaymentDue = String.valueOf(checkout.getPaymentDue());
                String getSubtotalPrice = String.valueOf(checkout.getSubtotalPrice());
                String getTotalPrice = String.valueOf(checkout.getTotalPrice());

                Bundle bundle = new Bundle();
                bundle.putString("url", url);
                fragmentTransaction(WebViewFragment.getInstance(), R.id.container, bundle);
                dismissProgressDialog(getActivity());
            }

            @Override
            public void onFailure() {
                dismissProgressDialog(getActivity());

            }

            @Override
            public void onError(String error) {
                dismissProgressDialog(getActivity());

            }
        });

    }

    private void creditCard(String vault_url, ID checkoutId, BigDecimal amount) {

        CardClient cardClient = new CardClient();
        CreditCard creditCard = CreditCard.builder()
                .firstName("John")
                .lastName("Smith")
                .number("4242424242424242")
                .expireMonth("12")
                .expireYear("18")
                .verificationCode("123")
                .build();

        cardClient.vault(creditCard, vault_url).enqueue(new CreditCardVaultCall.Callback() {
            @Override
            public void onResponse(@NonNull String token) {

                String s = token;
                new ApiClass().payWithCreditCard(getActivity(), checkoutId, amount, token, input, new PaymentInterface() {
                    @Override
                    public void onPayment(Storefront.Payment response, ID checkout_id) {
                        boolean paymentReady = response.getReady();
                        boolean paymentReady1 = response.getTest();
                        String getAmount = String.valueOf(response.getAmount());

                        String pay_id = String.valueOf(response.getId());
                        new ApiClass().completeCheckoutViaCreditCard(getActivity(), checkout_id, new CheckoutInterface() {
                            @Override
                            public void onSucessfullCheckout(Storefront.Checkout checkout) {

                            }

                            @Override
                            public void onFailure() {

                            }

                            @Override
                            public void onError(String error) {

                            }
                        });

                    }

                    @Override
                    public void onFailure(String error) {

                    }

                    @Override
                    public void onError(String error) {

                    }
                });
                // proceed to complete checkout with token
            }

            @Override
            public void onFailure(@NonNull IOException error) {
                // handle error
            }
        });


    }

    private void payWithAndroidPay(BigDecimal sub_price, BigDecimal total_price) {

        PayCart payCart = PayCart.builder()
                .merchantName(Constants.MERCHANT_NAME)
                .currencyCode(getCurrency())
                .shippingAddressRequired(true)
                .phoneNumberRequired(true)
                .shipsToCountries(Arrays.asList("US", "CA"))
                .addLineItem("Product1", 1, BigDecimal.valueOf(1.99))
                .addLineItem("Product2", 10, BigDecimal.valueOf(3.99))
                .subtotal(sub_price)
                .totalPrice(total_price)
                .countryCode("IN")
                .build();


        MaskedWalletRequest maskedWalletRequest = payCart.maskedWalletRequest(Constants.PAY_API_KEY);
        if (PayHelper.isAndroidPayEnabledInManifest(getActivity())) {


            googleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(Wallet.API, new Wallet.WalletOptions.Builder()
                            .setEnvironment(WalletConstants.ENVIRONMENT_SANDBOX)
                            .setTheme(WalletConstants.THEME_DARK)
                            .build())
                    .addConnectionCallbacks(this)
                    .build();


            googleApiClient.connect();
            PayHelper.requestMaskedWallet(googleApiClient, payCart, Constants.PAY_API_KEY);

// show Android Pay button
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(Tag, "onActivityResult" + requestCode + " " + resultCode);

        PayHelper.handleWalletResponse(requestCode, resultCode, data, new PayHelper.WalletResponseHandler() {

            @Override
            public void onWalletError(int requestCode, int errorCode) {
                Log.d(Tag, "onWalletError" + requestCode + " " + errorCode);

                // show error, errorCode is one of defined in com.google.android.gms.wallet.WalletConstants
            }

            @Override
            public void onMaskedWallet(final MaskedWallet maskedWallet) {
                Log.d(Tag, "maskedWallet" + requestCode + " " + resultCode);

                // show order confirmation screen to complete checkout
                // update checkout with shipping address from Masked Wallet
                // fetch shipping rates
            }
        });
    }


    private void addDiscountCoupanToCheckOut() {

        layoutPaymentBinding.applyBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String discount_code = layoutPaymentBinding.discountCodeET.getText().toString();

                hideKeyboard(getActivity());
                if (!discount_code.isEmpty()) {
                    initProgressDialog(getActivity());
                    new ApiClass().applyGiftCard(getActivity(), checkoutId, discount_code, new CheckoutInterface() {
                        @Override
                        public void onSucessfullCheckout(Storefront.Checkout checkout) {
                            dismissProgressDialog(getActivity());
                            //  String checkoutWebUrl = checkout.getWebUrl();
                            ID checkoutId = checkout.getId();

                            String discoupan_text = layoutPaymentBinding.discountCodeET.getText().toString();
                            String getPaymentDue = String.valueOf(checkout.getPaymentDue());
                            String getSubtotalPrice = String.valueOf(checkout.getSubtotalPrice());
                            String getTotalPrice = String.valueOf(checkout.getTotalPrice());
                            // subtotalPrice=getSubtotalPrice;
                            if (subtotalPrice.equals(getSubtotalPrice)) {
                                showMessage("Wrong coupan");
                            } else {
                                showMessage("Coupan applied sucessfully");
                                setLabel(discoupan_text);
                                double discount = Double.parseDouble(subtotalPrice) - Double.parseDouble(getSubtotalPrice);
                                showText(String.valueOf(discount), getSubtotalPrice);
                                discount_on_order = String.valueOf(discount);

                            }


                        }

                        @Override
                        public void onFailure() {
                            dismissProgressDialog(getActivity());

                        }

                        @Override
                        public void onError(String error) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
                                }
                            });
                            dismissProgressDialog(getActivity());

                        }
                    });
                } else {
                    Toast.makeText(getActivity(), "Please enter the discount code", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void setLabel(String discoupan_text) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                layoutPaymentBinding.discountNameTV.setText(discoupan_text);
                layoutPaymentBinding.discountCodeET.getText().clear();

            }
        });
    }

    private void showMessage(String msg) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void showText(String discount, String paymentdue) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                layoutPaymentBinding.discountLL.setVisibility(View.VISIBLE);
                layoutPaymentBinding.discountCoupan.setText(getCurrency() + formatString(discount));
                double due = Double.parseDouble(paymentdue) + Double.parseDouble(shipping_charges) + Double.parseDouble(tax);
                layoutPaymentBinding.totalTV.setText(getCurrency() + formatString(String.valueOf(due)));

            }
        });
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.search);
        MenuItem item1 = menu.findItem(R.id.sort);
        MenuItem item12 = menu.findItem(R.id.bag);
        MenuItem logout = menu.findItem(R.id.logout);
        MenuItem mode = menu.findItem(R.id.mode);
        mode.setVisible(false);
        item12.setVisible(false);
        item.setVisible(false);
        item1.setVisible(false);
        logout.setVisible(false);
    }

    private void initView() {


    }

    private void getDataFromBundle() {
        Bundle bundle = getArguments();

        if (bundle != null) {
            checkoutId = (ID) bundle.getSerializable("checkoutId");
            total_price = bundle.getString("totalPrice");
            subtotalPrice = bundle.getString("sub_total");
            shipping_charges = bundle.getString("shipping_price");
            shipping_handle = bundle.getString("shipping_handle");
            tax = bundle.getString("tax");
            input = (Storefront.MailingAddressInput) bundle.getSerializable("input");

            layoutPaymentBinding.subtotalTV.setText(subtotalPrice != null ? getCurrency() + formatString(subtotalPrice) : "");

            layoutPaymentBinding.shippingChargesTV.setText(shipping_charges != null ? getCurrency() + formatString(shipping_charges) : "");
            double price = Double.parseDouble(String.valueOf(total_price));
            double ship_price = Double.parseDouble(String.valueOf(shipping_charges));
            double t_p = price + ship_price;

            layoutPaymentBinding.totalTV.setText(getCurrency() + formatString(String.valueOf(t_p)));
            if (tax != null && !tax.isEmpty()) {
                layoutPaymentBinding.taxLL.setVisibility(View.VISIBLE);
                layoutPaymentBinding.taxesTV.setText(getCurrency() + formatString(tax));
            }


        }


    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}

