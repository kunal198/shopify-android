package com.brst.shopifyapplication.Interfaces;

/**
 * Created by brst-pc20 on 12/18/17.
 */

public interface CartInterface {


    public void addClicked(int adapter_pos,String pos);

    public void subClicked(int pos);

    public void deleteItem(int pos);


}
