package com.brst.shopifyapplication.Adaptors;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.brst.shopifyapplication.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;


/**
 * Created by brst-pc20 on 1/9/17.
 */

public class CategoryListingAdapter extends RecyclerView.Adapter<CategoryListingAdapter.MyViewHolder> {

    public boolean isLoading = false;
    ArrayList<Integer> data_list;
    Context context;
    Fragment fragment;
    RecyclerView recyclerView;
    private int lastVisibleItem,
            totalItemCount, firstVisiblePos, firstVisibleInListview;
    private int visibleThreshold = 5;
    //LoadMoreItems loadMoreItemss;

    public CategoryListingAdapter(Context context, Fragment fragment, RecyclerView recyclerView, ArrayList<Integer> data_list) {

        this.context = context;
        this.data_list = data_list;

        this.fragment = fragment;
        this.recyclerView = recyclerView;
      /*  this.loadMoreItemss = loadMoreItems;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        firstVisibleInListview = linearLayoutManager.findFirstVisibleItemPosition();


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                firstVisiblePos = linearLayoutManager.findFirstVisibleItemPosition();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (loadMoreItemss != null) {
                        loadMoreItemss.LoadItems();
                    }

                }
                Log.d("dyyyy", "=" + dy);

            }
        });*/

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_listing_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        //  Cartbean cartbean = quantity_list.get(position);
        //  holder.quantityTV.setText(""+cartbean.getGetQuantity());


        Glide.with(context).load(data_list.get(position)).into(holder.categoryIV);

    }

    @Override
    public int getItemCount() {
        return data_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView categoryIV;

        public MyViewHolder(View view) {
            super(view);

            categoryIV = view.findViewById(R.id.categoryIV);


          /*  priceTV = view.findViewById(R.id.priceTV);
            postNameTV = view.findViewById(R.id.postNameTV);
            locationTV = view.findViewById(R.id.locationTV);
            descriptionTV = view.findViewById(R.id.descriptionTV);
            timeAgoTV = view.findViewById(R.id.timeAgoTV);
            dayLeftTV = view.findViewById(R.id.dayLeftTV);
            hourLeftTV = view.findViewById(R.id.hourLeftTV);
            minLeftTV = view.findViewById(R.id.minLeftTV);*/
/*

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    GetProductsBean.Datum datum = data_list.get(getAdapterPosition());
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("data", datum);


                    ((ListPostsFragment) fragment).fragmentTransaction(PostDetailfragment.getInstance(), R.id.frag_container, bundle);
                }
            });
*/

        }
    }

 /*   public void addList(ArrayList<GetProductsBean.Datum> data) {
        this.data_list = data;
        notifyDataSetChanged();
    }
*/
}