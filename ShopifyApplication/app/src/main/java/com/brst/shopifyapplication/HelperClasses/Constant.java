package com.brst.shopifyapplication.HelperClasses;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.brst.shopifyapplication.Async_Thread.Super_AsyncTask;

import java.util.ArrayList;

/**
 * Created by brst-pc20 on 1/3/17.
 */

public class Constant {


    // public static String GET_COUNTRIES="http://api.geonames.org/countryInfoJSON?username=brst";
    public static String GET_CITIES = "http://api.geonames.org/childrenJSON?geonameId=";
    public static String GEONAME_USERNAME = "&username=brst";

    public static ArrayList<String> category_list = new ArrayList<>();


    private static Context context;

    public Constant(Context context) {
        this.context = context;
    }


    public static void execute(Super_AsyncTask asyncTask) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            asyncTask.execute();
        }

    }

    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }


}
