package com.brst.shopifyapplication.Interfaces;

import com.google.gson.JsonObject;
import com.shopify.buy3.Storefront;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface ShopifyApiInterface {
    //@GET("pages.json")

    @GET("pages/{id}.json")
    Call<JsonObject> getPages(@Path("id") String id, @Header("authorization") String authkey);

    @GET("pages.json")
    Call<JsonObject> getPagesListing(@Header("authorization") String authkey);

    @GET("products.json?title={query}")
    Call<Storefront.QueryRoot> searchProducts(@Path("query") String query, @Header("authorization") String authkey);
}
