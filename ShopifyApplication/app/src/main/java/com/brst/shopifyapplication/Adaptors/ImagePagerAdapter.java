package com.brst.shopifyapplication.Adaptors;


import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.brst.shopifyapplication.Fragments.GalleryViewFragment;
import com.brst.shopifyapplication.R;
import com.bumptech.glide.Glide;
import com.shopify.buy3.Storefront;

import java.util.ArrayList;
import java.util.List;

public class ImagePagerAdapter extends PagerAdapter {

    int layout;
    private String TAG = getClass().getSimpleName();
    private Context con;
    private List<Storefront.ImageEdge> imageIdList = new ArrayList<>();
    private GalleryViewFragment.PostionListener.PostionTouchListener postionTouchListener;


    public ImagePagerAdapter(Context con, List<Storefront.ImageEdge> banner_list, int layout, GalleryViewFragment.PostionListener.PostionTouchListener postionTouchListener) {
        Log.d(TAG, "Constructor");
        this.con = con;
        imageIdList = banner_list;
        this.layout = layout;
        this.postionTouchListener = postionTouchListener;

    }

    @Override
    public int getCount() {
        return imageIdList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup row, int position) {
        LayoutInflater inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(layout, null);

        // TouchImageView image =  v.findViewById(R.id.banner_image);

        Log.d(TAG, "Constructor" + position);
        ImageView image = v.findViewById(R.id.banner_image);

        Storefront.ImageEdge imageEdge = imageIdList.get(position);


        if (imageEdge.getNode().getSrc() != null && !imageEdge.getNode().getSrc().isEmpty()) {

            Glide.with(con).load(imageEdge.getNode().getSrc()).into(image);
        }
        row.addView(v);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postionTouchListener.touchListener();
            }
        });

        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }


}
