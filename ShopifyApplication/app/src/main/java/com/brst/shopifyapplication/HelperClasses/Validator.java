package com.brst.shopifyapplication.HelperClasses;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.widget.Toast;

/**
 * Created by brst-pc20 on 12/13/17.
 */

public class Validator {


    public static boolean signUpValidation(Context context, String name, String lastName, String phoneNo, String email, String password, String confirm_pass) {

        if (name.isEmpty()) {
            Toast.makeText(context, "Please enter your first name", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (lastName.isEmpty()) {
            Toast.makeText(context, "Please enter your last name", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (phoneNo.isEmpty()) {
            Toast.makeText(context, "Please enter your phone number", Toast.LENGTH_SHORT).show();
            return false;


        }
        if (phoneNo.length() > 13 || phoneNo.length() < 6) {
            Toast.makeText(context, "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            return false;


        }
        if (email.isEmpty() || !email.contains("@")) {
            Toast.makeText(context, "Please enter your valid email", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (password.isEmpty()) {
            Toast.makeText(context, "Please enter your password", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!password.equals(confirm_pass)) {
            Toast.makeText(context, "Password do not match", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public static boolean shippingValidation(Context context, String name, String lastName, String address_one, String address_two, String city, String state, String zip, String country, String phoneNo) {

        if (name.isEmpty()) {
            Toast.makeText(context, "Please enter your first name", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (lastName.isEmpty()) {
            Toast.makeText(context, "Please enter your last name", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (address_one.isEmpty()) {
            Toast.makeText(context, "Please fill Address Line 1", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (address_two.isEmpty()) {
            Toast.makeText(context, "Please fill Address Line 2", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (city.isEmpty()) {
            Toast.makeText(context, "Please enter city", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (state.isEmpty()) {
            Toast.makeText(context, "Please enter state", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (zip.isEmpty()) {
            Toast.makeText(context, "Please enter zip Code", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (zip.length() > 12) {
            Toast.makeText(context, "Please enter valid zip Code", Toast.LENGTH_SHORT).show();
            return false;
        }


        if (country.isEmpty()) {
            Toast.makeText(context, "Please enter your Country", Toast.LENGTH_SHORT).show();
            return false;


        }
        if (phoneNo.length() > 13 || phoneNo.length() < 6) {
            Toast.makeText(context, "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            return false;


        }

        return true;
    }

    public static boolean loginValidation(Context context, String email, String password) {


        if (email.isEmpty() || !email.contains("@")) {
            Toast.makeText(context, "Please enter your valid email", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (password.isEmpty()) {
            Toast.makeText(context, "Please enter your password", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public static boolean validCreditCardDetails(Context context, TextInputEditText firstname, TextInputEditText lastname, TextInputEditText card_no, TextInputEditText cvv) {


        if (getTextFromEditText(card_no).isEmpty() || getTextFromEditText(card_no).length() < 16) {
            Toast.makeText(context, "Please enter your valid Credit card no", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (getTextFromEditText(firstname).isEmpty()) {
            Toast.makeText(context, "Please enter your first name", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (getTextFromEditText(lastname).isEmpty()) {
            Toast.makeText(context, "Please enter your last name", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (getTextFromEditText(cvv).isEmpty() || getTextFromEditText(cvv).length() < 3) {
            Toast.makeText(context, "Please enter your valid cvv number", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private static String getTextFromEditText(TextInputEditText textInputEditText) {
        return textInputEditText.getText().toString();
    }
}