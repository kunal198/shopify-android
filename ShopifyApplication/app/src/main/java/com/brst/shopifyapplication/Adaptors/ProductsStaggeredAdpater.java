package com.brst.shopifyapplication.Adaptors;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Fragments.ProductDetailFragment2;
import com.brst.shopifyapplication.HelperClasses.Constants;
import com.brst.shopifyapplication.HelperClasses.MySharedPreferences;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.ShopifyApplication;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.etsy.android.grid.util.DynamicHeightImageView;
import com.shopify.buy3.Storefront;

import java.util.List;


/**
 * Created by brst-pc20 on 1/9/17.
 */

public class ProductsStaggeredAdpater extends RecyclerView.Adapter<ProductsStaggeredAdpater.MyViewHolder> {

    public boolean isLoading = false;
    List<Storefront.Product> data_list;
    Context context;
    Fragment fragment;
    RecyclerView recyclerView;
    //LoadMoreItems loadMoreItemss;
    String currency;
    private int lastVisibleItem,
            totalItemCount, firstVisiblePos, firstVisibleInListview;
    private int visibleThreshold = 5;

    public ProductsStaggeredAdpater(Context context, Fragment fragment, RecyclerView recyclerView, List<Storefront.Product> data_list) {

        this.context = context;
        this.data_list = data_list;

        this.fragment = fragment;
        this.recyclerView = recyclerView;

        currency = MySharedPreferences.getInstance().getData(ShopifyApplication.getAppContext(), Constants.SELECTED_CURRENCY);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.staggered_row_layout, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        /*
        data_list.get(position).getImages()*/
        if (data_list.get(position).getImages().getEdges().size() > 0) {
            String src=data_list.get(position).getImages().getEdges().get(0).getNode().getSrc();
            holder.progress.setVisibility(View.VISIBLE);
            Glide.with(context).applyDefaultRequestOptions(new RequestOptions().placeholder(R.drawable.white_background_squaredrawable)).load(src).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    holder.progress.setVisibility(View.GONE);

                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                    holder.progress.setVisibility(View.GONE);
                    return false;
                }
            }).into(holder.productIV);












        } else {
            Glide.with(context).load(R.mipmap.placeholder_image).into(holder.productIV);
        }
        String price = String.valueOf(data_list.get(position).getVariants().getEdges().get(0).getNode().getPrice());
        String compare_price = String.valueOf(data_list.get(position).getVariants().getEdges().get(0).getNode().getCompareAtPrice());
        holder.price.setText(currency != null ? currency + price : "" + price);
        if (compare_price != null && !compare_price.equals("null")) {
            holder.comparePrice.setVisibility(View.VISIBLE);
            holder.comparePrice.setPaintFlags(holder.comparePrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            holder.comparePrice.setText(currency != null ? currency + compare_price : "" + compare_price);
        }
        holder.title.setText(data_list.get(position).getTitle());

        holder.saleTagIV.setVisibility(getSaleTag(data_list.get(position)) ? View.VISIBLE : View.GONE);

   /*     Glide.with(context).load(data_list.get(position).getProducts().getEdges().get(position).getNode().getImages().getEdges().get(0).getNode().getSrc()).into(holder.productIV);

        holder.price.setText("$" + data_list.get(position).getNode().getVariants().getEdges().get(0).getNode().getPrice());
        holder.title.setText(data_list.get(position).getProducts().getEdges().get(position).getNode().getTitle());

   */
    }

    private boolean getSaleTag(Storefront.Product data_list) {
        for (int i = 0; i < data_list.getVariants().getEdges().size(); i++) {
            String availableForSale = String.valueOf(data_list.getVariants().getEdges().get(i).getNode().getCompareAtPrice());

            if (!availableForSale.equals("null")) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int getItemCount() {
        return data_list.size();
    }

    public void addList(List<Storefront.Product> data) {
        this.data_list = data;
        fragment.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                notifyDataSetChanged();
            }
        });
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView saleTagIV;
        TextView price, title, comparePrice;
        DynamicHeightImageView productIV;
        ProgressBar progress;

        public MyViewHolder(View view) {
            super(view);


            productIV = view.findViewById(R.id.productIV);
            price = view.findViewById(R.id.price);
            comparePrice = view.findViewById(R.id.comparePrice);
            title = view.findViewById(R.id.title);
            saleTagIV = view.findViewById(R.id.saleTagIV);
            progress = view.findViewById(R.id.progress);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((HomeActivity)context).closeSearchView() ;
              /*      Bundle bundle = new Bundle();
                    bundle.putSerializable("product_list",  data_list.get(getAdapterPosition()));*/
                    ProductDetailFragment2 productFragment = new ProductDetailFragment2();
                    productFragment.setData(data_list.get(getAdapterPosition()));

                    ((HomeActivity) context).fragmentTransaction(productFragment, R.id.container);
                }
            });


        }
    }
}