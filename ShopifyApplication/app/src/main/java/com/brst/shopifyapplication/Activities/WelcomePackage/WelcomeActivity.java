package com.brst.shopifyapplication.Activities.WelcomePackage;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Activities.LoginPackage.LoginActivity;
import com.brst.shopifyapplication.Activities.ParentActivity;
import com.brst.shopifyapplication.ApiImplementation.ApiClass;
import com.brst.shopifyapplication.HelperClasses.Constants;
import com.brst.shopifyapplication.HelperClasses.MySharedPreferences;
import com.brst.shopifyapplication.Interfaces.ShopPolicyInterface;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.ShopifyApplication;
import com.brst.shopifyapplication.databinding.LayoutSplashBinding;
import com.shopify.buy3.Storefront;

import org.greenrobot.eventbus.Subscribe;


public class WelcomeActivity extends ParentActivity {

    LayoutSplashBinding activityWelcomeOneBinding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityWelcomeOneBinding = DataBindingUtil.setContentView(this, R.layout.layout_splash);
        MySharedPreferences.getInstance().clearDataListData(WelcomeActivity.this);
        initView();


    }

    @Subscribe
    public void onMessageEvent() {




        /* Do something */
    }
    private void initView() {

        String currency = MySharedPreferences.getInstance().getData(ShopifyApplication.getAppContext(), Constants.SELECTED_CURRENCY);

        if (currency != null) {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    moveToNextPage();
                }
            }, 2000);

        } else {

            new ApiClass().fetchShopPolicy(ShopifyApplication.getAppContext(), new ShopPolicyInterface() {
                @Override
                public void onResponse(Storefront.Shop shop) {
                    moveToNextPage();
                }

                @Override
                public void onError(String er) {
                    moveToNextPage();
                }

                @Override
                public void onFail(String fail) {
                    moveToNextPage();
                }
            });
        }

    }

    public void moveToNextPage() {
        String access_token = MySharedPreferences.getInstance().getData(ShopifyApplication.getAppContext(), Constants.ACCESS_TOKEN);
        if (access_token == null) {

            setOnActivityTransfer(LoginActivity.class);
            finish();

        } else {
            setOnActivityTransfer(HomeActivity.class);
            finish();
        }
    }

}
