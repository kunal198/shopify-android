package com.brst.shopifyapplication.Adaptors;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.brst.shopifyapplication.Fragments.GalleryViewFragment;
import com.brst.shopifyapplication.R;
import com.bumptech.glide.Glide;
import com.shopify.buy3.Storefront;

import java.util.List;


/**
 * Created by brst-pc20 on 1/9/17.
 */

public class GalleryViewAdapter extends RecyclerView.Adapter<GalleryViewAdapter.MyViewHolder> {

    public int selected_position = 0;
    List<Storefront.ImageEdge> data_list;
    Context context;
    Fragment fragment;
    RecyclerView recyclerView;
    boolean helper = false;
    GalleryViewFragment.PostionListener postionListener;


    public GalleryViewAdapter(Context context, Fragment fragment, RecyclerView recyclerView, List<Storefront.ImageEdge> data_list, int position, GalleryViewFragment.PostionListener postionListener) {

        this.context = context;
        this.data_list = data_list;
        selected_position = position;
        this.fragment = fragment;
        this.recyclerView = recyclerView;
        this.postionListener = postionListener;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_row_galleryview, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {


        Storefront.ImageEdge imageEdge = data_list.get(position);

        if (!helper) {
            if (imageEdge.getNode().getSrc() != null && !imageEdge.getNode().getSrc().isEmpty()) {

                Glide.with(context).load(imageEdge.getNode().getSrc()).into(holder.previewIV);
            }
        }
        if (position == selected_position) {
            holder.previewIV.setBackground(ContextCompat.getDrawable(context, R.drawable.white_edittext_blue_lining_background));
        } else {
            holder.previewIV.setBackground(ContextCompat.getDrawable(context, R.drawable.white_edittext_grey_lining_background));

        }

    }

    public void notifyData(int pos) {
        notifyItemChanged(selected_position);
        notifyItemChanged(selected_position = pos);

    }

    @Override
    public int getItemCount() {
        return data_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView previewIV;

        public MyViewHolder(View view) {
            super(view);

            previewIV = view.findViewById(R.id.previewIV);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    helper = false;
                    notifyItemChanged(selected_position);
                    notifyItemChanged(getAdapterPosition());
                    postionListener.setPosition(selected_position = getAdapterPosition());

                    //  previewIV.setBackground(ContextCompat.getDrawable(context, R.drawable.white_edittext_orange_lining_background));
                }
            });


        }
    }


}