package com.brst.shopifyapplication.ApiImplementation;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;

import com.brst.shopifyapplication.HelperClasses.Constants;
import com.brst.shopifyapplication.HelperClasses.MySharedPreferences;
import com.brst.shopifyapplication.Interfaces.CheckoutInterface;
import com.brst.shopifyapplication.Interfaces.CommonInterface;
import com.brst.shopifyapplication.Interfaces.CustomerDetailInterface;
import com.brst.shopifyapplication.Interfaces.LoginSignupCallBack;
import com.brst.shopifyapplication.Interfaces.PaymentInterface;
import com.brst.shopifyapplication.Interfaces.PaymentSettingInterface;
import com.brst.shopifyapplication.Interfaces.ProductListingInterface;
import com.brst.shopifyapplication.Interfaces.ProductsInterface;
import com.brst.shopifyapplication.Interfaces.ShopPolicyInterface;
import com.shopify.buy3.GraphCall;
import com.shopify.buy3.GraphClient;
import com.shopify.buy3.GraphError;
import com.shopify.buy3.GraphResponse;
import com.shopify.buy3.HttpCachePolicy;
import com.shopify.buy3.RetryHandler;
import com.shopify.buy3.Storefront;
import com.shopify.graphql.support.ID;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Created by brst-pc20 on 12/1/17.
 */

public class ApiClass {


    String Tag = getClass().getSimpleName();

   // private static final String SHOP_NAME = "apptuse-test.myshopify.com";
    private static final String SHOP_NAME = "getshopifyapp.myshopify.com";
    private static final String ACCESS_TOKEN = "e4cc1afbbc1f5627307e61be6f90027f";
 //   private static final String ACCESS_TOKEN = "5dd1c564526f91eea87dccca6a1a36ae";


    public GraphClient getGraphClient(Context context) {
        return GraphClient.builder(context)
                //.shopDomain("apptuse-test.myshopify.com")
                // .shopDomain("getshopifyapp.myshopify.com")
                .shopDomain(SHOP_NAME)
                //  .shopDomain("abcgreen.myshopify.com")
                .accessToken(ACCESS_TOKEN)
                //  .accessToken("720ddd6752f8c3d1de9dbdacafc93b32")
                //    .accessToken("80f2eceafa436fb60e9b4427cba70404")
                .httpClient(new OkHttpClient())
                .httpCache(new File(context.getCacheDir(), "/http"), 10 * 1024 * 1024)
                .defaultHttpCachePolicy(HttpCachePolicy.CACHE_FIRST.expireAfter(5, TimeUnit.MINUTES)) // cached response valid by default for 5 minutes
                .build();
    }

    public GraphClient getGraphClient2(Context context) {
        return GraphClient.builder(context)
                .shopDomain("shoipybrst.myshopify.com")
                //  .shopDomain("abcgreen.myshopify.com")
                .accessToken("8f553c1b1de0ff8020d11daad338415f")
                .httpClient(new OkHttpClient())
                .httpCache(new File(context.getCacheDir(), "/http"), 10 * 1024 * 1024)
                .defaultHttpCachePolicy(HttpCachePolicy.CACHE_FIRST.expireAfter(5, TimeUnit.MINUTES)) // cached response valid by default for 5 minutes

                .build();
    }


    public void fetchShopPolicy(Context context, ShopPolicyInterface shopPolicyInterface) {


        Storefront.QueryRootQuery query = Storefront.query(rootQuery -> rootQuery
                .shop(shopQuery -> shopQuery
                        .name()
                        .currencyCode()
                        .moneyFormat()

                        .refundPolicy(refundPolicyQuery -> refundPolicyQuery
                                .title()
                                .url()
                        )

                )
        );

        getGraphClient(context).queryGraph(query).enqueue(new GraphCall.Callback<Storefront.QueryRoot>() {

            @Override
            public void onResponse(@NonNull GraphResponse<Storefront.QueryRoot> response) {
                String name = response.data().getShop().getName();
                String currencyCode = response.data().getShop().getCurrencyCode().toString();
                String money = response.data().getShop().getMoneyFormat().toString().replace("{{amount}}", "");
                String refundPolicyUrl = response.data().getShop().getRefundPolicy().getUrl();
                MySharedPreferences.getInstance().storeData(context, Constants.SELECTED_CURRENCY, money);
                if (shopPolicyInterface != null) {
                    shopPolicyInterface.onResponse(response.data().getShop());

                }
            }

            @Override
            public void onFailure(@NonNull GraphError error) {
                if (shopPolicyInterface != null) {
                    shopPolicyInterface.onError(error.getLocalizedMessage());

                }

            }


        });


    }


    public void fetchCategories(Context context, ProductListingInterface productListingInterface) {

        Storefront.QueryRootQuery query = Storefront.query(rootQuery -> rootQuery
                .shop(shopQuery -> shopQuery
                        .collections(arg -> arg.first(100), collectionConnectionQuery -> collectionConnectionQuery
                                .edges(collectionEdgeQuery -> collectionEdgeQuery
                                        .node(collectionQuery -> collectionQuery
                                                .title()
                                                .image(_queryBuilder -> _queryBuilder
                                                        .src()


                                                )
                                        )


                                )


                        )


                )

        );

        getGraphClient(context).queryGraph(query).enqueue(new GraphCall.Callback<Storefront.QueryRoot>() {

            @Override
            public void onResponse(@NonNull GraphResponse<Storefront.QueryRoot> response) {
                List<Storefront.Collection> collections = new ArrayList<>();
                collections.add(null);
                for (Storefront.CollectionEdge collectionEdge : response.data().getShop().getCollections().getEdges()) {
                    collections.add(collectionEdge.getNode());

                }
                productListingInterface.onSuccess(collections);

            }

            @Override
            public void onFailure(@NonNull GraphError error) {

                productListingInterface.onFailure(error.getMessage());
            }


        });

    }

    public void fetchArticles(Context context, CommonInterface<Storefront.Article> commonInterface) {

        Storefront.QueryRootQuery query = Storefront.query(rootQuery -> rootQuery
                .shop(shopQuery -> shopQuery

                        .articles(args -> args.first(20), articleQuery -> articleQuery

                                .edges(_queryBuilder -> _queryBuilder
                                        .node(_queryBuilder1 -> _queryBuilder1.tags()
                                                .title()

                                        )

                                        .cursor()


                                )


                        )


                )


        );

        getGraphClient(context).queryGraph(query).enqueue(new GraphCall.Callback<Storefront.QueryRoot>() {

            @Override
            public void onResponse(@NonNull GraphResponse<Storefront.QueryRoot> response) {
                List<Storefront.Article> collections = new ArrayList<>();

                for (Storefront.ArticleEdge articleEdge : response.data().getShop().getArticles().getEdges()) {
                    collections.add(articleEdge.getNode());


                }

                commonInterface.onSuccess(collections);

            }

            @Override
            public void onFailure(@NonNull GraphError error) {

                commonInterface.onFailure(error.getMessage());
            }


        });

    }

    public void fetchProductTypes(Context context, CommonInterface<Storefront.Article> commonInterface) {

        Storefront.QueryRootQuery query = Storefront.query(rootQuery -> rootQuery
                .shop(shopQuery -> shopQuery

                        .productTypes(10, _queryBuilder -> _queryBuilder

                                .edges(_queryBuilder1 -> _queryBuilder1
                                        .node()
                                        .cursor()


                                )


                        )
                        .articles(args -> args.first(20), articleQuery -> articleQuery

                                .edges(_queryBuilder -> _queryBuilder
                                        .node(_queryBuilder1 -> _queryBuilder1.tags()
                                                .title()

                                        )

                                        .cursor()


                                )


                        )


                )


        );

        getGraphClient(context).queryGraph(query).enqueue(new GraphCall.Callback<Storefront.QueryRoot>() {

            @Override
            public void onResponse(@NonNull GraphResponse<Storefront.QueryRoot> response) {
                List<Storefront.Article> collections = new ArrayList<>();

                for (Storefront.ArticleEdge articleEdge : response.data().getShop().getArticles().getEdges()) {
                    collections.add(articleEdge.getNode());


                }

                commonInterface.onSuccess(collections);

            }

            @Override
            public void onFailure(@NonNull GraphError error) {

                commonInterface.onFailure(error.getMessage());
            }


        });

    }


    public void fetchCategoriesProducts(Context context, ProductListingInterface productListingInterface) {

        Storefront.QueryRootQuery query = Storefront.query(rootQuery -> rootQuery
                .shop(shopQuery -> shopQuery
                        .collections(arg -> arg.first(100), collectionConnectionQuery -> collectionConnectionQuery
                                .edges(collectionEdgeQuery -> collectionEdgeQuery
                                        .node(collectionQuery -> collectionQuery
                                                .title()
                                                .products(arg -> arg.first(10), productConnectionQuery -> productConnectionQuery
                                                        .edges(productEdgeQuery -> productEdgeQuery
                                                                .node(productQuery -> productQuery
                                                                        .title()
                                                                        .description()
                                                                        .descriptionHtml()
                                                                        .vendor()
                                                                        .productType()
                                                                        .tags()
                                                                        .handle()
                                                                        .onlineStoreUrl()
                                                                        .publishedAt()
                                                                        .updatedAt()
                                                                        .createdAt()
                                                                        .images(arg -> arg.first(10), imageConnectionQuery -> imageConnectionQuery
                                                                                .edges(imageEdgeQuery -> imageEdgeQuery
                                                                                        .node(imageQuery -> imageQuery
                                                                                                .src()
                                                                                        )
                                                                                )
                                                                        )
                                                                        .variants(arg -> arg.first(10), variantConnectionQuery -> variantConnectionQuery
                                                                                .edges(variantEdgeQuery -> variantEdgeQuery
                                                                                        .node(productVariantQuery -> productVariantQuery
                                                                                                .price()
                                                                                                .title()
                                                                                                .available()
                                                                                                .compareAtPrice()
                                                                                                .availableForSale()
                                                                                                .weight()
                                                                                                .weightUnit()
                                                                                                .sku()
                                                                                                .selectedOptions(_queryBuilder -> _queryBuilder.name().value())

                                                                                        )

                                                                                )

                                                                        )

                                                                )

                                                        )


                                                )

                                        )
                                )
                        )
                )
        );

        getGraphClient(context).queryGraph(query).enqueue(new GraphCall.Callback<Storefront.QueryRoot>() {

            @Override
            public void onResponse(@NonNull GraphResponse<Storefront.QueryRoot> response) {
                List<Storefront.Collection> collections = new ArrayList<>();
                collections.add(null);
                for (Storefront.CollectionEdge collectionEdge : response.data().getShop().getCollections().getEdges()) {
                    collections.add(collectionEdge.getNode());


                }
                productListingInterface.onSuccess(collections);

            }

            @Override
            public void onFailure(@NonNull GraphError error) {

                productListingInterface.onFailure(error.getMessage());
            }


        });

    }


    public void fetchCategoriesWithPagination(Context context, String collection_node, String productPageCursor, boolean pagination, ProductsInterface productsInterface, int position) {

        Storefront.QueryRootQuery query;


        if (pagination) {


            query = Storefront.query(root -> root.node(new ID(collection_node), node -> node
                    .onCollection(collection -> collection
                            .products(arg -> arg.sortKey(Storefront.ProductCollectionSortKeys.TITLE).first(10).after(productPageCursor), productConnectionQuery -> productConnectionQuery
                                    .pageInfo(pageInfoQuery -> pageInfoQuery
                                            .hasNextPage()
                                    )
                                    .edges(productEdgeQuery -> productEdgeQuery
                                            .node(productQuery -> productQuery
                                                    .title()

                                                    .description()
                                                    .descriptionHtml()
                                                    .vendor()
                                                    .productType()
                                                    .tags()
                                                    .handle()
                                                    .onlineStoreUrl()
                                                    .publishedAt()
                                                    .updatedAt()
                                                    .createdAt()
                                                    .options(args ->
                                                            args.name().values()
                                                                    .name()
                                                                    .values()


                                                    )

                                                    .images(arg -> arg.first(10), imageConnectionQuery -> imageConnectionQuery
                                                            .edges(imageEdgeQuery -> imageEdgeQuery
                                                                    .node(imageQuery -> imageQuery
                                                                            .src()
                                                                    )
                                                            )
                                                    )

                                                    .variants(arg -> arg.first(10), variantConnectionQuery -> variantConnectionQuery
                                                            .edges(variantEdgeQuery -> variantEdgeQuery
                                                                    .node(productVariantQuery -> productVariantQuery
                                                                            .price()
                                                                            .title()
                                                                            .available()
                                                                            .compareAtPrice()
                                                                            .availableForSale()
                                                                            .weight()
                                                                            .weightUnit()
                                                                            .sku()
                                                                            .selectedOptions(_queryBuilder -> _queryBuilder.name().value())


                                                                    )


                                                            )


                                                    )


                                            )
                                            .cursor()

                                    )
                            )
                    )
            ));


            /*query = Storefront.query(rootQuery -> rootQuery
                    .shop(shopQuery -> shopQuery
                            .collections(arg -> arg.first(100), collectionConnectionQuery -> collectionConnectionQuery
                                    .edges(collectionEdgeQuery -> collectionEdgeQuery
                                            .node(collectionQuery -> collectionQuery
                                                    .title()
                                                    .descriptionHtml()
                                                    .products(arg -> arg.sortKey(Storefront.ProductCollectionSortKeys.TITLE).first(10).after(productPageCursor), productConnectionQuery -> productConnectionQuery
                                                            .pageInfo(pageInfoQuery -> pageInfoQuery
                                                                    .hasNextPage()
                                                            )
                                                            .edges(productEdgeQuery -> productEdgeQuery
                                                                    .node(productQuery -> productQuery
                                                                            .title()

                                                                            .description()
                                                                            .descriptionHtml()
                                                                            .vendor()
                                                                            .productType()
                                                                            .tags()
                                                                            .handle()
                                                                            .onlineStoreUrl()
                                                                            .publishedAt()
                                                                            .updatedAt()
                                                                            .createdAt()


                                                                            .options(args ->
                                                                                    args.name().values()
                                                                                            .name()
                                                                                            .values()


                                                                            )

                                                                            .images(arg -> arg.first(10), imageConnectionQuery -> imageConnectionQuery
                                                                                    .edges(imageEdgeQuery -> imageEdgeQuery
                                                                                            .node(imageQuery -> imageQuery
                                                                                                    .src()
                                                                                            )
                                                                                    )
                                                                            )

                                                                            .variants(arg -> arg.first(10), variantConnectionQuery -> variantConnectionQuery
                                                                                    .edges(variantEdgeQuery -> variantEdgeQuery
                                                                                            .node(productVariantQuery -> productVariantQuery
                                                                                                    .price()
                                                                                                    .title()
                                                                                                    .available()
                                                                                                    .compareAtPrice()
                                                                                                    .availableForSale()
                                                                                                    .weight()
                                                                                                    .weightUnit()
                                                                                                    .sku()
                                                                                                    .selectedOptions(_queryBuilder -> _queryBuilder.name().value())


                                                                                            )


                                                                                    )


                                                                            )


                                                                    )
                                                                    .cursor()

                                                            )
                                                    )
                                            )
                                    )
                            )
                    )
            );
*/
        } else {

            query = Storefront.query(root -> root.node(new ID(collection_node), node -> node
                    .onCollection(collection -> collection
                            .products(arg -> arg.sortKey(Storefront.ProductCollectionSortKeys.TITLE).first(10), productConnectionQuery -> productConnectionQuery
                                    .pageInfo(Storefront.PageInfoQuery::hasNextPage
                                    )
                                    .edges(productEdgeQuery -> productEdgeQuery
                                            .node(productQuery -> productQuery
                                                    .title()
                                                    .description()
                                                    .descriptionHtml()
                                                    .vendor()
                                                    .productType()
                                                    .tags()
                                                    .handle()
                                                    .onlineStoreUrl()

                                                    .publishedAt()
                                                    .updatedAt()
                                                    .createdAt()
                                                    .options(args ->
                                                            args.name().values()
                                                                    .name()
                                                                    .values()

                                                    )
                                                    .images(arg -> arg.first(10), imageConnectionQuery -> imageConnectionQuery
                                                            .edges(imageEdgeQuery -> imageEdgeQuery
                                                                    .node(imageQuery -> imageQuery
                                                                            .src()
                                                                    )
                                                            )
                                                    )

                                                    .variants(arg -> arg.first(10), variantConnectionQuery -> variantConnectionQuery
                                                            .edges(variantEdgeQuery -> variantEdgeQuery
                                                                    .node(productVariantQuery -> productVariantQuery
                                                                            .price()
                                                                            .title()
                                                                            .available()
                                                                            .compareAtPrice()
                                                                            .availableForSale()
                                                                            .weight()
                                                                            .weightUnit()
                                                                            .sku()
                                                                            .selectedOptions(_queryBuilder -> _queryBuilder.name().value())
                                                                            .image(_queryBuilder -> _queryBuilder.src())
                                                                    )
                                                            )
                                                    )

                                            )
                                            .cursor()


                                    )

                            )

                    )
            ));










          /*  query = Storefront.query(rootQuery -> rootQuery
                    .shop(shopQuery -> shopQuery
                            .collections(arg -> arg.first(100), collectionConnectionQuery -> collectionConnectionQuery
                                    .edges(collectionEdgeQuery -> collectionEdgeQuery
                                            .node(collectionQuery -> collectionQuery
                                                    .title()
                                                    .descriptionHtml()
                                                    .products(arg -> arg.sortKey(Storefront.ProductCollectionSortKeys.TITLE).first(10), productConnectionQuery -> productConnectionQuery
                                                            .pageInfo(Storefront.PageInfoQuery::hasNextPage
                                                            )
                                                            .edges(productEdgeQuery -> productEdgeQuery
                                                                    .node(productQuery -> productQuery
                                                                            .title()
                                                                            .description()
                                                                            .descriptionHtml()
                                                                            .vendor()
                                                                            .productType()
                                                                            .tags()
                                                                            .handle()
                                                                            .onlineStoreUrl()

                                                                            .publishedAt()
                                                                            .updatedAt()
                                                                            .createdAt()
                                                                            .options(args ->
                                                                                    args.name().values()
                                                                                            .name()
                                                                                            .values()

                                                                            )
                                                                            .images(arg -> arg.first(10), imageConnectionQuery -> imageConnectionQuery
                                                                                    .edges(imageEdgeQuery -> imageEdgeQuery
                                                                                            .node(imageQuery -> imageQuery
                                                                                                    .src()
                                                                                            )
                                                                                    )
                                                                            )

                                                                            .variants(arg -> arg.first(10), variantConnectionQuery -> variantConnectionQuery
                                                                                    .edges(variantEdgeQuery -> variantEdgeQuery
                                                                                            .node(productVariantQuery -> productVariantQuery
                                                                                                    .price()
                                                                                                    .title()
                                                                                                    .available()
                                                                                                    .compareAtPrice()
                                                                                                    .availableForSale()
                                                                                                    .weight()
                                                                                                    .weightUnit()
                                                                                                    .sku()
                                                                                                    .selectedOptions(_queryBuilder -> _queryBuilder.name().value())
                                                                                                    .image(_queryBuilder -> _queryBuilder.src())
                                                                                            )
                                                                                    )
                                                                            )

                                                                    )
                                                                    .cursor()


                                                            )

                                                    )
                                            )
                                    )
                            )
                    )
            );*/
        }


        getGraphClient(context).queryGraph(query).enqueue(new GraphCall.Callback<Storefront.QueryRoot>() {

            @Override
            public void onResponse(@NonNull GraphResponse<Storefront.QueryRoot> response) {

                final Storefront.Collection collection = (Storefront.Collection) response.data().getNode();
                // this.callback.onResponse(Converter.convertProducts(collection.getProducts()));

                /*for (int k = 0; k < collection.getProducts().getEdges().size(); k++) {
                    String aaa = collection.getProducts().getEdges().get(k).getNode().getTitle();
                    String aaass = collection.getProducts().getEdges().get(k).getNode().getDescription();


                }*/
                List<Storefront.Product> products = new ArrayList<>();

                boolean hasNextProductPage = false;
                String cursor = "";
                int edge_size = collection.getProducts().getEdges().size() - 1;

                for (Storefront.ProductEdge productEdge : collection.getProducts().getEdges()) {
                    products.add(productEdge.getNode());
                    hasNextProductPage = collection.getProducts().getPageInfo().getHasNextPage();
                    cursor = collection.getProducts().getEdges().get(edge_size).getCursor();
                }


                /*int size = 1;
                boolean hasNextProductPage = false;
                String cursor = "";
                List<Storefront.Collection> collections = new ArrayList<>();
                List<Storefront.Product> products = null;
                for (Storefront.CollectionEdge collectionEdge : response.data().getShop().getCollections().getEdges()) {
                    Log.d("collection", "collection" + position);
                    collections.add(collectionEdge.getNode());
                    products = new ArrayList<>();
                    int edge_size = collectionEdge.getNode().getProducts().getEdges().size() - 1;
                    for (Storefront.ProductEdge productEdge : collectionEdge.getNode().getProducts().getEdges()) {
                        products.add(productEdge.getNode());
                        hasNextProductPage = collectionEdge.getNode().getProducts().getPageInfo().getHasNextPage();
                        cursor = collectionEdge.getNode().getProducts().getEdges().get(edge_size).getCursor();
                    }
                    if (position == size) {
                        break;
                    }
                    size++;


                }*/

                productsInterface.onSuccess(products, hasNextProductPage, pagination, cursor,"");


            }

            @Override
            public void onFailure(@NonNull GraphError error) {

                productsInterface.onError(error.getMessage());
            }


        });


    }


    /*COde for Search*/


/*
    if (swipeRefreshLayout != null)
            swipeRefreshLayout.setRefreshing(true);
    ShopifyApiInterface shopifyApiInterface = ApiClient.getClient().create(ShopifyApiInterface.class);

    Call<Storefront.QueryRoot> call = shopifyApiInterface.searchProducts(search_query, token);
        call.enqueue(new Callback<Storefront.QueryRoot>() {

        @Override
        public void onResponse(Call<Storefront.QueryRoot> call, Response<Storefront.QueryRoot> response) {




        }

        @Override
        public void onFailure(Call<Storefront.QueryRoot> call, Throwable t) {

            String error = t.getMessage();
            Log.d("Error", t.getMessage());
        }
    });
*/


    /*
        public void fetchCategoriesWithPaginationOnSearch(Context context, Activity activity, String collection_node, String productPageCursor, boolean pagination, ProductsInterface productsInterface, int position, String search_query, SwipeRefreshLayout swipeRefreshLayout) {
            Storefront.QueryRootQuery queryy;
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (swipeRefreshLayout != null)
                        swipeRefreshLayout.setRefreshing(true);
                }
            });

            String title = "title:" + search_query;
            if (pagination) {


                queryy = Storefront.query(root -> root
                        .shop(shop -> shop
                                .collections(
                                        arg -> arg
                                                .first(100),
                                        connection -> connection
                                                .edges(edges -> edges
                                                        .node(node -> node
                                                                .title()
                                                                .description()
                                                                .descriptionHtml()
                                                                .products(arg -> arg.sortKey(Storefront.ProductCollectionSortKeys.TITLE).first(10).after(productPageCursor), productConnectionQuery -> productConnectionQuery
                                                                        .pageInfo(pageInfoQuery -> pageInfoQuery
                                                                                .hasNextPage()
                                                                        )

                                                                        .edges(productEdgeQuery -> productEdgeQuery
                                                                                .node(productQuery -> productQuery
                                                                                        .title()
                                                                                        .descriptionHtml()
                                                                                        .description()
                                                                                        .vendor()
                                                                                        .productType()
                                                                                        .tags()
                                                                                        .handle()
                                                                                        .onlineStoreUrl()

                                                                                        .publishedAt()
                                                                                        .updatedAt()
                                                                                        .createdAt()
                                                                                        .options(args ->
                                                                                                args.name().values()
                                                                                                        .name()
                                                                                                        .values()

                                                                                        )
                                                                                        .images(arg -> arg.first(10), imageConnectionQuery -> imageConnectionQuery
                                                                                                .edges(imageEdgeQuery -> imageEdgeQuery
                                                                                                        .node(imageQuery -> imageQuery
                                                                                                                .src()
                                                                                                        )
                                                                                                )
                                                                                        )
                                                                                        .variants(arg -> arg.first(10), variantConnectionQuery -> variantConnectionQuery
                                                                                                .edges(variantEdgeQuery -> variantEdgeQuery
                                                                                                        .node(productVariantQuery -> productVariantQuery
                                                                                                                .price()
                                                                                                                .title()
                                                                                                                .available()
                                                                                                                .compareAtPrice()
                                                                                                                .availableForSale()
                                                                                                                .weight()
                                                                                                                .weightUnit()
                                                                                                                .sku()
                                                                                                                .selectedOptions(_queryBuilder -> _queryBuilder.name().value())

                                                                                                        )
                                                                                                )
                                                                                        )

                                                                                )
                                                                                .cursor()

                                                                        )
                                                                )
                                                        )
                                                )
                                )
                        )
                );

            } else {
                queryy = Storefront.query(root -> root
                        .shop(shop -> shop
                                .collections(
                                        arg -> arg
                                                .first(100)
                                                .query(search_query),
                                        connection -> connection
                                                .edges(edges -> edges
                                                        .node(node -> node
                                                                .title()
                                                                .description()
                                                                .descriptionHtml()


                                                                .products(arg -> arg.sortKey(Storefront.ProductCollectionSortKeys.TITLE).first(10), productConnectionQuery -> productConnectionQuery
                                                                        .pageInfo(pageInfoQuery -> pageInfoQuery
                                                                                .hasNextPage()
                                                                        )
                                                                        .edges(productEdgeQuery -> productEdgeQuery
                                                                                .node(productQuery -> productQuery
                                                                                        .title()

                                                                                        .description()
                                                                                        .descriptionHtml()

                                                                                        .vendor()
                                                                                        .productType()
                                                                                        .tags()
                                                                                        .handle()
                                                                                        .onlineStoreUrl()
                                                                                        .publishedAt()
                                                                                        .updatedAt()
                                                                                        .createdAt()
                                                                                        .options(args ->
                                                                                                args.name().values()
                                                                                                        .name()
                                                                                                        .values()

                                                                                        )
                                                                                        .images(arg -> arg.first(10), imageConnectionQuery -> imageConnectionQuery
                                                                                                .edges(imageEdgeQuery -> imageEdgeQuery
                                                                                                        .node(imageQuery -> imageQuery
                                                                                                                .src()
                                                                                                        )
                                                                                                )
                                                                                        )
                                                                                        .variants(arg -> arg.first(10), variantConnectionQuery -> variantConnectionQuery
                                                                                                .edges(variantEdgeQuery -> variantEdgeQuery
                                                                                                        .node(productVariantQuery -> productVariantQuery
                                                                                                                .price()
                                                                                                                .compareAtPrice()
                                                                                                                .availableForSale()
                                                                                                                .weight()
                                                                                                                .weightUnit()
                                                                                                                .sku()
                                                                                                                .title()
                                                                                                                .available()
                                                                                                                .selectedOptions(_queryBuilder -> _queryBuilder.name().value())

                                                                                                        )
                                                                                                )
                                                                                        )

                                                                                )
                                                                                .cursor()

                                                                        )
                                                                )
                                                        )
                                                )
                                )
                        )
                );
            }


            getGraphClient(context).queryGraph(queryy).enqueue(new GraphCall.Callback<Storefront.QueryRoot>() {

                @Override
                public void onResponse(@NonNull GraphResponse<Storefront.QueryRoot> response) {
                    int size = 1;
                    boolean hasNextProductPage = false;
                    String cursor = "";
                    List<Storefront.Collection> collections = new ArrayList<>();
                    List<Storefront.Product> products = null;
                    //collections.add(null);
                    for (Storefront.CollectionEdge collectionEdge : response.data().getShop().getCollections().getEdges()) {


                        Log.d("collection", "collection" + position);
                        collections.add(collectionEdge.getNode());
                        products = new ArrayList<>();
                        int edge_size = collectionEdge.getNode().getProducts().getEdges().size() - 1;
                        for (Storefront.ProductEdge productEdge : collectionEdge.getNode().getProducts().getEdges()) {
                            products.add(productEdge.getNode());
                            hasNextProductPage = collectionEdge.getNode().getProducts().getPageInfo().getHasNextPage();
                            cursor = collectionEdge.getNode().getProducts().getEdges().get(edge_size).getCursor();
                        }
                        if (position == size) {
                            break;
                        }
                        size++;


                    }
                    if (collections.size() > 0) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (swipeRefreshLayout != null)
                                    swipeRefreshLayout.setRefreshing(false);
                            }
                        });
                        productsInterface.onSuccess(products, hasNextProductPage, pagination, cursor);
                    } else {
                        productsInterface.onError("No match Found");
                    }

                    */
/*else {
                    // productsInterface.onError("No Match Found");
                    String search_text = "tag:" + search_query;
                    fetchCategoriesWithPaginationOnSearchForProduct(context, activity, "", "", false, productsInterface, 0, search_text, swipeRefreshLayout);
                }*//*

            }

            @Override
            public void onFailure(@NonNull GraphError error) {

                productsInterface.onError(error.getMessage());
            }


        });


    }
*/
    public void fetchCategoriesWithPaginationOnSearch(Context context, Activity activity, String collection_node, String productPageCursor, boolean pagination, ProductsInterface productsInterface, int position, String search_query, SwipeRefreshLayout swipeRefreshLayout) {
      /*.sortKey(Storefront.ProductSortKeys.TITLE)*/
        Storefront.QueryRootQuery queryy;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(true);
            }
        });

        if (pagination) {

            queryy = Storefront.query(root -> root
                    .shop(shop -> shop
                            .products(arg -> arg.sortKey(Storefront.ProductSortKeys.TITLE).first(10).query(search_query), productConnectionQuery -> productConnectionQuery
                                    .pageInfo(pageInfoQuery -> pageInfoQuery
                                            .hasNextPage()
                                    )
                                    .edges(productEdgeQuery -> productEdgeQuery
                                            .node(productQuery -> productQuery
                                                    .title()
                                                    .description()
                                                    .descriptionHtml()
                                                    .vendor()
                                                    .productType()
                                                    .tags()
                                                    .handle()
                                                    .onlineStoreUrl()
                                                    .publishedAt()
                                                    .updatedAt()
                                                    .createdAt()
                                                    .options(args ->
                                                            args.name().values()
                                                                    .name()
                                                                    .values()

                                                    )
                                                    .images(arg -> arg.first(10), imageConnectionQuery -> imageConnectionQuery
                                                            .edges(imageEdgeQuery -> imageEdgeQuery
                                                                    .node(imageQuery -> imageQuery
                                                                            .src()
                                                                    )
                                                            )
                                                    )
                                                    .variants(arg -> arg.first(10), variantConnectionQuery -> variantConnectionQuery
                                                            .edges(variantEdgeQuery -> variantEdgeQuery
                                                                    .node(productVariantQuery -> productVariantQuery
                                                                            .price()
                                                                            .title()
                                                                            .available()
                                                                            .compareAtPrice()
                                                                            .availableForSale()
                                                                            .weight()
                                                                            .weightUnit()
                                                                            .sku()
                                                                            .selectedOptions(_queryBuilder -> _queryBuilder.name().value())

                                                                    )
                                                            )
                                                    )

                                            )
                                            .cursor()

                                    )
                            )
                    )
            );

        } else {

            queryy = Storefront.query(root -> root
                    .shop(shop -> shop
                            .products(arg -> arg.sortKey(Storefront.ProductSortKeys.TITLE).first(20).query(search_query), productConnectionQuery -> productConnectionQuery
                                    .pageInfo(pageInfoQuery -> pageInfoQuery
                                            .hasNextPage()
                                    )
                                    .edges(productEdgeQuery -> productEdgeQuery
                                            .node(productQuery -> productQuery
                                                    .title()
                                                    .description()
                                                    .descriptionHtml()
                                                    .vendor()
                                                    .productType()
                                                    .tags()
                                                    .handle()
                                                    .onlineStoreUrl()
                                                    .publishedAt()
                                                    .updatedAt()
                                                    .createdAt()
                                                    .options(args ->
                                                            args.name().values()
                                                                    .name()
                                                                    .values()

                                                    )
                                                    .images(arg -> arg.first(10), imageConnectionQuery -> imageConnectionQuery
                                                            .edges(imageEdgeQuery -> imageEdgeQuery
                                                                    .node(imageQuery -> imageQuery
                                                                            .src()
                                                                    )
                                                            )
                                                    )
                                                    .variants(arg -> arg.first(10), variantConnectionQuery -> variantConnectionQuery
                                                            .edges(variantEdgeQuery -> variantEdgeQuery
                                                                    .node(productVariantQuery -> productVariantQuery
                                                                            .price()
                                                                            .title()
                                                                            .available()
                                                                            .compareAtPrice()
                                                                            .availableForSale()
                                                                            .weight()
                                                                            .weightUnit()
                                                                            .sku()
                                                                            .selectedOptions(_queryBuilder -> _queryBuilder.name().value())


                                                                    )
                                                            )


                                                    )


                                            )
                                            .cursor()

                                    )
                            )
                    )
            );
        }


        getGraphClient(context).queryGraph(queryy).enqueue(new GraphCall.Callback<Storefront.QueryRoot>() {

            @Override
            public void onResponse(@NonNull GraphResponse<Storefront.QueryRoot> response) {

                boolean hasNextProductPage = false;
                String cursor = "";

                List<Storefront.Product> products = new ArrayList<>();
                //collections.add(null);

                for (Storefront.ProductEdge productEdge : response.data().getShop().getProducts().getEdges()) {


                    Log.d("collection", "collection" + position);
                    String s = productEdge.getNode().getTitle();
                    products.add(productEdge.getNode());


                }
                if (products.size() > 0) {
                    productsInterface.onSuccess(products, hasNextProductPage, pagination, cursor,search_query);
                } else {
                    productsInterface.onError("No Match Found");
                }

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (swipeRefreshLayout != null)
                            swipeRefreshLayout.setRefreshing(false);
                    }
                });

            }

            @Override
            public void onFailure(@NonNull GraphError error) {

                productsInterface.onError(error.getMessage());
            }


        });


    }


    public void signupProcess(Context context, String name, String lastName, String phoneNo, String email, String password, LoginSignupCallBack loginSignupCallBack) {
        Storefront.CustomerCreateInput input = new Storefront.CustomerCreateInput(email, password)
                .setFirstName(name)
                .setLastName(lastName)
                .setAcceptsMarketing(true)
                .setPhone(phoneNo);

        Storefront.MutationQuery mutationQuery = Storefront.mutation(rootQuery -> rootQuery
                .customerCreate(input, createCustomer -> createCustomer
                        .customer(customerQuery -> customerQuery
                                .id()
                                .email()
                                .firstName()
                                .lastName()
                        )
                        .userErrors(userError -> userError
                                .field()
                                .message()


                        )
                )
        );
/*
        Storefront.Customer customer = response.data().getCustomerCreate().getCustomer();*/


        getGraphClient(context).mutateGraph(mutationQuery).enqueue(new GraphCall.Callback<Storefront.Mutation>() {
            @Override
            public void onResponse(@NonNull GraphResponse<Storefront.Mutation> response) {


                //Log.d(Tag,"getCustomerCreate"+response.data().getCustomerCreate());
                //  Log.d(Tag,"getDisplayName"+response.data().getCustomerCreate().getCustomer().getDisplayName());

                if (response.data().getCustomerCreate() != null) {


                    if (response.data().getCustomerCreate().getUserErrors() != null) {

                        if (response.data().getCustomerCreate().getUserErrors().size() > 0) {


                            for (Storefront.UserError error : response.data().getCustomerCreate().getUserErrors()) {

                                Log.d(Tag, "error" + error.getMessage());
                                loginSignupCallBack.onError(error.getMessage());
                            }
                        } else {
                            loginSignupCallBack.onSuccess("");

                        }
                    }
                } else {


                    if (response.data().getCustomerCreate() != null) {
                        Storefront.Customer customer = response.data().getCustomerCreate().getCustomer();

                        if (customer != null) {
                            loginSignupCallBack.onSuccess("");

                            //    Log.d(Tag, customer.getId().toString());
                            //Log.d(Tag,"getEmail"+response.data().getCustomerCreate().getUserErrors());


                        } else {
                            loginSignupCallBack.onError("Please enter valid email and phone no");

                        }
                    } else {
                        loginSignupCallBack.onError("Please enter valid email and phone no");


                    }
                }
            }

            @Override
            public void onFailure(@NonNull GraphError error) {

            }
        });


    }

    public void loginProcess(Context context, String email, String password, LoginSignupCallBack loginSignupCallBack) {
        Storefront.CustomerAccessTokenCreateInput input = new Storefront.CustomerAccessTokenCreateInput(email, password);
        Storefront.MutationQuery mutationQuery = Storefront.mutation(mutation -> mutation
                .customerAccessTokenCreate(input, query -> query
                        .customerAccessToken(customerAccessToken -> customerAccessToken
                                .accessToken()
                                .expiresAt()

                        )
                        .userErrors(userError -> userError
                                .field()
                                .message()

                        )
                )
        );


        getGraphClient(context).mutateGraph(mutationQuery).enqueue(new GraphCall.Callback<Storefront.Mutation>() {
            @Override
            public void onResponse(@NonNull GraphResponse<Storefront.Mutation> response) {


                if (response.data().getCustomerAccessTokenCreate().getCustomerAccessToken() != null) {
                    if (response.data().getCustomerAccessTokenCreate().getCustomerAccessToken().getAccessToken() != null) {
                        loginSignupCallBack.onSuccess(response.data().getCustomerAccessTokenCreate().getCustomerAccessToken().getAccessToken());
                    } else {
                        loginSignupCallBack.onError("error");


                    }


                } else {
                    loginSignupCallBack.onError("error");

                }


            }

            @Override
            public void onFailure(@NonNull GraphError error) {

            }
        });


    }


    public void purchaseItem(Context context, Storefront.CheckoutCreateInput input, CheckoutInterface checkoutInterface) {


        Storefront.MutationQuery query = Storefront.mutation(mutationQuery -> mutationQuery
                .checkoutCreate(input, createPayloadQuery -> createPayloadQuery


                        .checkout(checkoutQuery -> checkoutQuery
                                .webUrl()

                        )
                        .userErrors(userErrorQuery -> userErrorQuery
                                .field()
                                .message()
                        )
                )


        );

        getGraphClient(context).mutateGraph(query).enqueue(new GraphCall.Callback<Storefront.Mutation>() {
            @Override
            public void onResponse(@NonNull GraphResponse<Storefront.Mutation> response) {
                if (!response.data().getCheckoutCreate().getUserErrors().isEmpty()) {
                    // handle user friendly errors

                    for (Storefront.UserError error : response.data().getCheckoutCreate().getUserErrors()) {

                        Log.d(Tag, "error" + error.getMessage());
                        checkoutInterface.onError(error.getMessage());
                    }

                } else {
                    checkoutInterface.onSucessfullCheckout(response.data().getCheckoutCreate().getCheckout());

                }
            }

            // Z2lkOi8vc2hvcGlmeS9DaGVja291dC82ZDBkMDljMWRkOTYxZjc1MjllMDZkMjYwZTUzOTQxYz9rZXk9M2NiMzNjYmJhYmFhZTIzYzdhNTU4MzVhY2I3ZDMzMzQ=
            //   https://apptuse-test.myshopify.com/25738608/checkouts/6d0d09c1dd961f7529e06d260e53941c?key=3cb33cbbabaae23c7a55835acb7d3334
            @Override
            public void onFailure(@NonNull GraphError error) {
                checkoutInterface.onError(error.getMessage());
                // handle errors
            }
        });


    }

    public void updatingToken(Context context, ID id, String token, CheckoutInterface checkoutInterface) {

        Storefront.MutationQuery query = Storefront.mutation(mutationQuery -> mutationQuery
                .checkoutCustomerAssociate(id, token, customerAssociatePayloadQuery -> customerAssociatePayloadQuery
                        .checkout(checkoutQuery -> checkoutQuery
                                .webUrl()
                        )
                        .userErrors(userErrorQuery -> userErrorQuery
                                .field()
                                .message()
                        )
                )


        );


        getGraphClient(context).mutateGraph(query).enqueue(new GraphCall.Callback<Storefront.Mutation>() {
            @Override
            public void onResponse(@NonNull GraphResponse<Storefront.Mutation> response) {


                if (!response.data().getCheckoutCustomerAssociate().getUserErrors().isEmpty()) {
                    // handle user friendly errors

                    for (Storefront.UserError error : response.data().getCheckoutCustomerAssociate().getUserErrors()) {

                        Log.d(Tag, "error" + error.getMessage());
                        checkoutInterface.onError(error.getMessage());
                    }

                } else {
                    checkoutInterface.onSucessfullCheckout(response.data().getCheckoutCustomerAssociate().getCheckout());


                }
            }

            // Z2lkOi8vc2hvcGlmeS9DaGVja291dC82ZDBkMDljMWRkOTYxZjc1MjllMDZkMjYwZTUzOTQxYz9rZXk9M2NiMzNjYmJhYmFhZTIzYzdhNTU4MzVhY2I3ZDMzMzQ=
            //   https://apptuse-test.myshopify.com/25738608/checkouts/6d0d09c1dd961f7529e06d260e53941c?key=3cb33cbbabaae23c7a55835acb7d3334
            @Override
            public void onFailure(@NonNull GraphError error) {
                checkoutInterface.onError(error.getMessage());
                // handle errors
            }
        });


    }

    public void updatingAddress(Context context, ID id, Storefront.MailingAddressInput input, CheckoutInterface checkoutInterface) {

        Storefront.MutationQuery query = Storefront.mutation((mutationQuery -> mutationQuery
                        .checkoutShippingAddressUpdate(input, id, shippingAddressUpdatePayloadQuery -> shippingAddressUpdatePayloadQuery
                                .checkout(checkoutQuery -> checkoutQuery
                                        .webUrl()
                                        .totalPrice()
                                        .taxesIncluded()
                                        .taxExempt()
                                        .totalTax()
                                        .subtotalPrice()

                                )
                                .userErrors(userErrorQuery -> userErrorQuery
                                        .field()
                                        .message()
                                )

                        )


                /*
                .checkoutGiftCardApply()*/

                )
        );


        getGraphClient(context).mutateGraph(query).enqueue(new GraphCall.Callback<Storefront.Mutation>() {
            @Override
            public void onResponse(@NonNull GraphResponse<Storefront.Mutation> response) {


                if (!response.data().getCheckoutShippingAddressUpdate().getUserErrors().isEmpty()) {
                    // handle user friendly errors

                    for (Storefront.UserError error : response.data().getCheckoutShippingAddressUpdate().getUserErrors()) {

                        Log.d(Tag, "error" + error.getMessage());
                        checkoutInterface.onError(error.getMessage());
                    }

                } else {
                    checkoutInterface.onSucessfullCheckout(response.data().getCheckoutShippingAddressUpdate().getCheckout());


                }
            }

            // Z2lkOi8vc2hvcGlmeS9DaGVja291dC82ZDBkMDljMWRkOTYxZjc1MjllMDZkMjYwZTUzOTQxYz9rZXk9M2NiMzNjYmJhYmFhZTIzYzdhNTU4MzVhY2I3ZDMzMzQ=
            //   https://apptuse-test.myshopify.com/25738608/checkouts/6d0d09c1dd961f7529e06d260e53941c?key=3cb33cbbabaae23c7a55835acb7d3334
            @Override
            public void onFailure(@NonNull GraphError error) {
                checkoutInterface.onError(error.getMessage());
                // handle errors
            }
        });


    }

    public void completeOrder(Context context, ID checkoutId, CheckoutInterface checkoutInterface) {
        Storefront.QueryRootQuery query = Storefront.query(rootQuery -> rootQuery
                .node(checkoutId, nodeQuery -> nodeQuery
                        .onCheckout(checkoutQuery -> checkoutQuery


                                .order(orderQuery -> orderQuery


                                        .orderNumber()
                                        .totalPrice()

                                )
                        )
                )
        );

        getGraphClient(context).queryGraph(query).enqueue(
                new GraphCall.Callback<Storefront.QueryRoot>() {
                    @Override
                    public void onResponse(@NonNull GraphResponse<Storefront.QueryRoot> response) {
                        Storefront.Checkout checkout = (Storefront.Checkout) response.data().getNode();

                        //  String orderId = checkout.getOrder().getId().toString();
                        checkoutInterface.onSucessfullCheckout(checkout);
                    }

                    @Override
                    public void onFailure(@NonNull GraphError error) {
                        checkoutInterface.onError(error.getMessage());
                    }
                },
                null,
                RetryHandler.exponentialBackoff(1, TimeUnit.MILLISECONDS, 1.2f)

                        .maxCount(4)
                        .build()
        );
    }


    public void applyGiftCard(Context context, ID id, String coupan, CheckoutInterface checkoutInterface) {

        Storefront.MutationQuery query = Storefront.mutation((mutationQuery -> mutationQuery

                        .checkoutDiscountCodeApply(coupan, id, _queryBuilder -> _queryBuilder



                                .checkout(checkoutQuery -> checkoutQuery


                                                .paymentDue()
                                                .totalPrice()
                                                .subtotalPrice()
                                                .totalTax()



                                        /*.appliedGiftCards(_queryBuilder1 -> _queryBuilder1

                                                .amountUsed()
                                                .balance()
                                                .lastCharacters()
                                        )*/


                                )


                        )


                )
        );


        getGraphClient(context).mutateGraph(query).enqueue(new GraphCall.Callback<Storefront.Mutation>() {
            @Override
            public void onResponse(@NonNull GraphResponse<Storefront.Mutation> response) {
                checkoutInterface.onSucessfullCheckout(response.data().getCheckoutDiscountCodeApply().getCheckout());


               /* if (!response.data().getCheckoutDiscountCodeApply().getUserErrors().isEmpty()) {
                    // handle user friendly errors

                    for (Storefront.UserError error : response.data().getCheckoutDiscountCodeApply().getUserErrors()) {

                        Log.d(Tag, "error" + error.getMessage());
                        checkoutInterface.onError(error.getMessage());
                    }

                } else {
                    checkoutInterface.onSucessfullCheckout(response.data().getCheckoutDiscountCodeApply().getCheckout());


                }*/
            }

            // Z2lkOi8vc2hvcGlmeS9DaGVja291dC82ZDBkMDljMWRkOTYxZjc1MjllMDZkMjYwZTUzOTQxYz9rZXk9M2NiMzNjYmJhYmFhZTIzYzdhNTU4MzVhY2I3ZDMzMzQ=
            //   https://apptuse-test.myshopify.com/25738608/checkouts/6d0d09c1dd961f7529e06d260e53941c?key=3cb33cbbabaae23c7a55835acb7d3334
            @Override
            public void onFailure(@NonNull GraphError error) {
                checkoutInterface.onError(error.getMessage());
                // handle errors
            }
        });


    }


    public void shippingLineUpdate(Context context, ID id, String shippinghandle, CheckoutInterface checkoutInterface) {

        Storefront.MutationQuery query = Storefront.mutation((mutationQuery -> mutationQuery

                .checkoutShippingLineUpdate(id, shippinghandle, checkoutShippingLineUpdate -> checkoutShippingLineUpdate

                        .checkout(checkoutQuery -> checkoutQuery
                                .paymentDue()
                                .totalPrice()
                                .subtotalPrice()
                                .webUrl()

                                .order(orderQuery -> orderQuery


                                        .orderNumber()
                                        .totalPrice()

                                )

                        )


                )


        ));


        getGraphClient(context).mutateGraph(query).enqueue(new GraphCall.Callback<Storefront.Mutation>() {
            @Override
            public void onResponse(@NonNull GraphResponse<Storefront.Mutation> response) {
                checkoutInterface.onSucessfullCheckout(response.data().getCheckoutShippingLineUpdate().getCheckout());


                /*if (!response.data().getCheckoutShippingLineUpdate().getUserErrors().isEmpty()) {
                    // handle user friendly errors

                    for (Storefront.UserError error : response.data().getCheckoutShippingLineUpdate().getUserErrors()) {

                        Log.d(Tag, "error" + error.getMessage());
                        checkoutInterface.onError(error.getMessage());
                    }

                } else {
                    checkoutInterface.onSucessfullCheckout(response.data().getCheckoutShippingLineUpdate().getCheckout());


                }*/
            }

            // Z2lkOi8vc2hvcGlmeS9DaGVja291dC82ZDBkMDljMWRkOTYxZjc1MjllMDZkMjYwZTUzOTQxYz9rZXk9M2NiMzNjYmJhYmFhZTIzYzdhNTU4MzVhY2I3ZDMzMzQ=
            //   https://apptuse-test.myshopify.com/25738608/checkouts/6d0d09c1dd961f7529e06d260e53941c?key=3cb33cbbabaae23c7a55835acb7d3334
            @Override
            public void onFailure(@NonNull GraphError error) {
                checkoutInterface.onError(error.getMessage());
                // handle errors
            }
        });


    }

    public void paymentSettings(Context context, PaymentSettingInterface paymentSettings) {
        Storefront.QueryRootQuery query = Storefront.query(rootQuery -> rootQuery
                .shop(_queryBuilder -> _queryBuilder
                        .paymentSettings(_queryBuilder1 -> _queryBuilder1

                                .acceptedCardBrands()
                                .cardVaultUrl()
                                .shopifyPaymentsAccountId()

                                .supportedDigitalWallets()


                        )

                )


        );


        getGraphClient(context).queryGraph(query).enqueue(
                new GraphCall.Callback<Storefront.QueryRoot>() {
                    @Override
                    public void onResponse(@NonNull final GraphResponse<Storefront.QueryRoot> response) {
                        Storefront.PaymentSettings settings = response.data().getShop().getPaymentSettings();

                        paymentSettings.onPayemntSettings(settings);
                        // String card_vault_url=settings.getCardVaultUrl();
                        //   List getAcceptedCardBrands=settings.getAcceptedCardBrands();
                        //  String getShopifyPaymentsAccountId=settings.getShopifyPaymentsAccountId();
                        //   List getSupportedDigitalWallets=settings.getSupportedDigitalWallets();


                        //  checkoutInterface.onSucessfullCheckout(settings);
                        // List<Storefront.ShippingRate> shippingRates = checkout.getAvailableShippingRates().getShippingRates();
                    }

                    @Override
                    public void onFailure(@NonNull final GraphError error) {
                        paymentSettings.onFailure(error.getMessage());
                    }
                });


    }


    public void payWithCreditCard(Context context, ID checkoutId, BigDecimal amount, String creditCardVaultToken, Storefront.MailingAddressInput mailingAddressInput, PaymentInterface paymentInterface) {

        //  GraphClient client = ...;
        //  ID checkoutId = ...;
        // BigDecimal amount = ...;
        String idempotencyKey = UUID.randomUUID().toString();
        //    Storefront.MailingAddressInput billingAddress = ...;
        //   String creditCardVaultToken = ...;

        Storefront.CreditCardPaymentInput input = new Storefront.CreditCardPaymentInput(amount, idempotencyKey, mailingAddressInput,
                creditCardVaultToken);

        Storefront.MutationQuery query = Storefront.mutation(mutationQuery -> mutationQuery
                .checkoutCompleteWithCreditCard(checkoutId, input, payloadQuery -> payloadQuery
                        .payment(paymentQuery -> paymentQuery
                                .test()
                                .ready()
                                .errorMessage()
                                .amount()


                        )

                        .checkout(checkoutQuery -> checkoutQuery
                                .ready()


                        )
                        .userErrors(userErrorQuery -> userErrorQuery
                                .field()
                                .message()

                        )
                )

        );

        getGraphClient(context).mutateGraph(query).enqueue(new GraphCall.Callback<Storefront.Mutation>() {
            @Override
            public void onResponse(@NonNull final GraphResponse<Storefront.Mutation> response) {
                // boolean checkoutReady1 = response.data().getCheckoutCompleteWithCreditCard().getCheckout().getReady();
                // String errorMessage = response.data().getCheckoutCompleteWithCreditCard().getPayment().getErrorMessage();
                //  String paymentReady122 = String.valueOf(response.data().getCheckoutCompleteWithCreditCard().getPayment().getAmount());
                //   boolean paymentReady12 = response.data().getCheckoutCompleteWithCreditCard().getPayment().getReady();


                if (!response.data().getCheckoutCompleteWithCreditCard().getUserErrors().isEmpty()) {
                    for (Storefront.UserError error : response.data().getCheckoutCompleteWithCreditCard().getUserErrors()) {

                        Log.d(Tag, "error" + error.getMessage());
                        String errorw = String.valueOf(error.getMessage());
                        String errorwa = String.valueOf(error.getMessage());

                    }


                    // handle user friendly errors
                } else {
                    ID id = response.data().getCheckoutCompleteWithCreditCard().getCheckout().getId();

                    paymentInterface.onPayment(response.data().getCheckoutCompleteWithCreditCard().getPayment(), id);
                    boolean checkoutReady = response.data().getCheckoutCompleteWithCreditCard().getCheckout().getReady();
                    boolean paymentReady = response.data().getCheckoutCompleteWithCreditCard().getPayment().getReady();
                    boolean paymentReady1 = response.data().getCheckoutCompleteWithCreditCard().getPayment().getTest();
                    String getAmount = String.valueOf(response.data().getCheckoutCompleteWithCreditCard().getPayment().getAmount());
                    String pay_id = String.valueOf(response.data().getCheckoutCompleteWithCreditCard().getPayment().getId());
                    String error = String.valueOf(response.data().getCheckoutCompleteWithCreditCard().getPayment().getErrorMessage());
                    String error1 = String.valueOf(response.data().getCheckoutCompleteWithCreditCard().getPayment().getErrorMessage());
                }
            }

            @Override
            public void onFailure(@NonNull final GraphError error) {
                // handle errors
                paymentInterface.onFailure(error.getMessage());

            }
        });
    }


    public void completeCheckoutViaCreditCard(Context context, ID checkoutId, CheckoutInterface checkoutInterface) {


        Storefront.QueryRootQuery query = Storefront.query(rootQuery -> rootQuery
                .node(checkoutId, nodeQuery -> nodeQuery
                        .onCheckout(checkoutQuery -> checkoutQuery
                                .order(orderQuery -> orderQuery


                                        .orderNumber()
                                        .totalPrice()
                                )

                        )


                )


        );

        getGraphClient(context).queryGraph(query).enqueue(
                new GraphCall.Callback<Storefront.QueryRoot>() {
                    @Override
                    public void onResponse(@NonNull GraphResponse<Storefront.QueryRoot> response) {
                        Storefront.Checkout checkout = (Storefront.Checkout) response.data().getNode();
                        //  String orderId = checkout.getOrder().getId().toString();
                        //  String paymentReady = String.valueOf(checkout.getOrder().getOrderNumber());
                        checkoutInterface.onError("");
                        if (((Storefront.Checkout) response.data().getNode()).getOrder() != null) {

                            String price = String.valueOf(checkout.getOrder().getTotalPrice());
                            String order = String.valueOf(checkout.getOrder().getOrderNumber());
                            String pricee = String.valueOf(checkout.getOrder().getTotalPrice());
                        }

                    }

                    @Override
                    public void onFailure(@NonNull GraphError error) {
                        checkoutInterface.onError(error.getMessage());

                    }
                },
                null,
                RetryHandler.exponentialBackoff(1, TimeUnit.MILLISECONDS, 1.2f)
                        .<Storefront.QueryRoot>whenResponse(
                                response -> ((Storefront.Checkout) response.data().getNode()).getOrder() == null
                        )
                        .maxCount(10)
                        .build()
        );
    }



/*
    public void checkOutCompleteFree(Context context, ID id, CheckoutInterface checkoutInterface) {

        Storefront.MutationQuery query = Storefront.mutation((mutationQuery -> mutationQuery

                .checkoutCompleteFree(id, checkoutShippingLineUpdate -> checkoutShippingLineUpdate

                        .checkout(checkoutQuery -> checkoutQuery
                                .paymentDue()
                                .totalPrice()
                                .subtotalPrice()
                                .webUrl()
                                .order(orderQuery -> orderQuery


                                        .orderNumber()
                                        .totalPrice()

                                )

                        )
                        .userErrors(userErrorQuery -> userErrorQuery
                                .field()
                                .message()

                        )


                )


        ));




        getGraphClient(context).queryGraph(query).enqueue(
                new GraphCall.Callback<Storefront.QueryRoot>() {
                    @Override public void onResponse(@NonNull GraphResponse<Storefront.QueryRoot> response) {
                        Storefront.Checkout checkout = (Storefront.Checkout) response.data().getNode();
                        String orderId = checkout.getOrder().getId().toString();
                    }

                    @Override public void onFailure(@NonNull GraphError error) {
                    }
                },
                null,
                RetryHandler.exponentialBackoff(1, TimeUnit.MILLISECONDS, 1.2f)
                        .whenResponse(
                                response -> ((Storefront.Checkout) response.data().getNode()).getOrder() == null
                        )
                        .maxCount(10)
                        .build()
        );










        getGraphClient(context).mutateGraph(query).enqueue(new GraphCall.Callback<Storefront.Mutation>() {
            @Override
            public void onResponse(@NonNull GraphResponse<Storefront.Mutation> response) {
               // checkoutInterface.onSucessfullCheckout(response.data().getCheckoutCompleteFree().getCheckout());


               *//* if (!response.data().getCheckoutCompleteFree().getUserErrors().isEmpty()) {
                    // handle user friendly errors

                    for (Storefront.UserError error : response.data().getCheckoutCompleteFree().getUserErrors()) {

                        Log.d(Tag, "error" + error.getMessage());
                        String price = error.getMessage();

                        checkoutInterface.onError(error.getMessage());
                    }

                } else {*//*

                //    boolean checkoutReady = response.data().getCheckoutCompleteFree().getCheckout().getReady();
                    String paymentReady = String.valueOf(response.data().getCheckoutCompleteFree().getCheckout().getOrder().getOrderNumber());
                    String price = String.valueOf(response.data().getCheckoutCompleteFree().getCheckout().getOrder().getTotalPrice());





                    checkoutInterface.onSucessfullCheckout(response.data().getCheckoutCompleteFree().getCheckout());


              //  }
            }

            // Z2lkOi8vc2hvcGlmeS9DaGVja291dC82ZDBkMDljMWRkOTYxZjc1MjllMDZkMjYwZTUzOTQxYz9rZXk9M2NiMzNjYmJhYmFhZTIzYzdhNTU4MzVhY2I3ZDMzMzQ=
            //   https://apptuse-test.myshopify.com/25738608/checkouts/6d0d09c1dd961f7529e06d260e53941c?key=3cb33cbbabaae23c7a55835acb7d3334
            @Override
            public void onFailure(@NonNull GraphError error) {
                checkoutInterface.onError(error.getMessage());
                // handle errors
            }
        });


    }*/

    public void getShippingCharges(Context context, ID id, CheckoutInterface checkoutInterface) {
        Storefront.QueryRootQuery query = Storefront.query(rootQuery -> rootQuery
                .node(id, nodeQuery -> nodeQuery
                        .onCheckout(checkoutQuery -> checkoutQuery
                                        .availableShippingRates(availableShippingRatesQuery -> availableShippingRatesQuery
                                                .ready()

                                                .shippingRates(shippingRateQuery -> shippingRateQuery
                                                        .handle()

                                                        .price()
                                                        .title()


                                                )

                                        )
                               /* .appliedGiftCards(appliedGiftCardsQuery -> appliedGiftCardsQuery

                                        .amountUsed()
                                        .lastCharacters()
                                        .balance()

                                )*/

                        )
                )
        );


        getGraphClient(context).queryGraph(query).enqueue(
                new GraphCall.Callback<Storefront.QueryRoot>() {
                    @Override
                    public void onResponse(@NonNull final GraphResponse<Storefront.QueryRoot> response) {
                        Storefront.Checkout checkout = (Storefront.Checkout) response.data().getNode();
                        checkoutInterface.onSucessfullCheckout(checkout);
                        // List<Storefront.ShippingRate> shippingRates = checkout.getAvailableShippingRates().getShippingRates();
                    }

                    @Override
                    public void onFailure(@NonNull final GraphError error) {
                        checkoutInterface.onError(error.getMessage());
                    }
                },
                null,
                RetryHandler.exponentialBackoff(800, TimeUnit.MILLISECONDS, 1.2f)
                        .<Storefront.QueryRoot>whenResponse(
                                response -> ((Storefront.Checkout) response.data().getNode()).getAvailableShippingRates().getReady() == false
                        )
                        .maxCount(5)
                        .build()
        );


    }

    public void getCustomerDetails(Context context, String accessToken, CustomerDetailInterface customerDetailInterface) {

        Storefront.QueryRootQuery query = Storefront.query(root -> root
                .customer(accessToken, customer -> customer
                        .firstName()
                        .lastName()
                        .email()
                        .phone()
                )
        );

        getGraphClient(context).queryGraph(query).enqueue(new GraphCall.Callback<Storefront.QueryRoot>() {

            @Override
            public void onResponse(@NonNull GraphResponse<Storefront.QueryRoot> response) {


                customerDetailInterface.onSuccessFulDetail(response.data().getCustomer());


            }

            @Override
            public void onFailure(@NonNull GraphError error) {

                customerDetailInterface.onFailure();
            }


        });


    }








/*
    public void getProducts()
    {
        List<Storefront.Product> products = new ArrayList<>();
        for (Storefront.ProductEdge productEdge : collectionEdge.getNode().getProducts().getEdges()) {
            products.add(productEdge.getNode());
        }
        for (int i=0;i<products.size();i++)
        {

            Log.d(Tag,"products 1"+ products.size());
            Log.d(Tag,"products 2"+ products.get(i).getTitle());
            Log.d(Tag,"products 3"+ products.get(i).getDescription());

            Log.d(Tag,"products 5"+ products.get(i).getId());
            Log.d(Tag,"products 5"+ products.get(i).getId());

            strings.add(String.valueOf(products.get(i).getId()));

        }

    }*/
/*


*/

    public void getProductDetail(Context context, String nodeid) {


        Storefront.QueryRootQuery query = Storefront.query(rootQuery -> rootQuery
                .node(new ID(nodeid), nodeQuery -> nodeQuery
                        .onProduct(productQuery -> productQuery
                                .title()
                                .description()
                                .descriptionHtml()
                                .images(arg -> arg.first(10), imageConnectionQuery -> imageConnectionQuery
                                        .edges(imageEdgeQuery -> imageEdgeQuery
                                                .node(imageQuery -> imageQuery
                                                        .src()
                                                )
                                        )
                                )
                                .options(args ->
                                        args.name().values()
                                                .name()
                                                .values()

                                )

                                .variants(arg -> arg.first(10), variantConnectionQuery -> variantConnectionQuery
                                        .edges(variantEdgeQuery -> variantEdgeQuery
                                                .node(productVariantQuery -> productVariantQuery
                                                        .price()
                                                        .title()
                                                        .available()
                                                        .compareAtPrice()
                                                        .availableForSale()
                                                        .weight()
                                                        .weightUnit()
                                                        .sku()
                                                        .selectedOptions(_queryBuilder -> _queryBuilder.name().value())

                                                )
                                        )

                                )
                        )
                )
        );

        getGraphClient(context).queryGraph(query).enqueue(new GraphCall.Callback<Storefront.QueryRoot>() {

            @Override
            public void onResponse(@NonNull GraphResponse<Storefront.QueryRoot> response) {
                Storefront.Product product = (Storefront.Product) response.data().getNode();
                List<Storefront.Image> productImages = new ArrayList<>();
                for (final Storefront.ImageEdge imageEdge : product.getImages().getEdges()) {
                    productImages.add(imageEdge.getNode());
                }
                List<Storefront.ProductVariant> productVariants = new ArrayList<>();
                for (final Storefront.ProductVariantEdge productVariantEdge : product.getVariants().getEdges()) {
                    productVariants.add(productVariantEdge.getNode());
                }


            }

            @Override
            public void onFailure(@NonNull GraphError error) {

            }


        });
    }


}
/*Pagination code*/
/*
 query = Storefront.query(rootQuery -> rootQuery

         .node(new ID(collection_node), nodeQuery -> nodeQuery
         .onCollection(collectionQuery -> collectionQuery
         .products(
         args -> args
         .first(10)
         .after(productPageCursor),
         productConnectionQuery -> productConnectionQuery
         .pageInfo(pageInfoQuery -> pageInfoQuery
         .hasNextPage()
         )
         .edges(productEdgeQuery -> productEdgeQuery
         .node(productQuery -> productQuery
         .title()
         .productType()
         .description()

         .images(arg -> arg.first(10), imageConnectionQuery -> imageConnectionQuery
         .edges(imageEdgeQuery -> imageEdgeQuery
         .node(imageQuery -> imageQuery
         .src()
         )
         )
         )
         .variants(arg -> arg.first(10), variantConnectionQuery -> variantConnectionQuery
         .edges(variantEdgeQuery -> variantEdgeQuery
         .node(productVariantQuery -> productVariantQuery
         .price()
         .title()
         .available()

         )
         )
         )

         )

         )
         )
         ))

         );*/



/**/