package com.brst.shopifyapplication.HelperClasses;

import android.content.Context;
import android.util.Log;

import com.brst.shopifyapplication.Beans.Country;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by brst-pc20 on 3/17/17.
 */

public class CountryGetter {

    Context context;

    public CountryGetter(Context context) {
        this.context = context;
    }


    public ArrayList<Country> ReadFromfile() {
        StringBuilder returnString = new StringBuilder();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try {
            fIn = context.getResources().getAssets()
                    .open("country.txt", Context.MODE_WORLD_READABLE);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null) {
                returnString.append(line);
            }
        } catch (Exception e) {
            Log.e("start", "<--Exception-->" + e);
        } finally {
            try {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            } catch (Exception e2) {
                e2.getMessage();
            }
        }

        String json_string = returnString.toString();
        Log.e("json_string", "<-->" + json_string);
        ArrayList<Country> countries = parseJsonData(json_string);
        return countries;
    }

    private ArrayList<Country> parseJsonData(String json_string) {
        try {
            JSONObject jsonObject = new JSONObject(json_string);
            Log.e("jsonObject>>1", "" + jsonObject);
            JSONArray jArray = jsonObject.getJSONArray("geonames");
            Log.e("jArray", "" + jArray);
            return getStringArray(jArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<Country> getStringArray(JSONArray jsonArray) {
        ArrayList<Country> countriesArrayList = new ArrayList<>();
        int length = jsonArray.length();
        if (jsonArray != null) {
            for (int i = 0; i < length; i++) {

                Country country = new Country();
                JSONObject jsonObject = jsonArray.optJSONObject(i);
                country.setCountryName(jsonObject.optString("countryName"));
                country.setGeoNameId(jsonObject.optString("geonameId"));
                country.setCountryCode(jsonObject.optString("countryCode"));
                countriesArrayList.add(country);
            }
        }
        Log.d("countriesArrayList", "--" + countriesArrayList);
        return countriesArrayList;
    }
}
