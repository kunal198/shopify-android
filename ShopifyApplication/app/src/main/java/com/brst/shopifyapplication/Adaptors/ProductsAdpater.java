package com.brst.shopifyapplication.Adaptors;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Fragments.ProductDetailFragment;
import com.brst.shopifyapplication.HelperClasses.Constants;
import com.brst.shopifyapplication.HelperClasses.MySharedPreferences;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.ShopifyApplication;
import com.bumptech.glide.Glide;
import com.shopify.buy3.Storefront;

import java.util.List;


/**
 * Created by brst-pc20 on 1/9/17.
 */

public class ProductsAdpater extends RecyclerView.Adapter<ProductsAdpater.MyViewHolder> {

    public boolean isLoading = false;
    List<Storefront.Product> data_list;
    Context context;
    Fragment fragment;
    RecyclerView recyclerView;
    //LoadMoreItems loadMoreItemss;
    String currency;
    private int lastVisibleItem,
            totalItemCount, firstVisiblePos, firstVisibleInListview;
    private int visibleThreshold = 5;

    public ProductsAdpater(Context context, Fragment fragment, RecyclerView recyclerView, List<Storefront.Product> data_list) {

        this.context = context;
        this.data_list = data_list;

        this.fragment = fragment;
        this.recyclerView = recyclerView;

        currency = MySharedPreferences.getInstance().getData(ShopifyApplication.getAppContext(), Constants.SELECTED_CURRENCY);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grid_product_view, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        /*
        data_list.get(position).getImages()*/
        if (data_list.get(position).getImages().getEdges().size() > 0) {
            Glide.with(context).load(data_list.get(position).getImages().getEdges().get(0).getNode().getSrc()).into(holder.productIV);
        } else {
            Glide.with(context).load(R.mipmap.placeholder_image).into(holder.productIV);
        }

        String price = String.valueOf(data_list.get(position).getVariants().getEdges().get(0).getNode().getPrice());
        holder.price.setText(currency != null ? currency : "" + price);
        holder.title.setText(data_list.get(position).getTitle());

        holder.saleTagIV.setVisibility(getSaleTag(data_list.get(position)) ? View.VISIBLE : View.GONE);

   /*     Glide.with(context).load(data_list.get(position).getProducts().getEdges().get(position).getNode().getImages().getEdges().get(0).getNode().getSrc()).into(holder.productIV);

        holder.price.setText("$" + data_list.get(position).getNode().getVariants().getEdges().get(0).getNode().getPrice());
        holder.title.setText(data_list.get(position).getProducts().getEdges().get(position).getNode().getTitle());

   */
    }

    private boolean getSaleTag(Storefront.Product data_list) {
        for (int i = 0; i < data_list.getVariants().getEdges().size(); i++) {
            String availableForSale = String.valueOf(data_list.getVariants().getEdges().get(i).getNode().getCompareAtPrice());

            if (!availableForSale.equals("null")) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int getItemCount() {
        return data_list.size();
    }

    public void addList(List<Storefront.Product> data) {
        this.data_list = data;
        fragment.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                notifyDataSetChanged();
            }
        });
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView productIV, saleTagIV;
        TextView price, title;

        public MyViewHolder(View view) {
            super(view);


            productIV = view.findViewById(R.id.productIV);
            price = view.findViewById(R.id.price);
            title = view.findViewById(R.id.title);
            saleTagIV = view.findViewById(R.id.saleTagIV);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
              /*      Bundle bundle = new Bundle();
                    bundle.putSerializable("product_list",  data_list.get(getAdapterPosition()));*/
                    ProductDetailFragment productFragment = new ProductDetailFragment();
                    productFragment.setData(data_list.get(getAdapterPosition()));

                    ((HomeActivity) context).fragmentTransaction(productFragment, R.id.container);
                }
            });


        }
    }
}