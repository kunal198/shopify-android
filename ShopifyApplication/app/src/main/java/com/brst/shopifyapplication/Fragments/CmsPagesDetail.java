package com.brst.shopifyapplication.Fragments;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Interfaces.ShopifyApiInterface;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.RetrofitPackage.ApiClient;
import com.brst.shopifyapplication.databinding.LayoutCmsBinding;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CmsPagesDetail extends BaseFragment {

    static CmsPagesDetail productFragment;
    View view;
    LayoutCmsBinding layoutCmsBinding;

    public static CmsPagesDetail getInstance() {
        if (productFragment == null) {
            return productFragment = new CmsPagesDetail();
        }
        return productFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //  if (view == null) {
        layoutCmsBinding = DataBindingUtil.inflate(
                inflater, R.layout.layout_cms, container, false);
        view = layoutCmsBinding.getRoot();
        getActivity().setTitle("");


        ((HomeActivity) getActivity()).setExpansion(true, false, 0);

        getDataFromBundle();
        return view;
    }

    private void getDataFromBundle() {

        Bundle bundle = getArguments();
        if (bundle != null) {
            String title = bundle.getString("title");
            String id = bundle.getString("id");
            callApi(id);
            getActivity().setTitle(title);

        }

    }


    //https://6a45267ae59af22a49957eb05f32cf3b:81999f1b7aabef1d747a98ae86d0e243@getshopifyapp.myshopify.com/admin/pages.json

   /* private String getHtmlData(String bodyHTML) {
        String head = "<head><style>img{max-width: 100%; width:auto; height: auto; margin:10px 0px 10px 0px; }</style></head>";
        return "<html>" + head + "<body>" + bodyHTML + "</body></html>";
    }*/


    private void callApi(String id) {
        ShopifyApiInterface shopifyApiInterface = ApiClient.getClient().create(ShopifyApiInterface.class);
//        Call<Example> call = shopifyApiInterface.getPages();
        initProgressDialog(getContext());
        Call<JsonObject> call = shopifyApiInterface.getPages(id, getAuthToken());
        call.enqueue(new Callback<JsonObject>() {
            @SuppressLint("JavascriptInterface")
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dismissProgressDialog(getContext());
                JsonObject jsonObject = response.body().getAsJsonObject();
                JsonObject page_object = jsonObject.getAsJsonObject("page");
                String title = page_object.get("title").getAsString();
                String body_html = page_object.get("body_html").getAsString();
                //response.body().getPages().size();
                layoutCmsBinding.titleTV.setText(title != null ? title : "");
                getActivity().setTitle(title != null ? title : "");
                layoutCmsBinding.webBodyTV.getSettings().setJavaScriptEnabled(true);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    layoutCmsBinding.webBodyTV.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING);
                } else {
                    layoutCmsBinding.webBodyTV.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
                }
                layoutCmsBinding.webBodyTV.getSettings().setLoadsImagesAutomatically(true);
                body_html = body_html.replace("src=\"", "src=http:");
                layoutCmsBinding.webBodyTV.loadData(getHtmlData(body_html), "text/html", null);

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dismissProgressDialog(getContext());

                String error = t.getMessage();
                Log.d("Error", t.getMessage());
            }
        });

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.search);
        MenuItem item1 = menu.findItem(R.id.sort);
        MenuItem item12 = menu.findItem(R.id.bag);
        MenuItem logout = menu.findItem(R.id.logout);
        MenuItem mode = menu.findItem(R.id.mode);
        mode.setVisible(false);
        item12.setVisible(false);
        item.setVisible(false);
        item1.setVisible(false);
        logout.setVisible(false);
    }

}

