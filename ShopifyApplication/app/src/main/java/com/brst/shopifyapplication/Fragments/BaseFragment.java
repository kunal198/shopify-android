package com.brst.shopifyapplication.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Beans.CartBean;
import com.brst.shopifyapplication.HelperClasses.CircleProgressBar;
import com.brst.shopifyapplication.HelperClasses.Constants;
import com.brst.shopifyapplication.HelperClasses.MySharedPreferences;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.ShopifyApplication;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;


/**
 */

public abstract class BaseFragment extends Fragment {

    Dialog dialog;
    TextView messageTV;

    ProgressDialog progressDialog;
    int last_page_str, current_page_str;
    Handler handler = null;


    protected void setOnActivityTransfer(Class<?> des_class, int req_code, BaseFragment.OnActivityTransfer listener) {
        startActivity(new Intent(getActivity(), des_class));
        listener.onTransfer(req_code);

    }


    protected void setOnActivityTransfer(Class<?> des_class, int req_code, BaseFragment.OnActivityTransfer listener, String json) {
        Intent intent = new Intent(getActivity(), des_class);
        intent.putExtra("data", json);
        startActivity(intent);
        listener.onTransfer(req_code);


    }

    public void setCountOnBadge() {
        ArrayList<CartBean> existingList = MySharedPreferences.getInstance().getSerializableListData(getActivity());

        if (existingList != null && existingList.size() > 0) {
            ((HomeActivity) getActivity()).setBadge(existingList.size());
            ((HomeActivity) getActivity()).setBadgeCount(getActivity(), existingList.size());
        }

    }


    public String formatString(String s) {

        if (s != null && !s.isEmpty()) {
            Double double_val=Double.parseDouble(s);
          return   new DecimalFormat("################.##").format(double_val);

        }
        return "";


    }

    public void replaceFragment(Fragment fragment, Context context) {
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.container, fragment);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    public String getHtmlData(String bodyHTML) {
        String head = "<head><style>img{max-width: 100%; width:auto; height: auto; margin:10px 0px 10px 0px; }iframe { display: block; width:100%; height: 200px;   margin-top:10px; margin-bottom:10px;}</style></head>";
        return "<html>" + head + "<body style='color:#272727;'>" + bodyHTML + "</body></html>";
    }

    protected String getAuthToken() {
        byte[] data = new byte[0];
        try {
            data = ("ad9b4682b7b1e9b5c0b877ad83de003b" + ":" + "007765389a2a09ec2e47480e18156f1c").getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
    }


    public String getCurrency() {
        String curency = MySharedPreferences.getInstance().getData(ShopifyApplication.getAppContext(), Constants.SELECTED_CURRENCY);

        if (curency == null) {
            return "";
        }
        return curency;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler(Looper.getMainLooper());
    }


    public void showSnackMsg(View view, String msg) {
        Snackbar.make(view, msg, Snackbar.LENGTH_LONG).show();
    }


    public void initProgressDialog(Context context) {
/*
        progressDialog = ProgressDialog.show(context, "", "");
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setCanceledOnTouchOutside(false);
        GeometricProgressView geometricProgressView = progressDialog.findViewById(R.id.progressBar1);
        geometricProgressView.setType(GeometricProgressView.TYPE.TRIANGLE);
        geometricProgressView.setFigurePaddingInDp(1);
        geometricProgressView.setNumberOfAngles(10);
*/

        progressDialog = ProgressDialog.show(context, "", "");
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        progressDialog.setContentView(R.layout.progress_dialog);
        CircleProgressBar circleProgressBar = progressDialog.findViewById(R.id.progressBar1);
        circleProgressBar.setColorSchemeResources(R.color.colorApp, R.color.colorOrange, R.color.colorBlack, R.color.colorBlue);
        progressDialog.show();
    }
/*
, PendingIntent resultPendingIntent
*/

    public void showNotification(Context context, String title, String message) {
        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(ShopifyApplication.getAppContext());

        NotificationCompat.BigTextStyle inboxStyle = new NotificationCompat.BigTextStyle();
        Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        inboxStyle.bigText(message);

        Notification notification;
        notification = mBuilder.setSmallIcon(R.mipmap.ic_launcher)
                .setWhen(0)
                .setAutoCancel(true)
                .setContentTitle(title)
/*
                .setContentIntent(resultPendingIntent)
*/
                .setSound(notificationSound)

                .setStyle(inboxStyle)
                .setPriority(Notification.PRIORITY_MAX)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                .setContentText(message)
                .build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
    }


    public void dismissProgressDialog(Context context) {
        Log.d("progressdialog", "dialog");
        if (progressDialog != null) {
            Log.d("progressdialog", "dialog" + progressDialog);
            if (progressDialog.isShowing()) {

                progressDialog.dismiss();
            }
        }
    }


    //1 minute = 60 seconds
//1 hour = 60 x 60 = 3600
//1 day = 3600 x 24 = 86400


    protected void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }


    public void fragmentTransaction(Fragment fragment, int layout) {
        if (handler == null) {
            handler = new Handler(Looper.getMainLooper());
        }
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.animation_enter_from_right, R.anim.animation_leave_out_to_left, R.anim.animation_enter_from_right, R.anim.animation_leave_out_to_left);
                //  fragmentTransaction.setCustomAnimations(R.anim.animation_enter_from_right, R.anim.animation_leave_out_to_left);
                fragmentTransaction.replace(layout, fragment).addToBackStack(fragment.getClass().getName())
                        .commit();
            }
        };
        handler.post(runnable);

    }

    public void fragmentTransaction(Fragment fragment, int layout, Bundle bundle) {
        if (handler == null) {
            handler = new Handler(Looper.getMainLooper());
        }
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                fragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.animation_enter_from_right, R.anim.animation_leave_out_to_left, R.anim.animation_enter_from_right, R.anim.animation_leave_out_to_left);
                fragmentTransaction.replace(layout, fragment).addToBackStack(fragment.getClass().getName())
                        .commit();
            }
        };
        handler.post(runnable);
    }

    protected void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    protected void showDialog(Context context, String msg) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        ImageView okTV = (ImageView) dialog.findViewById(R.id.cancelIV);


        okTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

    }

    public boolean haveNetworkConnection() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    protected void dismissDialog() {
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }

    protected interface OnActivityTransfer {
        void onTransfer(int req_code);
    }
}
