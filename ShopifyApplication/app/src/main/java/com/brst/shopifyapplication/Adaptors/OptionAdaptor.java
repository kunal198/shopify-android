package com.brst.shopifyapplication.Adaptors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Interfaces.ModeInterface;
import com.brst.shopifyapplication.R;

import java.util.ArrayList;


/**
 * Created by brst-pc89 on 9/5/17.
 */

public class OptionAdaptor extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    int TYPE_HEADER = 0;
    int TYPE_ITEM = 1;
    ArrayList<String> data_list;
    Context context;
    ModeInterface modeInterface;

    public OptionAdaptor(Context context, ArrayList<String> data_list, ModeInterface modeInterface) {

        this.context = context;
        this.data_list = data_list;
        this.modeInterface = modeInterface;


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView;

        if (viewType == TYPE_ITEM) {
            itemView = LayoutInflater.from(context).inflate(R.layout.option_item_view, parent, false);
            return new ItemViewHolder(itemView);

        } else {
            itemView = LayoutInflater.from(context).inflate(R.layout.option_header, parent, false);
            return new HeaderViewHolder(itemView);
        }

    }

    public void addData(ArrayList<String> data_list) {
        this.data_list = data_list;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        String categoryBean = data_list.get(position);

        if (holder instanceof ItemViewHolder) {

            ((ItemViewHolder) holder).categoryNameTV.setText(categoryBean);
        } else if (holder instanceof HeaderViewHolder) {
            //((HeaderViewHolder) holder).viewHeader.setVisibility(View.GONE);
            ((HeaderViewHolder) holder).headerViewTV.setText("Select value");

        }


    }

    @Override
    public int getItemCount() {
        return data_list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        } else {
            return TYPE_ITEM;
        }

    }

    private boolean isPositionHeader(int position) {
        return data_list.get(position) == null;
    }

    public void addList(ArrayList<String> data_list) {
        this.data_list = data_list;
        ((HomeActivity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {

                notifyDataSetChanged();
            }
        });
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView categoryNameTV;


        public ItemViewHolder(View itemView) {
            super(itemView);

            categoryNameTV = itemView.findViewById(R.id.categoryNameTV);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    modeInterface.changeMode(getAdapterPosition());


                }
            });
        }
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {

        TextView headerViewTV;


        public HeaderViewHolder(View itemView) {
            super(itemView);

            headerViewTV = itemView.findViewById(R.id.headerViewTV);

        }
    }


}
