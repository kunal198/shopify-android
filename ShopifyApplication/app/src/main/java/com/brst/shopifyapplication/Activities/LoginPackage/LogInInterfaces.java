package com.brst.shopifyapplication.Activities.LoginPackage;

public interface LogInInterfaces {
    interface LoginViewInterface {
        void setEmailValidError();

        void setEmailEmptyError();

        void setPasswordEmptyError();

        void setPasswordLimitError();

        void navigateToHome();
    }

    interface LoginModelInterface {
        void validateCredentials(String email, String password, Listeners listeners);

        interface Listeners {
            void onEmailEmptyError();

            void onEmailValidError();

            void onPasswordEmptyError();

            void onPasswordLimitError();

            void onSuccess();
        }
    }
}