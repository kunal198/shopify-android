package com.brst.shopifyapplication.Adaptors;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brst.shopifyapplication.Beans.CartBean;
import com.brst.shopifyapplication.HelperClasses.Constants;
import com.brst.shopifyapplication.HelperClasses.MySharedPreferences;
import com.brst.shopifyapplication.Interfaces.CartInterface;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.ShopifyApplication;
import com.bumptech.glide.Glide;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by brst-pc20 on 1/9/17.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {

    String Tag="CartAdapter";
    ArrayList<CartBean> data_list;
    Context context;

    Fragment fragment;
    RecyclerView recyclerView;
    CartInterface cartInterface;
    String currency;
    List<String> qty=new ArrayList<>(Arrays.asList("1","2","3","4","5"));


    public CartAdapter(Context context, Fragment fragment, RecyclerView recyclerView, ArrayList<CartBean> data_list, CartInterface cartInterface) {

        this.context = context;
        this.data_list = data_list;
        this.cartInterface = cartInterface;

        this.fragment = fragment;
        this.recyclerView = recyclerView;
        currency = MySharedPreferences.getInstance().getData(ShopifyApplication.getAppContext(), Constants.SELECTED_CURRENCY);


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cart_list_item, parent, false);

        return new MyViewHolder(itemView);
    }
    public String formatString(String s) {

        if (s != null && !s.isEmpty()) {
            Double double_val=Double.parseDouble(s);
            return   new DecimalFormat("################.##").format(double_val);

        }
        return "";


    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {


        CartBean cartBean = data_list.get(position);
        CartBean.CartDetail cartDetail = cartBean.getCartDetail();

        if (cartDetail.getImageUrl() != null && !cartDetail.getImageUrl().isEmpty()) {

            Glide.with(context).load(cartDetail.getImageUrl()).into(holder.productIV);


        }
        holder.title.setText(cartDetail.getTitle());

        if (cartDetail.getPrice() != null) {

            double price = Double.parseDouble(cartDetail.getPrice()) * cartDetail.getQuantity();

            String price_str =  currency+" "+"<b>" + cartDetail.getPrice() + "</b>";
            String total_price = currency+" "+"<b>" + String.valueOf(price) + "</b>";
            holder.price.setText( Html.fromHtml(price_str));
            holder.totalItemPrice.setText(Html.fromHtml(total_price));







        }










        holder.quantitySP.setItems(qty);


        holder.quantitySP.setText(cartDetail.getQuantity()+"");




        if (cartDetail.getVariantName() != null && !cartDetail.getVariantName().isEmpty()) {
            holder.variant.setVisibility(View.VISIBLE);
            holder.variant.setText(cartDetail.getVariantName() + "");
        } else {
            holder.variant.setVisibility(View.INVISIBLE);









        }
       /* if (position == 0) {
            holder.divider.setVisibility(View.GONE);
        } else {
            holder.divider.setVisibility(View.VISIBLE);

        }*/
    }

    @Override
    public int getItemCount() {
        return data_list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView productIV, deleteIV, decrement_quantity, increment_quantity;
        TextView title, variant, price, quantity, totalItemPrice;
        View divider;
        MaterialSpinner quantitySP;


        public MyViewHolder(View view) {
            super(view);

            productIV = view.findViewById(R.id.productIV);
            title = view.findViewById(R.id.title);
            variant = view.findViewById(R.id.variant);
            price = view.findViewById(R.id.price);
            totalItemPrice = view.findViewById(R.id.totalItemPrice);
            quantity = view.findViewById(R.id.quantity);
            divider = view.findViewById(R.id.divider);
            increment_quantity = view.findViewById(R.id.increment_quantity);
            decrement_quantity = view.findViewById(R.id.decrement_quantity);
            deleteIV = view.findViewById(R.id.deleteIV);
            quantitySP = view.findViewById(R.id.quantitySP);


            /*increment_quantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cartInterface.addClicked(getAdapterPosition());
                }
            });
            decrement_quantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cartInterface.subClicked(getAdapterPosition());
                }
            });*/

            quantitySP.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                @Override
                public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                    Log.d(Tag,"=="+position);
                    Toast.makeText(context,"Quantity Updated",Toast.LENGTH_SHORT).show();
                    cartInterface.addClicked(getAdapterPosition(), String.valueOf(item));
                }
            });
            deleteIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cartInterface.deleteItem(getAdapterPosition());
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*ProductDetailFragment productFragment = new ProductDetailFragment();
                    String json = data_list.get(getAdapterPosition()).getCartDetail().getProduct();
                    Gson gson = new Gson();
                    Type type = new TypeToken<Storefront.Product>() {}.getType();

                    Storefront.Product product = gson.fromJson(json, type);

                    productFragment.setData(product);

                    ((HomeActivity) context).fragmentTransaction(productFragment, R.id.container);*/
                }
            });


        }
    }


}