package com.brst.shopifyapplication.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.widget.ImageView;
import android.widget.Toast;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Beans.CartBean;
import com.brst.shopifyapplication.HelperClasses.MySharedPreferences;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.databinding.LayoutProductDetail2Binding;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.shopify.buy3.Storefront;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class ProductDetailFragment2Backup extends BaseFragment {
    static List<Storefront.Product> productList;
    static ProductDetailFragment2Backup productFragment;
    String title = "", price = "", imageUrl = "";
    String Tag = "ProductDetailFragment";
    View view;
    boolean helperToStopCallForFirstTime = false;
    LayoutProductDetail2Binding layoutProductDetailBinding;
    String product_variant_id = "", variant_name = "";
    ArrayList<CartBean> cartBeans;
    Storefront.Product product;
    String valueOfFirstSpinner = "", valueOfSecondSpinner = "", valueOfThirdSpinner = "";
    int selected_position = 0;
    Handler handler;



    public static ProductDetailFragment2Backup getInstance() {
        if (productFragment == null) {
            return productFragment = new ProductDetailFragment2Backup();
        }
        return productFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //  if (view == null) {
        layoutProductDetailBinding = DataBindingUtil.inflate(
                inflater, R.layout.layout_product_detail2, container, false);
        view = layoutProductDetailBinding.getRoot();
        cartBeans = new ArrayList<>();
        //    initView();
        init(view);
        handler = new Handler(Looper.getMainLooper());
        // }
        // getDataFromBundle();]

        product = productList.get(0);
        selected_position = 0;


        ((HomeActivity) getActivity()).setExpansion(false, true, 0);

        //  }
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        handler.post(runnable);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            getOptions(product);
        }
    };


    public void getOptions(Storefront.Product product) {
        //if (!product.getOptions().get(0).getValues().get(0).equals("Default Title")) {
        for (int i = 0; i < product.getOptions().size(); i++) {

            ArrayList<String> color = new ArrayList<>();
            color.add(product.getOptions().get(i).getName());
            for (int k = 0; k < product.getOptions().get(i).getValues().size(); k++) {
                color.add(product.getOptions().get(i).getValues().get(k));

            }

            createSpinner(color, i, product);

            Log.d(Tag, "name" + product.getOptions().get(i).getName());
            Log.d(Tag, "size" + product.getOptions().get(i).getValues().get(0) + "");
            Log.d(Tag, "size" + product.getOptions().size());


        }
        // }

    }

    private void createSpinner(ArrayList<String> arrayList, int i, Storefront.Product product) {
        if (!product.getOptions().get(0).getValues().get(0).equals("Default Title")) {

            if (i == 0) {
                layoutProductDetailBinding.oneSp.setVisibility(View.VISIBLE);
                layoutProductDetailBinding.oneSp.setItems(arrayList);
                layoutProductDetailBinding.oneSp.setSelectedIndex(1);
                valueOfFirstSpinner = arrayList.get(1);

                layoutProductDetailBinding.oneSp.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                        valueOfFirstSpinner = arrayList.get(position);
                        if (helperToStopCallForFirstTime) {
                            getPositionOfSelectedData(product);
                        }
                    }
                });

            }
            if (i == 1) {
                layoutProductDetailBinding.twoSP.setVisibility(View.VISIBLE);

                layoutProductDetailBinding.twoSP.setItems(arrayList);
                layoutProductDetailBinding.twoSP.setSelectedIndex(1);
                valueOfSecondSpinner = arrayList.get(1);

                layoutProductDetailBinding.twoSP.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                        valueOfSecondSpinner = arrayList.get(position);
                        if (helperToStopCallForFirstTime) {
                            getPositionOfSelectedData(product);
                        }
                    }
                });

            }
            if (i == 2) {
                layoutProductDetailBinding.threeSP.setVisibility(View.VISIBLE);

                layoutProductDetailBinding.threeSP.setItems(arrayList);
                layoutProductDetailBinding.threeSP.setSelectedIndex(1);
                valueOfThirdSpinner = arrayList.get(1);
                layoutProductDetailBinding.threeSP.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                        valueOfThirdSpinner = arrayList.get(position);
                        if (helperToStopCallForFirstTime) {
                            getPositionOfSelectedData(product);
                        }
                    }
                });
            }
        }
        getPositionOfSelectedData(product);
        helperToStopCallForFirstTime = true;

    }

    private void getPositionOfSelectedData(Storefront.Product product) {

        for (int k = 0; k < product.getVariants().getEdges().size(); k++) {
            List<Storefront.SelectedOption> selectedOption = product.getVariants().getEdges().get(k).getNode().getSelectedOptions();
            String value1 = selectedOption.get(0).getValue();

            if (!value1.contains("Default")) {
                for (int i = 0; i < selectedOption.size(); i++) {

                    String value = selectedOption.get(i).getValue();

                    if (i == 0 && valueOfFirstSpinner.equalsIgnoreCase(value) && selectedOption.size() == 1) {
                        variant_name = value;

                        setDataWithDelay(product, k);
                        return;
                    }
                    if (i == 1 && valueOfFirstSpinner.equalsIgnoreCase(selectedOption.get(0).getValue()) && valueOfSecondSpinner.equalsIgnoreCase(value) && selectedOption.size() == 2) {
                        variant_name = selectedOption.get(0).getValue() + "/" + value;

                        setDataWithDelay(product, k);
                        return;

                    }
                    if (i == 2 && valueOfFirstSpinner.equalsIgnoreCase(selectedOption.get(0).getValue()) && valueOfSecondSpinner.equalsIgnoreCase(selectedOption.get(1).getValue()) && valueOfThirdSpinner.equalsIgnoreCase(value) && selectedOption.size() == 3) {
                        variant_name = selectedOption.get(0).getValue() + "/" + selectedOption.get(1).getValue() + "/" + value;

                        setDataWithDelay(product, k);
                        return;

                    }


                }
            } else {
                setDataWithDelay(product, 0);
                break;

            }
        }
    }

    private void setDataWithDelay(Storefront.Product product, int pos) {
        Log.d(Tag, "setDataWithDelay11" );
        dismissProgressDialog(getContext());
        initProgressDialog(getContext());

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d(Tag, "setDataWithDelay22" );

                dismissProgressDialog(getContext());
                setDataOnView(product, pos);

            }
        }, 1000);


    }


    private void setDataOnView(Storefront.Product product, int pos) {
        String compare_price = String.valueOf(product.getVariants().getEdges().get(pos).getNode().getCompareAtPrice());
        price = product.getVariants().getEdges().get(pos).getNode().getPrice() + "";
        title = product.getTitle();
        product_variant_id = String.valueOf(product.getVariants().getEdges().get(pos).getNode().getId());
        // Boolean sale = product.getVariants().getEdges().get(pos).getNode().getAvailable();
        Boolean availableForSale = product.getVariants().getEdges().get(pos).getNode().getAvailableForSale();

        //  Log.d(Tag, "sale" + sale);
        Log.d(Tag, "availableForSale" + availableForSale);


        String weight = String.valueOf(product.getVariants().getEdges().get(pos).getNode().getWeight());
        String weightUnit = String.valueOf(product.getVariants().getEdges().get(pos).getNode().getWeightUnit());
        String sku = String.valueOf(product.getVariants().getEdges().get(pos).getNode().getSku());
        String product_type = product.getProductType();
        String vendor = product.getVendor();
        List<String> tags = product.getTags();
        // String handle = product.getHandle();
        String onlineStoreUrl = product.getOnlineStoreUrl();
        getActivity().setTitle(title != null ? title : "");


        // String id= String.valueOf(product.getVariants().getEdges().get(pos).getNode().getImage().getId());
        // String id= String.valueOf(product.getVariants().getEdges().get(pos).getNode().getImage().getId());


        String src = "";
        if (product.getVariants().getEdges().size() > 0) {
            if (product.getVariants().getEdges().get(pos).getNode().getImage() != null) {
                if (product.getVariants().getEdges().get(pos).getNode().getImage().getSrc() != null) {
                    src = String.valueOf(product.getVariants().getEdges().get(pos).getNode().getImage().getSrc());
                    setImageOnMainPreview(src);
                }

            }
        }
        if (product.getImages().getEdges().size() > 0) {
            imageUrl = product.getImages().getEdges().get(0).getNode().getSrc();
            setImageOnMainPreview(imageUrl);

        }
        //  if (product.getVariants().getEdges().get(pos).getNode().getImage().getId())

        if (src != null && !src.isEmpty()) {
            if (product.getImages().getEdges().size() > 0) {
                // String image_id = String.valueOf(product.getImages().getEdges().get(pos).getNode().getId());


                for (int q = 0; q < product.getImages().getEdges().size(); q++) {

                    // String image_id = String.valueOf(product.getImages().getEdges().get(q).getNode().getSrc());
                    String image_id1 = String.valueOf(product.getImages().getEdges().get(q).getNode().getSrc());
                    if (image_id1 != null) {
                        if (src.equals(image_id1)) {
                            imageUrl = image_id1;
                            //selected_position=q;
                            setImageOnMainPreview(src);
                            //foldableListLayout.scrollToPosition(q);
                        }
                    }
                }


            }
        }


        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            layoutProductDetailBinding.descriptionTV.setText(product.getDescriptionHtml() != null ? Html.fromHtml(product.getDescriptionHtml(), Html.FROM_HTML_MODE_COMPACT) : "");
        } else {
            layoutProductDetailBinding.descriptionTV.setText(product.getDescriptionHtml() != null ? Html.fromHtml(product.getDescriptionHtml()) : "");
        }
*/
        layoutProductDetailBinding.webview.getSettings().setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            layoutProductDetailBinding.webview.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING);
        } else {
            layoutProductDetailBinding.webview.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        }
        layoutProductDetailBinding.webview.getSettings().setLoadsImagesAutomatically(true);

        if (product.getDescriptionHtml() != null) {

            layoutProductDetailBinding.webview.loadData(getHtmlData(product.getDescriptionHtml()), "text/html", null);
        }


        if (title != null && !title.isEmpty()) {

            layoutProductDetailBinding.title.setText(title);
        }

       /* if (availableForSale) {
            layoutProductDetailBinding.shopNowBT.setText(availableForSale ? getResources().getString(R.string.shop_now) : getResources().getString(R.string.sold));
        } else {
            layoutProductDetailBinding.shopNowBT.setText(availableForSale ? getResources().getString(R.string.shop_now) : getResources().getString(R.string.sold));
            layoutProductDetailBinding.shopNowBT.setTextAppearance(getActivity(), R.style.buttonStyleDisable);
        }*/

        if (product_type != null && !product_type.isEmpty()) {

            layoutProductDetailBinding.productType.setText(product_type);
        }

        if (vendor != null && !vendor.isEmpty()) {

            layoutProductDetailBinding.vendor.setText(vendor);
        }

        if (onlineStoreUrl != null && !onlineStoreUrl.isEmpty()) {

            layoutProductDetailBinding.onlineUrlLL.setVisibility(View.VISIBLE);
            layoutProductDetailBinding.fbShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
// intent.putExtra(Intent.EXTRA_SUBJECT, "Foo bar"); // NB: has no effect!
                    intent.putExtra(Intent.EXTRA_TEXT, onlineStoreUrl);

// See if official Facebook app is found
                    boolean facebookAppFound = false;
                    List<ResolveInfo> matches = getActivity().getPackageManager().queryIntentActivities(intent, 0);
                    for (ResolveInfo info : matches) {
                        if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook.katana")) {
                            intent.setPackage(info.activityInfo.packageName);
                            facebookAppFound = true;
                            break;
                        }
                    }

// As fallback, launch sharer.php in a browser
                    if (!facebookAppFound) {
                        String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + onlineStoreUrl;
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
                    }

                    startActivity(intent);

                }
            });

            layoutProductDetailBinding.twitterShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
// intent.putExtra(Intent.EXTRA_SUBJECT, "Foo bar"); // NB: has no effect!
                    intent.putExtra(Intent.EXTRA_TEXT, onlineStoreUrl);

// See if official Facebook app is found
                    boolean facebookAppFound = false;
                    List<ResolveInfo> matches = getActivity().getPackageManager().queryIntentActivities(intent, 0);
                    for (ResolveInfo info : matches) {
                        if (info.activityInfo.packageName.toLowerCase().startsWith("com.twitter.android")) {
                            intent.setPackage(info.activityInfo.packageName);
                            facebookAppFound = true;
                            break;
                        }
                    }

// As fallback, launch sharer.php in a browser
                    if (!facebookAppFound) {
                        String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + onlineStoreUrl;
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
                    }

                    startActivity(intent);

                }
            });
        }

        layoutProductDetailBinding.pinterestShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "https://www.pinterest.com/pin/create/button/?url=%s" + onlineStoreUrl;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                filterByPackageName(getActivity(), intent, "com.pinterest");
                startActivity(intent);
            }
        });


        if (tags.size() > 0) {

            Log.d("tag1", "" + tags.size());
            String tagss = "";
            for (int i = 0; i < tags.size(); i++) {
                Log.d("tag2", "" + tags.get(i));

                tagss = tagss + tags.get(i);
                Log.d("tag3", "" + tagss);

                if (i < tags.size() - 1) {
                    tagss = tagss + ",";
                    Log.d("tag4", "" + tagss);

                }

            }

            layoutProductDetailBinding.tags.setText(tagss);
        }

        if (compare_price != null && !compare_price.equals("null") && !compare_price.isEmpty()) {

            layoutProductDetailBinding.comparePriceTV.setVisibility(View.VISIBLE);
            layoutProductDetailBinding.comparePriceTV.setPaintFlags(layoutProductDetailBinding.comparePriceTV.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            layoutProductDetailBinding.comparePriceTV.setText(getCurrency() + compare_price);
        }
        if (price != null && !price.isEmpty()) {

            layoutProductDetailBinding.price.setText(getCurrency() + price);
        }
        if (sku != null && !sku.isEmpty() && !sku.equals("null")) {
            layoutProductDetailBinding.sku.setVisibility(View.VISIBLE);

            layoutProductDetailBinding.sku.setText("SKU: " + sku);
        }

        if (weight != null && weightUnit != null && !weight.equals("0.0") && !weight.equals("0.00")) {
            layoutProductDetailBinding.weight.setVisibility(View.VISIBLE);


            layoutProductDetailBinding.weight.setText("Weight: " + weight + " " + weightUnit);

        }


    }


    public void filterByPackageName(Context context, Intent intent, String prefix) {
        List<ResolveInfo> matches = context.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo info : matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith(prefix)) {
                intent.setPackage(info.activityInfo.packageName);
                return;
            }
        }
    }


    public void addClicked() {
        String qty = layoutProductDetailBinding.quantityTV.getText().toString();
        int qty_int = Integer.parseInt(qty) + 1;

        layoutProductDetailBinding.quantityTV.setText(String.valueOf(qty_int));
    }


    public void subClicked() {
        String qty = layoutProductDetailBinding.quantityTV.getText().toString();

        int qty_int = Integer.parseInt(qty);
        if (qty_int > 1) {
            qty_int = qty_int - 1;
            layoutProductDetailBinding.quantityTV.setText(String.valueOf(qty_int));

        }
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.search);
        MenuItem item1 = menu.findItem(R.id.sort);
        MenuItem item12 = menu.findItem(R.id.bag);
        MenuItem logout = menu.findItem(R.id.logout);
        MenuItem mode = menu.findItem(R.id.mode);
        mode.setVisible(false);
        item12.setVisible(false);
        item.setVisible(false);
        item1.setVisible(false);
        logout.setVisible(false);
    }

    private void init(View view) {

        int size = productList.get(0).getImages().getEdges().size();

        // Initializing video player with developeyoutubeFragment


        layoutProductDetailBinding.emptyView.setVisibility(size > 0 ? View.GONE : View.VISIBLE);
        setVisibilityOfPreviews(size);
        if (size > 0) {
            setImageOnMainPreview(productList.get(0).getImages().getEdges().get(0).getNode().getSrc());
        }

        layoutProductDetailBinding.shopNowBT.setOnClickListener(onClickListener);
        layoutProductDetailBinding.decrementQuantity.setOnClickListener(onClickListener);
        layoutProductDetailBinding.incrementQuantity.setOnClickListener(onClickListener);


    }


    private void setVisibilityOfPreviews(int count) {

        if (count > 0) {
            layoutProductDetailBinding.previewOne.setVisibility(View.VISIBLE);
            layoutProductDetailBinding.previewOne.setOnClickListener(onClickListener);
            String src1 = productList.get(0).getImages().getEdges().get(0).getNode().getSrc();

            Glide.with(getActivity()).applyDefaultRequestOptions(new RequestOptions().placeholder(R.mipmap.placeholder_image)).load(src1).into(layoutProductDetailBinding.previewOne);
        }
        if (count > 1) {
            layoutProductDetailBinding.previewTwo.setVisibility(View.VISIBLE);
            layoutProductDetailBinding.previewTwo.setOnClickListener(onClickListener);
            String src1 = productList.get(0).getImages().getEdges().get(1).getNode().getSrc();
            Glide.with(getActivity()).applyDefaultRequestOptions(new RequestOptions().placeholder(R.mipmap.placeholder_image)).load(src1).into(layoutProductDetailBinding.previewTwo);
        }
        if (count > 2) {
            layoutProductDetailBinding.previewThree.setVisibility(count > 0 ? View.VISIBLE : View.GONE);
            layoutProductDetailBinding.previewThree.setOnClickListener(onClickListener);
            String src1 = productList.get(0).getImages().getEdges().get(2).getNode().getSrc();
            Glide.with(getActivity()).applyDefaultRequestOptions(new RequestOptions().placeholder(R.mipmap.placeholder_image)).load(src1).into(layoutProductDetailBinding.previewThree);
        }
        if (count > 3) {
            layoutProductDetailBinding.previewFourRL.setVisibility(View.VISIBLE);
            layoutProductDetailBinding.previewFour.setOnClickListener(onClickListener);
            layoutProductDetailBinding.myImageViewText.setText("+" + (count - 3));
            String src1 = productList.get(0).getImages().getEdges().get(3).getNode().getSrc();
            Glide.with(getActivity()).applyDefaultRequestOptions(new RequestOptions().placeholder(R.mipmap.placeholder_image)).load(src1).into(layoutProductDetailBinding.previewFour);
        }
        layoutProductDetailBinding.mainPreview.setOnClickListener(onClickListener);


    }


    private void bottomSheet()
    {


    }











    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            switch (view.getId()) {
                case R.id.shopNowBT:

                   /* if (layoutProductDetailBinding.shopNowBT.getText().equals(getResources().getString(R.string.shop_now))) {

                    showNotification(getActivity(), "Order Added In Bag","There is pending order in your cart");
                        fetChandStoreDataInLocalPersistent(product_variant_id);


                    } else {
                        Toast.makeText(getActivity(), "Sorry! This prododuct is currently not available", Toast.LENGTH_SHORT).show();

                    }*/

                    break;
                case R.id.increment_quantity:
                    addClicked();
                    break;
                case R.id.decrement_quantity:
                    subClicked();
                    break;
                case R.id.previewOne:
                    selected_position = 0;

                    String src1 = productList.get(0).getImages().getEdges().get(selected_position).getNode().getSrc();
                    setBackground(layoutProductDetailBinding.previewOne, layoutProductDetailBinding.previewTwo, layoutProductDetailBinding.previewThree);
                    setImageOnMainPreview(src1);

                    break;
                case R.id.previewTwo:
                    selected_position = 1;

                    String src2 = productList.get(0).getImages().getEdges().get(selected_position).getNode().getSrc();
                    setBackground(layoutProductDetailBinding.previewTwo, layoutProductDetailBinding.previewOne, layoutProductDetailBinding.previewThree);


                    setImageOnMainPreview(src2);

                    break;
                case R.id.previewThree:
                    selected_position = 2;

                    String src3 = productList.get(0).getImages().getEdges().get(selected_position).getNode().getSrc();
                    setBackground(layoutProductDetailBinding.previewThree, layoutProductDetailBinding.previewOne, layoutProductDetailBinding.previewTwo);
                    setImageOnMainPreview(src3);

                    break;
                case R.id.previewFour:
                  /*  String src4 = productList.get(0).getImages().getEdges().get(3).getNode().getSrc();
                    layoutProductDetailBinding.previewFour.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.white_edittext_blue_lining_background));
                    Glide.with(getActivity()).applyDefaultRequestOptions(new RequestOptions().placeholder(R.mipmap.placeholder_image)).load(src4).into(layoutProductDetailBinding.mainPreview);
*/
                    selected_position = 3;

                    openGallerView();
                    break;
                case R.id.mainPreview:
                    openGallerView();

                    break;


            }


        }
    };


    private void openGallerView() {
        if (productList.get(0).getImages().getEdges().size() > 0) {


            Bundle bundle = new Bundle();

            bundle.putSerializable("image_list", (Serializable) productList.get(0).getImages().getEdges());
            bundle.putInt("position", selected_position);

            fragmentTransaction(GalleryViewFragment.getInstance(), R.id.container, bundle);
        }
    }

    private void setImageOnMainPreview(String src) {
        layoutProductDetailBinding.progress.setVisibility(View.VISIBLE);

        Glide.with(getActivity()).load(src).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                layoutProductDetailBinding.progress.setVisibility(View.GONE);

                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                layoutProductDetailBinding.progress.setVisibility(View.GONE);
                return false;
            }
        }).into(layoutProductDetailBinding.mainPreview);

    }


    private void setBackground(ImageView imageView, ImageView imageView1, ImageView imageView2) {

        imageView.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.white_edittext_blue_lining_background));
        imageView1.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.white_edittext_grey_lining_background));
        imageView2.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.white_edittext_grey_lining_background));

    }


    public void fetChandStoreDataInLocalPersistent(String product_variant_id) {

        //Object object = LocalPersistence.readObjectFromFile(ShopifyApplication.getAppContext(), "cartFile");
        ArrayList<CartBean> existingList = MySharedPreferences.getInstance().getSerializableListData(getActivity());
        if (existingList != null && existingList.size() > 0) {
            boolean helper = true;

            for (int i = 0; i < existingList.size(); i++) {
                CartBean cartBean = existingList.get(i);
                CartBean.CartDetail cartDetail = existingList.get(i).getCartDetail();

                if (cartBean.getVariantId().equals(product_variant_id)) {
                    helper = false;
                    int prv_qty = Integer.parseInt(layoutProductDetailBinding.quantityTV.getText().toString());
                    cartDetail.setQuantity(cartDetail.getQuantity() + prv_qty);


                    cartBean.setCartDetail(cartDetail);

                    existingList.set(i, cartBean);


                    break;
                }


            }
            if (helper) {

                existingList.add(addNewData());
                Toast.makeText(getActivity(), "Product added to cart", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(getActivity(), "Product quantity Updated", Toast.LENGTH_SHORT).show();

            }

            //     LocalPersistence.witeObjectToFile(ShopifyApplication.getAppContext(), cartBeans, "cartFile");
            MySharedPreferences.getInstance().storeSerializableListData(getActivity(), existingList);


        } else {
            cartBeans.add(addNewData());
            // LocalPersistence.witeObjectToFile(ShopifyApplication.getAppContext(), cartBeans, "cartFile");
            MySharedPreferences.getInstance().storeSerializableListData(getActivity(), cartBeans);

            Toast.makeText(getActivity(), "Product added to cart", Toast.LENGTH_SHORT).show();

        }


    }

    private CartBean addNewData() {
        CartBean cartBean = new CartBean();
        cartBean.setVariantId(product_variant_id);
        CartBean.CartDetail cartDetail = new CartBean.CartDetail();
        cartDetail.setTitle(title);
        cartDetail.setPrice(price);
        cartDetail.setQuantity(Integer.parseInt(layoutProductDetailBinding.quantityTV.getText().toString()));
        cartDetail.setImageUrl(imageUrl);
        cartDetail.setVariantName(variant_name);
        Gson gson = new Gson();
        String json = gson.toJson(product);
        cartDetail.setProduct(json);

        cartBean.setCartDetail(cartDetail);


        return cartBean;

    }

    public void setData(Storefront.Product collection) {
        productList = new ArrayList<>();
        productList.add(collection);
        //       layoutProductDetailBinding.title.setText(collection.getVariants().getEdges().get(0).getNode());

    }

}

