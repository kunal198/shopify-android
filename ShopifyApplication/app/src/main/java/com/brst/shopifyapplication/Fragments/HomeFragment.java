package com.brst.shopifyapplication.Fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Adaptors.CategoryListingAdapter;
import com.brst.shopifyapplication.HelperClasses.SliderLayout;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.databinding.FragmentHomeBinding;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import java.util.ArrayList;
import java.util.HashMap;


public class HomeFragment extends BaseFragment implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    public static FragmentHomeBinding fragmentHomeBinding;
    static HomeFragment homeFragment;
    View view;
    CategoryListingAdapter categoryListingAdapter;
    private android.support.v7.widget.Toolbar toolbar;
    private RecyclerView recyclerView;
    private CardView cardView;

    public static HomeFragment getInstance() {
        if (homeFragment == null) {
            return homeFragment = new HomeFragment();
        }
        return homeFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //  if (view == null) {
        fragmentHomeBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_home, container, false);
        view = fragmentHomeBinding.getRoot();
        initContrls();
        setCategoryListingAdaptor();
        //   }
        getActivity().setTitle("Home");
        ((HomeActivity) getActivity()).setExpansion(true, false, 0);

        ((HomeActivity) getActivity()).setVisibilityOfSearch(false);
        //    }
        ((HomeActivity) getActivity()).enableDrawer();
        fragmentHomeBinding.shopNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheet();
                //  fragmentTransaction(TagsListing.getInstance(),R.id.container);
            }
        });
        fragmentHomeBinding.shopNow2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheet2();
                //  fragmentTransaction(TagsListing.getInstance(),R.id.container);
            }
        });

        return view;
    }


    private void bottomSheet() {
        ArrayList<String> ss = new ArrayList<>();
        ss.add("s");
        ss.add("s");
        ss.add("s");
        ss.add("s");
        ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, ss);

        DialogPlus dialog = DialogPlus.newDialog(getActivity())
                .setAdapter(arrayAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                    }
                })
                .setExpanded(true)
                .setGravity(Gravity.TOP)// This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();
    }

    private void bottomSheet2() {
        ArrayList<String> ss = new ArrayList<>();
        ss.add("s");
        ss.add("s");
        ss.add("s");
        ss.add("s");
        ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, ss);

        DialogPlus dialog = DialogPlus.newDialog(getActivity())
                .setAdapter(arrayAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                    }
                })
                .setExpanded(true)
                .setGravity(Gravity.BOTTOM)// This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.search);
        MenuItem item1 = menu.findItem(R.id.sort);
        MenuItem item12 = menu.findItem(R.id.bag);
        MenuItem logout = menu.findItem(R.id.logout);
        MenuItem mode = menu.findItem(R.id.mode);
        mode.setVisible(false);
        item12.setVisible(false);
        item.setVisible(false);
        item1.setVisible(false);
        logout.setVisible(true);
    }

    private void initContrls() {


        HashMap<String, Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("Ws", R.drawable.slid1);
        file_maps.put("Ws Design", R.drawable.slid2);
        file_maps.put("Ws Android", R.drawable.slid3);
        file_maps.put("Ws Web", R.drawable.slid4);


        for (String name : file_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);


            textSliderView.bundle(new Bundle());
            // textSliderView.getBundle().putString("extra",name);

            fragmentHomeBinding.mDemoSlider.addSlider(textSliderView);
        }
        fragmentHomeBinding.mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        fragmentHomeBinding.mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        fragmentHomeBinding.mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        fragmentHomeBinding.mDemoSlider.setDuration(4000);
        fragmentHomeBinding.mDemoSlider.addOnPageChangeListener(this);

    }


    private void setCategoryListingAdaptor() {

        ArrayList<Integer> images = new ArrayList<>();
        images.add(R.drawable.one);
        images.add(R.drawable.two);
        images.add(R.drawable.three);
        images.add(R.drawable.one);
        images.add(R.drawable.three);
        images.add(R.drawable.two);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());


        fragmentHomeBinding.categoryListRV.setLayoutManager(mLayoutManager);
        fragmentHomeBinding.categoryListRV.setItemAnimator(new DefaultItemAnimator());
        categoryListingAdapter = new CategoryListingAdapter(getActivity(), HomeFragment.this, fragmentHomeBinding.categoryListRV, images);

        fragmentHomeBinding.categoryListRV.setAdapter(categoryListingAdapter);

    }

    @Override
    public void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        fragmentHomeBinding.mDemoSlider.stopAutoCycle();
        super.onStop();
    }


    @Override
    public void onResume() {
        super.onResume();
        fragmentHomeBinding.mDemoSlider.startAutoCycle();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        Log.d("Slider Demo", "Page Changed: " + position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

}

