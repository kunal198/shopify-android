package com.brst.shopifyapplication.Fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Adaptors.CatSubcatAdapter;
import com.brst.shopifyapplication.Beans.CategorySubCategoryBean;
import com.brst.shopifyapplication.Beans.MainCatSubCategoryBean;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.databinding.FragmentExpandableCategoryBinding;

import java.util.Arrays;
import java.util.List;


public class CategorySubCategoryFragment extends Fragment {


    static CategorySubCategoryFragment categorySubCategoryFragment;
    View view;
    CatSubcatAdapter mAdapter;
    FragmentExpandableCategoryBinding fragmentExpandableCategoryBinding;

    public static CategorySubCategoryFragment getInstance() {
        if (categorySubCategoryFragment == null) {
            return categorySubCategoryFragment = new CategorySubCategoryFragment();
        }
        return categorySubCategoryFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //  if (view == null) {
          /*  ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }*/
        // if (view == null) {
        fragmentExpandableCategoryBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_expandable_category, container, false);
        view = fragmentExpandableCategoryBinding.getRoot();
        ((HomeActivity) getActivity()).setExpansion(true, false, 0);
        initView();

        //   }
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.search);
        MenuItem item1 = menu.findItem(R.id.sort);
        MenuItem item12 = menu.findItem(R.id.bag);
        MenuItem logout = menu.findItem(R.id.logout);
        MenuItem mode = menu.findItem(R.id.mode);
        mode.setVisible(false);
        item12.setVisible(false);
        item.setVisible(false);
        item1.setVisible(false);
        logout.setVisible(false);
    }

    private void initView() {
        CategorySubCategoryBean tops = new CategorySubCategoryBean("Tops");
        CategorySubCategoryBean dresses = new CategorySubCategoryBean("Dresses & JumpSuits");
        CategorySubCategoryBean jeans = new CategorySubCategoryBean("Jeans");
        CategorySubCategoryBean tunics = new CategorySubCategoryBean("Tunics");
        CategorySubCategoryBean trousers = new CategorySubCategoryBean("Trousers");
        CategorySubCategoryBean shorts = new CategorySubCategoryBean("Shorts");

        CategorySubCategoryBean caps = new CategorySubCategoryBean("Caps");
        CategorySubCategoryBean socks = new CategorySubCategoryBean("Socks");

        MainCatSubCategoryBean indianwear = new MainCatSubCategoryBean("INDIANWEAR", Arrays.asList(tops, dresses, jeans, tunics));
        MainCatSubCategoryBean westernwear = new MainCatSubCategoryBean("WESTERNWEAR", Arrays.asList(trousers, shorts, caps, socks));
        MainCatSubCategoryBean footwear = new MainCatSubCategoryBean("FOOTWEAR", Arrays.asList(tops, dresses, jeans, tunics));
        MainCatSubCategoryBean taco = new MainCatSubCategoryBean("LINGERIE & SLEEPWEAR", Arrays.asList(tops, dresses, jeans, tunics));
        MainCatSubCategoryBean quesadilla = new MainCatSubCategoryBean("WESTERNWEAR", Arrays.asList(trousers, shorts, caps, socks));
        MainCatSubCategoryBean burger = new MainCatSubCategoryBean("WATCHES", Arrays.asList(tops, dresses, jeans, tunics));
        MainCatSubCategoryBean shoes = new MainCatSubCategoryBean("SHOES", Arrays.asList(tops, dresses, jeans, tunics));
        MainCatSubCategoryBean one = new MainCatSubCategoryBean("WESTERNWEAR", Arrays.asList(trousers, shorts, caps, socks));
        MainCatSubCategoryBean two = new MainCatSubCategoryBean("FOOTWEAR", Arrays.asList(tops, dresses, jeans, tunics));
        MainCatSubCategoryBean three = new MainCatSubCategoryBean("LINGERIE & SLEEPWEAR", Arrays.asList(tops, dresses, jeans, tunics));
        MainCatSubCategoryBean four = new MainCatSubCategoryBean("WESTERNWEAR", Arrays.asList(trousers, shorts, caps, socks));
        MainCatSubCategoryBean five = new MainCatSubCategoryBean("WATCHES", Arrays.asList(tops, dresses, jeans, tunics));
        MainCatSubCategoryBean six = new MainCatSubCategoryBean("SHOES", Arrays.asList(tops, dresses, jeans, tunics));
        final List<MainCatSubCategoryBean> categoryBeanList = Arrays.asList(indianwear, westernwear, footwear, taco, quesadilla, burger, shoes, one, two, three, footwear, four, five, six);

        fragmentExpandableCategoryBinding.categoryExpandableRV.setLayoutManager(new LinearLayoutManager(getActivity()));


        mAdapter = new CatSubcatAdapter(getActivity(), categoryBeanList);
        fragmentExpandableCategoryBinding.categoryExpandableRV.setAdapter(mAdapter);
        mAdapter.setExpandCollapseListener(new ExpandableRecyclerAdapter.ExpandCollapseListener() {
            @UiThread
            @Override
            public void onParentExpanded(int parentPosition) {
                MainCatSubCategoryBean expandedRecipe = categoryBeanList.get(parentPosition);

                Toast.makeText(getActivity(),
                        expandedRecipe.getName(), Toast.LENGTH_SHORT)
                        .show();
            }

            @UiThread
            @Override
            public void onParentCollapsed(int parentPosition) {
                MainCatSubCategoryBean collapsedRecipe = categoryBeanList.get(parentPosition);

                Toast.makeText(getActivity(),
                        collapsedRecipe.getName(),
                        Toast.LENGTH_SHORT)
                        .show();
            }
        });


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mAdapter.onSaveInstanceState(outState);
    }


}

