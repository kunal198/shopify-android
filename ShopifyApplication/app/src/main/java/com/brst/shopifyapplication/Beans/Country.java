package com.brst.shopifyapplication.Beans;

/**
 * Created by brst-pc20 on 3/17/17.
 */

public class Country<T> {

    private T countryName, geoNameId, countryCode;

    public T getGeoNameId() {
        return geoNameId;
    }

    public void setGeoNameId(T geoNameId) {
        this.geoNameId = geoNameId;
    }

    public T getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(T countryCode) {
        this.countryCode = countryCode;
    }

    public T getCountryName() {
        return countryName;
    }

    public void setCountryName(T countryName) {
        this.countryName = countryName;
    }


    @Override
    public String toString() {
        return "" + countryName;
    }

    public String geoID() {
        return "" + geoNameId;
    }

    public String countryCode() {
        return "" + countryCode;
    }

}
