package com.brst.shopifyapplication.Adaptors;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.brst.shopifyapplication.Beans.Country;
import com.brst.shopifyapplication.R;

import java.util.ArrayList;
import java.util.List;

public class CustomerAdapter extends ArrayAdapter<Country> {
    ArrayList<Country> mCustomers;
    private LayoutInflater layoutInflater;
    private Filter mFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            return ((Country) resultValue).getCountryName().toString();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null) {
                ArrayList<Country> suggestions = new ArrayList<Country>();
                for (Country customer : mCustomers) {
                    // Note: change the "contains" to "startsWith" if you only want starting matches
                    if (customer.getCountryName().toString().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(customer);
                    }
                }

                results.values = suggestions;
                results.count = suggestions.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            if (results != null && results.count > 0) {
                // we have filtered results
                addAll((ArrayList<Country>) results.values);
            } else {
                // no filter, add entire original list back in
                addAll(mCustomers);
            }
            notifyDataSetChanged();
        }
    };

    public CustomerAdapter(Context context, int textViewResourceId, List<Country> customers) {
        super(context, textViewResourceId, customers);
        // copy all the customers into a master list
        mCustomers = new ArrayList<Country>(customers.size());
        mCustomers.addAll(customers);
        layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            view = layoutInflater.inflate(R.layout.spinner_layout2, null);
        }

        Country customer = getItem(position);

        TextView name = (TextView) view.findViewById(R.id.sortItemmTV);
        name.setText(customer.getCountryName().toString());

        return view;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }


    public void addList(ArrayList<Country> arrayList) {
        this.mCustomers = arrayList;
        notifyDataSetChanged();
    }
}