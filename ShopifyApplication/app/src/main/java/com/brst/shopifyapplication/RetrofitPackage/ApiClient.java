package com.brst.shopifyapplication.RetrofitPackage;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Jatin on 10/27/17.
 */
public class ApiClient {

    // public static final String BASE_URL = "http://cartradersa.com/car-trader/webservices/";
    private static final String BASE_URL = "http://app.cartradersa.com/webservices/";
    private static final String BASE_URL2 = "https://6a45267ae59af22a49957eb05f32cf3b81999f1b7aabef1d747a98ae86d0e243@getshopifyapp.myshopify.com/";
    private static final String BASE_URL21 = "https://720ddd6752f8c3d1de9dbdacafc93b32:8cedda1cc48a1f5f5a5392d42d6d2b33@apptuse-test.myshopify.com/";
    private static final String BASE_URL211 = "https://6a45267ae59af22a49957eb05f32cf3b:81999f1b7aabef1d747a98ae86d0e243@getshopifyapp.myshopify.com/";
//    private static final String BASE_ = "https://6a45267ae59af22a49957eb05f32cf3b:81999f1b7aabef1d747a98ae86d0e243@getshopifyapp.myshopify.com";

   // private static final String BASE_ = "https://9ba157482df7ad72661cc8953e98a8c4:53456866997e0706e40ba3b85ed24387@shoipybrst.myshopify.com/admin/";
    private static final String BASE_ = "https://ad9b4682b7b1e9b5c0b877ad83de003b:007765389a2a09ec2e47480e18156f1c@brsttheme.myshopify.com/admin/";
   // private static final String BASE_ = "https://6a45267ae59af22a49957eb05f32cf3b:81999f1b7aabef1d747a98ae86d0e243@getshopifyapp.myshopify.com/admin/";

    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }


}
