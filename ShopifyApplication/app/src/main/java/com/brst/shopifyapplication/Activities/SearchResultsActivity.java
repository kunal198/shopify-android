package com.brst.shopifyapplication.Activities;

import android.app.ListActivity;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.util.Log;
import android.widget.Toast;

import com.brst.shopifyapplication.HelperClasses.MySuggestionProvider;


public class SearchResultsActivity extends ListActivity {

    String Tag = "SearchResultsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Toast.makeText(SearchResultsActivity.this, "search", Toast.LENGTH_SHORT).show();
        Log.d(Tag, "intent_called");

        handleIntent(getIntent());


        Log.d(Tag, "intent_called");

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Toast.makeText(SearchResultsActivity.this, "search", Toast.LENGTH_SHORT).show();
        Log.d(Tag, "intent_called");

        handleIntent(intent);

    }

    private void handleIntent(Intent intent) {
        Bundle appDataa = getIntent().getBundleExtra(SearchManager.APP_DATA);

        Log.d(Tag, "intent_called");


        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {

            Bundle appData = getIntent().getBundleExtra(SearchManager.APP_DATA);

            String query = intent.getStringExtra(SearchManager.QUERY);
            //use the query to search your data somehow

            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                    MySuggestionProvider.AUTHORITY, MySuggestionProvider.MODE);
            suggestions.saveRecentQuery(query, null);
           /* Intent intent1 = new Intent(this, HomeActivity.class);
            startActivity(intent1);*/

        }
    }


}
