package com.brst.shopifyapplication.Adaptors.ViewHolders;

import android.annotation.SuppressLint;
import android.os.Build;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.brst.shopifyapplication.Beans.MainCatSubCategoryBean;
import com.brst.shopifyapplication.R;


public class CategoryViewHolder extends ParentViewHolder {

    private static final float INITIAL_POSITION = 0.0f;
    private static final float ROTATED_POSITION = 90f;

    private TextView mRecipeTextView;
    private ImageView arrowRightIV;

    public CategoryViewHolder(@NonNull View itemView) {
        super(itemView);
        mRecipeTextView = itemView.findViewById(R.id.recipe_textview);
        arrowRightIV = itemView.findViewById(R.id.arrowRightIV);

    }

    public void bind(@NonNull MainCatSubCategoryBean categoryBean) {
        mRecipeTextView.setText(categoryBean.getName());
    }

    @SuppressLint("NewApi")
    @Override
    public void setExpanded(boolean expanded) {
        super.setExpanded(expanded);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            if (expanded) {
                arrowRightIV.setRotation(ROTATED_POSITION);
            } else {
                arrowRightIV.setRotation(INITIAL_POSITION);
            }
        }
    }

    @Override
    public void onExpansionToggled(boolean expanded) {
        super.onExpansionToggled(expanded);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            RotateAnimation rotateAnimation;
            if (expanded) { // rotate clockwise
                rotateAnimation = new RotateAnimation(ROTATED_POSITION,
                        INITIAL_POSITION,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            } else { // rotate counterclockwise
                rotateAnimation = new RotateAnimation(-1 * ROTATED_POSITION,
                        INITIAL_POSITION,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                        RotateAnimation.RELATIVE_TO_SELF, 0.5f);
            }

            rotateAnimation.setDuration(200);
            rotateAnimation.setFillAfter(true);
            //   mArrowExpandImageView.startAnimation(rotateAnimation);
        }
    }
}
