package com.brst.shopifyapplication.Adaptors;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.Fragments.ProductStaggeredFragment;
import com.brst.shopifyapplication.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.shopify.buy3.Storefront;

import java.util.List;

/**
 * Created by brst-pc89 on 12/30/17.
 */

public class CategoryAdaptor extends RecyclerView.Adapter<CategoryAdaptor.ViewHolder> {

    Context context;
    List<Storefront.Collection> categoryList;

    public CategoryAdaptor(Context context, List<Storefront.Collection> categoryList) {

        this.context = context;
        this.categoryList = categoryList;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // return null;
        View view = LayoutInflater.from(context).inflate(R.layout.custom_categoryview, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Storefront.Collection collection = categoryList.get(position);

        holder.textViewCategory.setText(collection.getTitle());

        if (collection.getImage()!=null&&collection.getImage().getSrc() != null) {
            String src = collection.getImage().getSrc();
            holder.progress.setVisibility(View.VISIBLE);

            Glide.with(context).load(src).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    holder.progress.setVisibility(View.GONE);

                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                    holder.progress.setVisibility(View.GONE);
                    return false;
                }
            }).into(holder.collectionIV);

        }

        else {
            Glide.with(context).load(R.mipmap.placeholder_image).into(holder.collectionIV);
        }

    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public void addList(List<Storefront.Collection> collections) {
        this.categoryList = collections;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewCategory;
        ProgressBar progress;
        ImageView collectionIV;

        public ViewHolder(View itemView) {
            super(itemView);

            textViewCategory = itemView.findViewById(R.id.textViewCategory);
            progress = itemView.findViewById(R.id.progress);
            collectionIV = itemView.findViewById(R.id.collectionIV);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Bundle bundle = new Bundle();
                    bundle.putString("collection_id", String.valueOf(categoryList.get(getAdapterPosition()).getId()));
                    bundle.putString("title", String.valueOf(categoryList.get(getAdapterPosition()).getTitle()));
                    bundle.putInt("pos", getAdapterPosition());
                    ((HomeActivity) context).setVisibilityOfSearch(true);
              /**/
                    //  ProductFragment productFragment = new ProductFragment();
                    ProductStaggeredFragment productFragment = new ProductStaggeredFragment();
                    //   productFragment.setData(data_list.get(getAdapterPosition()));

                    ((HomeActivity) context).fragmentTransaction(productFragment, R.id.container, bundle);
                }
            });
        }
    }
}
