package com.brst.shopifyapplication.Fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import com.brst.shopifyapplication.Activities.HomePackage.HomeActivity;
import com.brst.shopifyapplication.R;
import com.brst.shopifyapplication.databinding.WebviewFragmentBinding;


public class WebViewFragment extends BaseFragment {

    static WebViewFragment productFragment;
    View view;
    WebviewFragmentBinding webviewFragmentBinding;

    /*public void setData(Storefront.Collection collection) {
        data = new ArrayList<>();
        data.add(collection);

    }
*/
    public static WebViewFragment getInstance() {
        if (productFragment == null) {
            return productFragment = new WebViewFragment();
        }
        return productFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //  if (view == null) {
        webviewFragmentBinding = DataBindingUtil.inflate(
                inflater, R.layout.webview_fragment, container, false);
        view = webviewFragmentBinding.getRoot();

        initView();
        getActivity().setTitle("Web Checkout");

        getDataFromBundle();

        //    }
        ((HomeActivity) getActivity()).setExpansion(true, false, 0);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.search);
        MenuItem item1 = menu.findItem(R.id.sort);
        MenuItem item12 = menu.findItem(R.id.bag);
        MenuItem logout = menu.findItem(R.id.logout);
        MenuItem mode = menu.findItem(R.id.mode);
        mode.setVisible(false);
        item12.setVisible(false);
        item.setVisible(false);
        item1.setVisible(false);
        logout.setVisible(false);
    }

    private void initView() {


    }

    private void getDataFromBundle() {
        Bundle bundle = getArguments();


        if (bundle != null) {

            String url = bundle.getString("url");
            webviewFragmentBinding.webView.loadUrl(url);
            initProgressDialog(getActivity());
            webviewFragmentBinding.webView.setWebChromeClient(new WebChromeClient() {
                public void onProgressChanged(WebView view, int progress) {

                    if (progress == 100) {
                        dismissProgressDialog(getActivity());

                    }
                }
            });
        }


    }


}

