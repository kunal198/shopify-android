package com.brst.shopifyapplication.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.transition.Explode;
import android.transition.Transition;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.brst.shopifyapplication.HelperClasses.CircleProgressBar;
import com.brst.shopifyapplication.R;

import org.greenrobot.eventbus.Subscribe;


/**
 * Created by Jatin on 9/3/2017.
 */

public abstract class ParentActivity extends AppCompatActivity {

    Dialog dialog;
    private ProgressDialog progressDialog;
    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
        If you have set an enter transition for the second activity,
        the transition is also activated when the activity starts.
         */

        handler = new Handler(Looper.getMainLooper());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            Transition ts = new Explode();  //Slide(); //Explode();

            ts.setStartDelay(2000);
            ts.setDuration(5000);

            getWindow().setEnterTransition(ts);
            getWindow().setExitTransition(ts);
        }


    }

    @Subscribe
    public void onMessageEvent( String s) {

        /* Do something */
    }


    protected void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    protected void clearFocus(TextInputEditText textInputEditText) {
        textInputEditText.clearFocus();
    }

    public void setOnActivityTransfer(Class<?> des_class) {
        startActivity(new Intent(this, des_class));


    }

    public void initProgressDialog(Context context) {
/*
        progressDialog = ProgressDialog.show(context, "", "");
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setCanceledOnTouchOutside(false);
        GeometricProgressView geometricProgressView = progressDialog.findViewById(R.id.progressBar1);
        geometricProgressView.setType(GeometricProgressView.TYPE.TRIANGLE);
        geometricProgressView.setFigurePaddingInDp(1);
        geometricProgressView.setNumberOfAngles(10);
*/

        progressDialog = ProgressDialog.show(context, "", "");
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        progressDialog.setContentView(R.layout.progress_dialog);
        CircleProgressBar circleProgressBar = progressDialog.findViewById(R.id.progressBar1);
        circleProgressBar.setColorSchemeResources(R.color.colorApp, R.color.colorOrange, R.color.colorBlack, R.color.colorBlue);
        progressDialog.show();
    }

    public void dismissProgressDialog(Context context) {
        Log.d("progressdialog", "dialog");
        if (progressDialog != null) {
            Log.d("progressdialog", "dialog" + progressDialog);
            if (progressDialog.isShowing()) {

                progressDialog.dismiss();
            }
        }
    }

    protected void setOnActivityTransfer(Class<?> des_class, int req_code, ParentActivity.OnActivityTransfer listener, String json) {
        Intent intent = new Intent(this, des_class);
        intent.putExtra("data", json);
        startActivity(intent);
        listener.onTransfer(req_code);

    }

    public void fragmentTransaction(Fragment fragment, int layout) {
        if (handler == null) {
            handler = new Handler(Looper.getMainLooper());
        }
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                FragmentTransaction fragmentTransaction = ParentActivity.this.getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.animation_enter_from_right, R.anim.animation_leave_out_to_left, R.anim.animation_enter_from_right, R.anim.animation_leave_out_to_left);
                // fragmentTransaction.setCustomAnimations(R.anim.animation_enter_from_right, R.anim.animation_leave_out_to_left);
                fragmentTransaction.replace(layout, fragment).addToBackStack(fragment.getClass().getSimpleName())
                        .commit();

            }
        };

        handler.post(runnable);
    }

    protected void fragmentTransactionWithBackStack(Fragment fragment, int layout) {
        if (handler == null) {
            handler = new Handler(Looper.getMainLooper());
        }
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                FragmentTransaction fragmentTransaction = ParentActivity.this.getSupportFragmentManager().beginTransaction();
//        fragmentTransaction.setCustomAnimations(R.anim.animation_enter_from_right, R.anim.animation_leave_out_to_left);
                fragmentTransaction.setCustomAnimations(R.anim.animation_enter_from_right, R.anim.animation_leave_out_to_left, R.anim.animation_enter_from_right, R.anim.animation_leave_out_to_left);

                fragmentTransaction.replace(layout, fragment).addToBackStack(null)
                        .commit();
            }
        };

        handler.post(runnable);
    }

    public void fragmentTransaction(Fragment fragment, int layout, Bundle bundle) {
        if (handler == null) {
            handler = new Handler(Looper.getMainLooper());
        }
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                fragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = ParentActivity.this.getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.animation_enter_from_right, R.anim.animation_leave_out_to_left, R.anim.animation_enter_from_right, R.anim.animation_leave_out_to_left);

                fragmentTransaction.replace(layout, fragment).addToBackStack(fragment.getClass().getSimpleName())
                        .commit();
            }
        };
        handler.post(runnable);

    }

    protected void showDialog(Context context, String msg) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.show();
        ImageView okTV = (ImageView) dialog.findViewById(R.id.cancelIV);
        TextView msgTV = dialog.findViewById(R.id.msgTV);
        msgTV.setText(msg);


        okTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

    }

    protected void dismissDialog() {
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }

    protected interface OnActivityTransfer {
        void onTransfer(int req_code);
    }


}
