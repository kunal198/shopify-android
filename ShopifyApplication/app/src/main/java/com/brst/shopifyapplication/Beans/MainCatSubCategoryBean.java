package com.brst.shopifyapplication.Beans;


import com.bignerdranch.expandablerecyclerview.model.Parent;

import java.util.List;

public class MainCatSubCategoryBean implements Parent<CategorySubCategoryBean> {

    private String mName;
    private List<CategorySubCategoryBean> mIngredients;

    public MainCatSubCategoryBean(String name, List<CategorySubCategoryBean> ingredients) {
        mName = name;
        mIngredients = ingredients;
    }

    public String getName() {
        return mName;
    }

    @Override
    public List<CategorySubCategoryBean> getChildList() {
        return mIngredients;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }

    public CategorySubCategoryBean getIngredient(int position) {
        return mIngredients.get(position);
    }


}
