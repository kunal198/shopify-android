package com.brst.shopifyapplication.Interfaces;

import com.shopify.buy3.Storefront;

/**
 * Created by brst-pc20 on 12/18/17.
 */

public interface CustomerDetailInterface {


    public void onSuccessFulDetail(Storefront.Customer customer);

    public void onFailure();

    public void onError(String error);


}
